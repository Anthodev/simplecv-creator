(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app"],{

/***/ "./assets/vue/App.vue":
/*!****************************!*\
  !*** ./assets/vue/App.vue ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _App_vue_vue_type_template_id_147f6b0c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./App.vue?vue&type=template&id=147f6b0c& */ "./assets/vue/App.vue?vue&type=template&id=147f6b0c&");
/* harmony import */ var _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./App.vue?vue&type=script&lang=js& */ "./assets/vue/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../node_modules/vuetify-loader/lib/runtime/installComponents.js */ "./node_modules/vuetify-loader/lib/runtime/installComponents.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vuetify_lib_components_VApp__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuetify/lib/components/VApp */ "./node_modules/vuetify/lib/components/VApp/index.js");
/* harmony import */ var vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vuetify/lib/components/VGrid */ "./node_modules/vuetify/lib/components/VGrid/index.js");
/* harmony import */ var vuetify_lib_components_VMain__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vuetify/lib/components/VMain */ "./node_modules/vuetify/lib/components/VMain/index.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _App_vue_vue_type_template_id_147f6b0c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _App_vue_vue_type_template_id_147f6b0c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* vuetify-loader */





_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default()(component, {VApp: vuetify_lib_components_VApp__WEBPACK_IMPORTED_MODULE_4__["VApp"],VCol: vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_5__["VCol"],VContainer: vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_5__["VContainer"],VMain: vuetify_lib_components_VMain__WEBPACK_IMPORTED_MODULE_6__["VMain"]})


/* hot reload */
if (false) { var api; }
component.options.__file = "assets/vue/App.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./assets/vue/App.vue?vue&type=script&lang=js&":
/*!*****************************************************!*\
  !*** ./assets/vue/App.vue?vue&type=script&lang=js& ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/babel-loader/lib??ref--0-0!../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./assets/vue/App.vue?vue&type=template&id=147f6b0c&":
/*!***********************************************************!*\
  !*** ./assets/vue/App.vue?vue&type=template&id=147f6b0c& ***!
  \***********************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_147f6b0c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=template&id=147f6b0c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/App.vue?vue&type=template&id=147f6b0c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_147f6b0c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_147f6b0c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./assets/vue/components/auth/Signin.vue":
/*!***********************************************!*\
  !*** ./assets/vue/components/auth/Signin.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Signin_vue_vue_type_template_id_2bcbf1b2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Signin.vue?vue&type=template&id=2bcbf1b2& */ "./assets/vue/components/auth/Signin.vue?vue&type=template&id=2bcbf1b2&");
/* harmony import */ var _Signin_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Signin.vue?vue&type=script&lang=js& */ "./assets/vue/components/auth/Signin.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vuetify-loader/lib/runtime/installComponents.js */ "./node_modules/vuetify-loader/lib/runtime/installComponents.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vuetify_lib_components_VBtn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuetify/lib/components/VBtn */ "./node_modules/vuetify/lib/components/VBtn/index.js");
/* harmony import */ var vuetify_lib_components_VCard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vuetify/lib/components/VCard */ "./node_modules/vuetify/lib/components/VCard/index.js");
/* harmony import */ var vuetify_lib_components_VForm__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vuetify/lib/components/VForm */ "./node_modules/vuetify/lib/components/VForm/index.js");
/* harmony import */ var vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! vuetify/lib/components/VGrid */ "./node_modules/vuetify/lib/components/VGrid/index.js");
/* harmony import */ var vuetify_lib_components_VTextField__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! vuetify/lib/components/VTextField */ "./node_modules/vuetify/lib/components/VTextField/index.js");
/* harmony import */ var vuetify_lib_components_VToolbar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! vuetify/lib/components/VToolbar */ "./node_modules/vuetify/lib/components/VToolbar/index.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Signin_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Signin_vue_vue_type_template_id_2bcbf1b2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Signin_vue_vue_type_template_id_2bcbf1b2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* vuetify-loader */










_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default()(component, {VBtn: vuetify_lib_components_VBtn__WEBPACK_IMPORTED_MODULE_4__["VBtn"],VCard: vuetify_lib_components_VCard__WEBPACK_IMPORTED_MODULE_5__["VCard"],VCardActions: vuetify_lib_components_VCard__WEBPACK_IMPORTED_MODULE_5__["VCardActions"],VCardText: vuetify_lib_components_VCard__WEBPACK_IMPORTED_MODULE_5__["VCardText"],VForm: vuetify_lib_components_VForm__WEBPACK_IMPORTED_MODULE_6__["VForm"],VSpacer: vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_7__["VSpacer"],VTextField: vuetify_lib_components_VTextField__WEBPACK_IMPORTED_MODULE_8__["VTextField"],VToolbar: vuetify_lib_components_VToolbar__WEBPACK_IMPORTED_MODULE_9__["VToolbar"],VToolbarTitle: vuetify_lib_components_VToolbar__WEBPACK_IMPORTED_MODULE_9__["VToolbarTitle"]})


/* hot reload */
if (false) { var api; }
component.options.__file = "assets/vue/components/auth/Signin.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./assets/vue/components/auth/Signin.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./assets/vue/components/auth/Signin.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Signin_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Signin.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/auth/Signin.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Signin_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./assets/vue/components/auth/Signin.vue?vue&type=template&id=2bcbf1b2&":
/*!******************************************************************************!*\
  !*** ./assets/vue/components/auth/Signin.vue?vue&type=template&id=2bcbf1b2& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Signin_vue_vue_type_template_id_2bcbf1b2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Signin.vue?vue&type=template&id=2bcbf1b2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/auth/Signin.vue?vue&type=template&id=2bcbf1b2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Signin_vue_vue_type_template_id_2bcbf1b2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Signin_vue_vue_type_template_id_2bcbf1b2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./assets/vue/components/auth/Signup.vue":
/*!***********************************************!*\
  !*** ./assets/vue/components/auth/Signup.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Signup_vue_vue_type_template_id_029f44c6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Signup.vue?vue&type=template&id=029f44c6& */ "./assets/vue/components/auth/Signup.vue?vue&type=template&id=029f44c6&");
/* harmony import */ var _Signup_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Signup.vue?vue&type=script&lang=js& */ "./assets/vue/components/auth/Signup.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vuetify-loader/lib/runtime/installComponents.js */ "./node_modules/vuetify-loader/lib/runtime/installComponents.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vuetify_lib_components_VBtn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuetify/lib/components/VBtn */ "./node_modules/vuetify/lib/components/VBtn/index.js");
/* harmony import */ var vuetify_lib_components_VCard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vuetify/lib/components/VCard */ "./node_modules/vuetify/lib/components/VCard/index.js");
/* harmony import */ var vuetify_lib_components_VForm__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vuetify/lib/components/VForm */ "./node_modules/vuetify/lib/components/VForm/index.js");
/* harmony import */ var vuetify_lib_components_VProgressLinear__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! vuetify/lib/components/VProgressLinear */ "./node_modules/vuetify/lib/components/VProgressLinear/index.js");
/* harmony import */ var vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! vuetify/lib/components/VGrid */ "./node_modules/vuetify/lib/components/VGrid/index.js");
/* harmony import */ var vuetify_lib_components_VTextField__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! vuetify/lib/components/VTextField */ "./node_modules/vuetify/lib/components/VTextField/index.js");
/* harmony import */ var vuetify_lib_components_VToolbar__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! vuetify/lib/components/VToolbar */ "./node_modules/vuetify/lib/components/VToolbar/index.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Signup_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Signup_vue_vue_type_template_id_029f44c6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Signup_vue_vue_type_template_id_029f44c6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* vuetify-loader */











_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default()(component, {VBtn: vuetify_lib_components_VBtn__WEBPACK_IMPORTED_MODULE_4__["VBtn"],VCard: vuetify_lib_components_VCard__WEBPACK_IMPORTED_MODULE_5__["VCard"],VCardActions: vuetify_lib_components_VCard__WEBPACK_IMPORTED_MODULE_5__["VCardActions"],VCardText: vuetify_lib_components_VCard__WEBPACK_IMPORTED_MODULE_5__["VCardText"],VForm: vuetify_lib_components_VForm__WEBPACK_IMPORTED_MODULE_6__["VForm"],VProgressLinear: vuetify_lib_components_VProgressLinear__WEBPACK_IMPORTED_MODULE_7__["VProgressLinear"],VSpacer: vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_8__["VSpacer"],VTextField: vuetify_lib_components_VTextField__WEBPACK_IMPORTED_MODULE_9__["VTextField"],VToolbar: vuetify_lib_components_VToolbar__WEBPACK_IMPORTED_MODULE_10__["VToolbar"],VToolbarTitle: vuetify_lib_components_VToolbar__WEBPACK_IMPORTED_MODULE_10__["VToolbarTitle"]})


/* hot reload */
if (false) { var api; }
component.options.__file = "assets/vue/components/auth/Signup.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./assets/vue/components/auth/Signup.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./assets/vue/components/auth/Signup.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Signup_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Signup.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/auth/Signup.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Signup_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./assets/vue/components/auth/Signup.vue?vue&type=template&id=029f44c6&":
/*!******************************************************************************!*\
  !*** ./assets/vue/components/auth/Signup.vue?vue&type=template&id=029f44c6& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Signup_vue_vue_type_template_id_029f44c6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Signup.vue?vue&type=template&id=029f44c6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/auth/Signup.vue?vue&type=template&id=029f44c6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Signup_vue_vue_type_template_id_029f44c6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Signup_vue_vue_type_template_id_029f44c6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./assets/vue/index.js":
/*!*****************************!*\
  !*** ./assets/vue/index.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var _App__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./App */ "./assets/vue/App.vue");
/* harmony import */ var _router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./router */ "./assets/vue/router/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./store/store */ "./assets/vue/store/store.js");
/* harmony import */ var _plugins_vuetify__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./plugins/vuetify */ "./assets/vue/plugins/vuetify.js");
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(vuelidate__WEBPACK_IMPORTED_MODULE_8__);









vue__WEBPACK_IMPORTED_MODULE_2__["default"].use(vuelidate__WEBPACK_IMPORTED_MODULE_8___default.a);
axios__WEBPACK_IMPORTED_MODULE_5___default.a.defaults.baseURL = "https://" + window.location.hostname;
var token = _store_store__WEBPACK_IMPORTED_MODULE_6__["default"].getters.userToken;
if (token) axios__WEBPACK_IMPORTED_MODULE_5___default.a.interceptors.request.use(function (config) {
  config.headers.Authorization = "Bearer ".concat(token);
  return config;
}, function (error) {
  return Promise.reject(error);
});
new vue__WEBPACK_IMPORTED_MODULE_2__["default"]({
  el: '#app',
  router: _router__WEBPACK_IMPORTED_MODULE_4__["default"],
  store: _store_store__WEBPACK_IMPORTED_MODULE_6__["default"],
  vuetify: _plugins_vuetify__WEBPACK_IMPORTED_MODULE_7__["default"],
  Vuelidate: vuelidate__WEBPACK_IMPORTED_MODULE_8___default.a,
  render: function render(h) {
    return h(_App__WEBPACK_IMPORTED_MODULE_3__["default"]);
  },
  components: {
    App: _App__WEBPACK_IMPORTED_MODULE_3__["default"]
  },
  template: "<App/>"
}).$mount('#app');

/***/ }),

/***/ "./assets/vue/plugins/vuetify.js":
/*!***************************************!*\
  !*** ./assets/vue/plugins/vuetify.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var vuetify__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuetify */ "./node_modules/vuetify/dist/vuetify.js");
/* harmony import */ var vuetify__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vuetify__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vuetify_dist_vuetify_min_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuetify/dist/vuetify.min.css */ "./node_modules/vuetify/dist/vuetify.min.css");
/* harmony import */ var vuetify_dist_vuetify_min_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vuetify_dist_vuetify_min_css__WEBPACK_IMPORTED_MODULE_2__);



vue__WEBPACK_IMPORTED_MODULE_0__["default"].use(vuetify__WEBPACK_IMPORTED_MODULE_1___default.a);
var opts = {
  theme: {
    dark: true
  }
};
/* harmony default export */ __webpack_exports__["default"] = (new vuetify__WEBPACK_IMPORTED_MODULE_1___default.a(opts));

/***/ }),

/***/ "./assets/vue/router/index.js":
/*!************************************!*\
  !*** ./assets/vue/router/index.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_some__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.some */ "./node_modules/core-js/modules/es.array.some.js");
/* harmony import */ var core_js_modules_es_array_some__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_some__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.esm.js");
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../store/store */ "./assets/vue/store/store.js");
/* harmony import */ var _views_Home__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../views/Home */ "./assets/vue/views/Home.vue");
/* harmony import */ var _views_Auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../views/Auth */ "./assets/vue/views/Auth.vue");
/* harmony import */ var _views_Admin__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../views/Admin */ "./assets/vue/views/Admin.vue");






 // import authMiddleware from "../middleware/auth"

vue__WEBPACK_IMPORTED_MODULE_1__["default"].use(vue_router__WEBPACK_IMPORTED_MODULE_2__["default"]);
var router = new vue_router__WEBPACK_IMPORTED_MODULE_2__["default"]({
  mode: "history",
  routes: [{
    path: "",
    name: 'Home',
    component: _views_Home__WEBPACK_IMPORTED_MODULE_4__["default"]
  }, {
    path: "/auth",
    name: 'Auth',
    component: _views_Auth__WEBPACK_IMPORTED_MODULE_5__["default"]
  }, {
    path: "/admin",
    name: 'Admin',
    component: _views_Admin__WEBPACK_IMPORTED_MODULE_6__["default"],
    meta: {
      requiresAuth: true
    }
  }]
});
router.beforeEach(function (to, from, next) {
  if (to.matched.some(function (record) {
    return record.meta.requiresAuth;
  })) {
    if (!localStorage.getItem('userToken') || localStorage.getItem('userToken') == undefined) {
      next({
        name: 'Auth'
      });
    } else {
      next();
    }
  } else next();
});
/* harmony default export */ __webpack_exports__["default"] = (router);

/***/ }),

/***/ "./assets/vue/store/store.js":
/*!***********************************!*\
  !*** ./assets/vue/store/store.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! regenerator-runtime/runtime */ "./node_modules/regenerator-runtime/runtime.js");
/* harmony import */ var regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../router */ "./assets/vue/router/index.js");




function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }


 // import { axios } from '../plugins/axios'



vue__WEBPACK_IMPORTED_MODULE_3__["default"].use(vuex__WEBPACK_IMPORTED_MODULE_4__["default"]);
/* harmony default export */ __webpack_exports__["default"] = (new vuex__WEBPACK_IMPORTED_MODULE_4__["default"].Store({
  state: {
    cvData: null,
    authentificated: false,
    usercount: null
  },
  mutations: {
    SET_CV_DATA: function SET_CV_DATA(state, payload) {
      state.cvData = payload.cvData;
    },
    SET_AUTHENTIFICATED: function SET_AUTHENTIFICATED(state, payload) {
      state.authentificated = payload.authentificated;
    },
    SET_USERCOUNT: function SET_USERCOUNT(state, payload) {
      state.usercount = payload;
    }
  },
  actions: {
    SIGNUP: function SIGNUP(_ref, authData) {
      return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        var dispatch;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                dispatch = _ref.dispatch;
                _context.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_5___default.a.post("/api/user/new", {
                  username: authData.username,
                  password: authData.password,
                  roleUser: authData.role
                }).then(function (res) {
                  console.log(res);
                  dispatch("LOGIN", authData);
                })["catch"](function (error) {
                  return console.log(error);
                });

              case 3:
                return _context.abrupt("return", _context.sent);

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    LOGIN: function LOGIN(_ref2, authData) {
      return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
        var commit, res;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                commit = _ref2.commit;
                _context2.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_5___default.a.post("/api/auth/login_check", {
                  username: authData.username,
                  password: authData.password
                });

              case 3:
                res = _context2.sent;
                console.info(res);
                commit("SET_AUTHENTIFICATED", true);
                localStorage.setItem("userToken", res.data.token);
                _router__WEBPACK_IMPORTED_MODULE_6__["default"].push("/admin");

              case 8:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    LOGOUT: function LOGOUT(_ref3) {
      var commit = _ref3.commit;
      localStorage.removeItem('userToken');
      commit("SET_AUTHENTIFICATED", false);
      _router__WEBPACK_IMPORTED_MODULE_6__["default"].push('/');
    },
    FETCH_CV_DATA: function FETCH_CV_DATA(_ref4) {
      return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
        var commit;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                commit = _ref4.commit;
                _context3.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_5___default.a.get("/get/data").then(function (res) {
                  commit("SET_CV_DATA", {
                    cvData: res
                  });
                  return res;
                })["catch"](function (error) {
                  return console.error(error);
                });

              case 3:
                return _context3.abrupt("return", _context3.sent);

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    FETCH_USERCOUNT: function FETCH_USERCOUNT(_ref5) {
      return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
        var commit;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                commit = _ref5.commit;
                _context4.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_5___default.a.get("/api/user/count").then(function (res) {
                  commit('SET_USERCOUNT', res.data);
                  return res;
                })["catch"](function (error) {
                  return console.error(error);
                });

              case 3:
                return _context4.abrupt("return", _context4.sent);

              case 4:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }))();
    }
  },
  getters: {
    cvData: function cvData(state) {
      return state.cvData;
    },
    authentificated: function authentificated(state) {
      return state.authentificated;
    },
    usercount: function usercount(state) {
      return state.usercount;
    },
    userToken: function userToken() {
      return localStorage.getItem("userToken");
    }
  }
}));

/***/ }),

/***/ "./assets/vue/views/Admin.vue":
/*!************************************!*\
  !*** ./assets/vue/views/Admin.vue ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Admin_vue_vue_type_template_id_74c25147___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Admin.vue?vue&type=template&id=74c25147& */ "./assets/vue/views/Admin.vue?vue&type=template&id=74c25147&");
/* harmony import */ var _Admin_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Admin.vue?vue&type=script&lang=js& */ "./assets/vue/views/Admin.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vuetify-loader/lib/runtime/installComponents.js */ "./node_modules/vuetify-loader/lib/runtime/installComponents.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuetify/lib/components/VGrid */ "./node_modules/vuetify/lib/components/VGrid/index.js");
/* harmony import */ var vuetify_lib_components_VTabs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vuetify/lib/components/VTabs */ "./node_modules/vuetify/lib/components/VTabs/index.js");
/* harmony import */ var vuetify_lib_components_VToolbar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vuetify/lib/components/VToolbar */ "./node_modules/vuetify/lib/components/VToolbar/index.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Admin_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Admin_vue_vue_type_template_id_74c25147___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Admin_vue_vue_type_template_id_74c25147___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* vuetify-loader */








_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default()(component, {VCol: vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_4__["VCol"],VRow: vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_4__["VRow"],VTab: vuetify_lib_components_VTabs__WEBPACK_IMPORTED_MODULE_5__["VTab"],VTabs: vuetify_lib_components_VTabs__WEBPACK_IMPORTED_MODULE_5__["VTabs"],VTabsItems: vuetify_lib_components_VTabs__WEBPACK_IMPORTED_MODULE_5__["VTabsItems"],VToolbar: vuetify_lib_components_VToolbar__WEBPACK_IMPORTED_MODULE_6__["VToolbar"],VToolbarTitle: vuetify_lib_components_VToolbar__WEBPACK_IMPORTED_MODULE_6__["VToolbarTitle"]})


/* hot reload */
if (false) { var api; }
component.options.__file = "assets/vue/views/Admin.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./assets/vue/views/Admin.vue?vue&type=script&lang=js&":
/*!*************************************************************!*\
  !*** ./assets/vue/views/Admin.vue?vue&type=script&lang=js& ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--0-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Admin.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/views/Admin.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./assets/vue/views/Admin.vue?vue&type=template&id=74c25147&":
/*!*******************************************************************!*\
  !*** ./assets/vue/views/Admin.vue?vue&type=template&id=74c25147& ***!
  \*******************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_template_id_74c25147___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Admin.vue?vue&type=template&id=74c25147& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/views/Admin.vue?vue&type=template&id=74c25147&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_template_id_74c25147___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_template_id_74c25147___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./assets/vue/views/Auth.vue":
/*!***********************************!*\
  !*** ./assets/vue/views/Auth.vue ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Auth_vue_vue_type_template_id_039e7540___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Auth.vue?vue&type=template&id=039e7540& */ "./assets/vue/views/Auth.vue?vue&type=template&id=039e7540&");
/* harmony import */ var _Auth_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Auth.vue?vue&type=script&lang=js& */ "./assets/vue/views/Auth.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vuetify-loader/lib/runtime/installComponents.js */ "./node_modules/vuetify-loader/lib/runtime/installComponents.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vuetify_lib_components_VApp__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuetify/lib/components/VApp */ "./node_modules/vuetify/lib/components/VApp/index.js");
/* harmony import */ var vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vuetify/lib/components/VGrid */ "./node_modules/vuetify/lib/components/VGrid/index.js");
/* harmony import */ var vuetify_lib_components_VMain__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vuetify/lib/components/VMain */ "./node_modules/vuetify/lib/components/VMain/index.js");
/* harmony import */ var vuetify_lib_components_VProgressCircular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! vuetify/lib/components/VProgressCircular */ "./node_modules/vuetify/lib/components/VProgressCircular/index.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Auth_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Auth_vue_vue_type_template_id_039e7540___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Auth_vue_vue_type_template_id_039e7540___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* vuetify-loader */







_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default()(component, {VApp: vuetify_lib_components_VApp__WEBPACK_IMPORTED_MODULE_4__["VApp"],VCol: vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_5__["VCol"],VContainer: vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_5__["VContainer"],VMain: vuetify_lib_components_VMain__WEBPACK_IMPORTED_MODULE_6__["VMain"],VProgressCircular: vuetify_lib_components_VProgressCircular__WEBPACK_IMPORTED_MODULE_7__["VProgressCircular"],VRow: vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_5__["VRow"]})


/* hot reload */
if (false) { var api; }
component.options.__file = "assets/vue/views/Auth.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./assets/vue/views/Auth.vue?vue&type=script&lang=js&":
/*!************************************************************!*\
  !*** ./assets/vue/views/Auth.vue?vue&type=script&lang=js& ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Auth_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--0-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Auth.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/views/Auth.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Auth_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./assets/vue/views/Auth.vue?vue&type=template&id=039e7540&":
/*!******************************************************************!*\
  !*** ./assets/vue/views/Auth.vue?vue&type=template&id=039e7540& ***!
  \******************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Auth_vue_vue_type_template_id_039e7540___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Auth.vue?vue&type=template&id=039e7540& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/views/Auth.vue?vue&type=template&id=039e7540&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Auth_vue_vue_type_template_id_039e7540___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Auth_vue_vue_type_template_id_039e7540___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./assets/vue/views/Home.vue":
/*!***********************************!*\
  !*** ./assets/vue/views/Home.vue ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Home_vue_vue_type_template_id_e8512dd2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Home.vue?vue&type=template&id=e8512dd2& */ "./assets/vue/views/Home.vue?vue&type=template&id=e8512dd2&");
/* harmony import */ var _Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Home.vue?vue&type=script&lang=js& */ "./assets/vue/views/Home.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Home_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Home.vue?vue&type=style&index=0&lang=sass& */ "./assets/vue/views/Home.vue?vue&type=style&index=0&lang=sass&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../node_modules/vuetify-loader/lib/runtime/installComponents.js */ "./node_modules/vuetify-loader/lib/runtime/installComponents.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vuetify/lib/components/VGrid */ "./node_modules/vuetify/lib/components/VGrid/index.js");
/* harmony import */ var vuetify_lib_components_VImg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vuetify/lib/components/VImg */ "./node_modules/vuetify/lib/components/VImg/index.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Home_vue_vue_type_template_id_e8512dd2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Home_vue_vue_type_template_id_e8512dd2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* vuetify-loader */




_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_4___default()(component, {VCol: vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_5__["VCol"],VImg: vuetify_lib_components_VImg__WEBPACK_IMPORTED_MODULE_6__["VImg"],VRow: vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_5__["VRow"]})


/* hot reload */
if (false) { var api; }
component.options.__file = "assets/vue/views/Home.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./assets/vue/views/Home.vue?vue&type=script&lang=js&":
/*!************************************************************!*\
  !*** ./assets/vue/views/Home.vue?vue&type=script&lang=js& ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--0-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/views/Home.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./assets/vue/views/Home.vue?vue&type=style&index=0&lang=sass&":
/*!*********************************************************************!*\
  !*** ./assets/vue/views/Home.vue?vue&type=style&index=0&lang=sass& ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_resolve_url_loader_index_js_ref_4_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_4_oneOf_1_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader/dist/cjs.js??ref--4-oneOf-1-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/resolve-url-loader??ref--4-oneOf-1-2!../../../node_modules/sass-loader/dist/cjs.js??ref--4-oneOf-1-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=style&index=0&lang=sass& */ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/resolve-url-loader/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/views/Home.vue?vue&type=style&index=0&lang=sass&");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_resolve_url_loader_index_js_ref_4_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_4_oneOf_1_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_resolve_url_loader_index_js_ref_4_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_4_oneOf_1_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_resolve_url_loader_index_js_ref_4_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_4_oneOf_1_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_resolve_url_loader_index_js_ref_4_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_4_oneOf_1_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_resolve_url_loader_index_js_ref_4_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_4_oneOf_1_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./assets/vue/views/Home.vue?vue&type=template&id=e8512dd2&":
/*!******************************************************************!*\
  !*** ./assets/vue/views/Home.vue?vue&type=template&id=e8512dd2& ***!
  \******************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_e8512dd2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=template&id=e8512dd2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/views/Home.vue?vue&type=template&id=e8512dd2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_e8512dd2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_e8512dd2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/App.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/App.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "App"
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/auth/Signin.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/components/auth/Signin.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Signin',
  layout: 'auth',
  data: function data() {
    return {
      username: '',
      password: '',
      formLoading: false,
      disabled: false
    };
  },
  computed: {
    usernameErrors: function usernameErrors() {
      var errors = [];
      if (!this.$v.username.$dirty) return errors;
      !this.$v.username.required && errors.push('Username is required.');
      !this.$v.username.alphaNum && errors.push('Username must be alphabetical and/or numerical.');
      !this.$v.username.minLength && errors.push('Username must be 4 characters minimum.');
      return errors;
    },
    passwordErrors: function passwordErrors() {
      var errors = [];
      if (!this.$v.password.$dirty) return errors;
      !this.$v.password.required && errors.push('Password is required.');
      !this.$v.password.minLength && errors.push('Password must be 8 characters minimum.');
      return errors;
    }
  },
  validations: {
    username: {
      required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["required"],
      alphaNum: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["alphaNum"],
      minLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["minLength"])(4)
    },
    password: {
      required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["required"],
      minLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["minLength"])(6)
    }
  },
  methods: {
    onSubmit: function onSubmit() {
      var _this = this;

      this.$v.$touch();
      this.formLoading = true;
      this.disabled = true;

      if (this.$v.$error) {
        this.formLoading = false;
        this.disabled = false;
        return;
      }

      var formData = {
        username: this.username,
        password: this.password
      };
      this.$store.dispatch('LOGIN', formData).then(function () {
        _this.formLoading = false;
        _this.disabled = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/auth/Signup.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/components/auth/Signup.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Signup',
  layout: 'auth',
  data: function data() {
    return {
      username: '',
      password: '',
      passwordConfirm: '',
      loading: false
    };
  },
  computed: {
    usernameErrors: function usernameErrors() {
      var errors = [];
      if (!this.$v.username.$dirty) return errors;
      !this.$v.username.required && errors.push('An username is required.');
      !this.$v.username.alphaNum && errors.push('The username must be alphabetical and/or numerical.');
      !this.$v.username.minLength && errors.push('The username must be 4 characters minimum.');
      return errors;
    },
    passwordErrors: function passwordErrors() {
      var errors = [];
      if (!this.$v.password.$dirty) return errors;
      !this.$v.password.required && errors.push('Password is required.');
      !this.$v.password.minLength && errors.push('Password must be 8 characters minimum.');
      return errors;
    },
    passwordConfirmErrors: function passwordConfirmErrors() {
      var errors = [];
      if (!this.$v.passwordConfirm.$dirty) return errors;
      !this.$v.passwordConfirm.required && errors.push('Confirm the password.');
      !this.$v.passwordConfirm.sameAsPassword && errors.push('This field must be the same as the password');
      return errors;
    },
    passwordLengthProgress: function passwordLengthProgress() {
      return Math.min(100, this.password.length * 12.5);
    },
    passwordConfirmLengthProgress: function passwordConfirmLengthProgress() {
      return Math.min(100, this.passwordConfirm.length * 12.5);
    },
    colorPassword: function colorPassword() {
      return ['error', 'warning', 'success'][Math.floor(this.passwordLengthProgress / 40)];
    },
    colorPasswordConfirm: function colorPasswordConfirm() {
      return ['error', 'warning', 'success'][Math.floor(this.passwordConfirmLengthProgress / 40)];
    }
  },
  validations: {
    username: {
      required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["required"],
      alphaNum: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["alphaNum"],
      minLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["minLength"])(4)
    },
    password: {
      required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["required"],
      minLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["minLength"])(6)
    },
    passwordConfirm: {
      required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["required"],
      sameAsPassword: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["sameAs"])('password')
    }
  },
  methods: {
    onSubmit: function onSubmit() {
      var _this = this;

      this.$v.$touch();
      this.loading = !this.loading;

      if (this.$v.$error) {
        this.loading = !this.loading;
        return;
      }

      if (this.$v.$error) return;
      var formData = {
        username: this.username,
        password: this.password,
        role: 'User'
      };
      this.$store.dispatch('SIGNUP', formData).then(function () {
        _this.loading = !_this.loading;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/views/Admin.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/views/Admin.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__);


//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Admin",
  middleware: 'auth',
  data: function data() {
    return {
      tab: null
    };
  },
  components: {
    appAdminInformation: function appAdminInformation() {
      return __webpack_require__.e(/*! import() */ 6).then(__webpack_require__.bind(null, /*! ../components/admin/Information */ "./assets/vue/components/admin/Information.vue"));
    },
    appAdminContacts: function appAdminContacts() {
      return __webpack_require__.e(/*! import() */ 16).then(__webpack_require__.bind(null, /*! ../components/admin/Contact */ "./assets/vue/components/admin/Contact.vue"));
    },
    appAdminExperiences: function appAdminExperiences() {
      return __webpack_require__.e(/*! import() */ 17).then(__webpack_require__.bind(null, /*! ../components/admin/Experience */ "./assets/vue/components/admin/Experience.vue"));
    },
    appAdminTrainings: function appAdminTrainings() {
      return __webpack_require__.e(/*! import() */ 21).then(__webpack_require__.bind(null, /*! ../components/admin/Training */ "./assets/vue/components/admin/Training.vue"));
    },
    appAdminPortfolio: function appAdminPortfolio() {
      return Promise.all(/*! import() */[__webpack_require__.e(1), __webpack_require__.e(19)]).then(__webpack_require__.bind(null, /*! ../components/admin/Portfolio */ "./assets/vue/components/admin/Portfolio.vue"));
    },
    appAdminAptitudes: function appAdminAptitudes() {
      return __webpack_require__.e(/*! import() */ 15).then(__webpack_require__.bind(null, /*! ../components/admin/Aptitude */ "./assets/vue/components/admin/Aptitude.vue"));
    },
    appAdminSkills: function appAdminSkills() {
      return __webpack_require__.e(/*! import() */ 5).then(__webpack_require__.bind(null, /*! ../components/admin/Skill */ "./assets/vue/components/admin/Skill.vue"));
    },
    appAdminSoftSkills: function appAdminSoftSkills() {
      return __webpack_require__.e(/*! import() */ 20).then(__webpack_require__.bind(null, /*! ../components/admin/SoftSkill */ "./assets/vue/components/admin/SoftSkill.vue"));
    },
    appAdminInterests: function appAdminInterests() {
      return __webpack_require__.e(/*! import() */ 18).then(__webpack_require__.bind(null, /*! ../components/admin/Interest */ "./assets/vue/components/admin/Interest.vue"));
    },
    appAdminLanguages: function appAdminLanguages() {
      return __webpack_require__.e(/*! import() */ 4).then(__webpack_require__.bind(null, /*! ../components/admin/Language */ "./assets/vue/components/admin/Language.vue"));
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/views/Auth.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/views/Auth.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_auth_Signin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/auth/Signin */ "./assets/vue/components/auth/Signin.vue");
/* harmony import */ var _components_auth_Signup__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/auth/Signup */ "./assets/vue/components/auth/Signup.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Auth",
  computed: {
    usercount: function usercount() {
      console.log(this.$store.getters.usercount);
      return this.$store.getters.usercount;
    }
  },
  components: {
    'appSignin': _components_auth_Signin__WEBPACK_IMPORTED_MODULE_0__["default"],
    'appSignup': _components_auth_Signup__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  methods: {
    fetchData: function fetchData() {
      console.log('check data');
      this.$store.dispatch('FETCH_USERCOUNT');
    }
  },
  created: function created() {
    this.fetchData();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/views/Home.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/views/Home.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__);


//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Home",
  components: {
    appContact: function appContact() {
      return __webpack_require__.e(/*! import() */ 7).then(__webpack_require__.bind(null, /*! ../components/Contact */ "./assets/vue/components/Contact.vue"));
    },
    appExperience: function appExperience() {
      return __webpack_require__.e(/*! import() */ 12).then(__webpack_require__.bind(null, /*! ../components/Experience */ "./assets/vue/components/Experience.vue"));
    },
    appPortfolioPro: function appPortfolioPro() {
      return __webpack_require__.e(/*! import() */ 3).then(__webpack_require__.bind(null, /*! ../components/PortfolioPro */ "./assets/vue/components/PortfolioPro.vue"));
    },
    appPortfolioPerso: function appPortfolioPerso() {
      return __webpack_require__.e(/*! import() */ 13).then(__webpack_require__.bind(null, /*! ../components/PortfolioPerso */ "./assets/vue/components/PortfolioPerso.vue"));
    },
    appTraining: function appTraining() {
      return __webpack_require__.e(/*! import() */ 14).then(__webpack_require__.bind(null, /*! ../components/Training */ "./assets/vue/components/Training.vue"));
    },
    appLanguage: function appLanguage() {
      return __webpack_require__.e(/*! import() */ 2).then(__webpack_require__.bind(null, /*! ../components/Language */ "./assets/vue/components/Language.vue"));
    },
    appSkill: function appSkill() {
      return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(10)]).then(__webpack_require__.bind(null, /*! ../components/Skill */ "./assets/vue/components/Skill.vue"));
    },
    appSoftSkill: function appSoftSkill() {
      return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(11)]).then(__webpack_require__.bind(null, /*! ../components/SoftSkill */ "./assets/vue/components/SoftSkill.vue"));
    },
    appInterest: function appInterest() {
      return __webpack_require__.e(/*! import() */ 9).then(__webpack_require__.bind(null, /*! ../components/Interest */ "./assets/vue/components/Interest.vue"));
    },
    appExtra: function appExtra() {
      return __webpack_require__.e(/*! import() */ 8).then(__webpack_require__.bind(null, /*! ../components/Extra */ "./assets/vue/components/Extra.vue"));
    }
  }
});

/***/ }),

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/resolve-url-loader/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/views/Home.vue?vue&type=style&index=0&lang=sass&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js??ref--4-oneOf-1-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/resolve-url-loader??ref--4-oneOf-1-2!./node_modules/sass-loader/dist/cjs.js??ref--4-oneOf-1-3!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/views/Home.vue?vue&type=style&index=0&lang=sass& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/App.vue?vue&type=template&id=147f6b0c&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/App.vue?vue&type=template&id=147f6b0c& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-app",
    { staticClass: "mb-6" },
    [
      _c(
        "v-main",
        [
          _c(
            "v-col",
            { staticClass: "mx-auto", attrs: { cols: "9" } },
            [
              _c(
                "v-container",
                { attrs: { fluid: "" } },
                [_c("router-view")],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/auth/Signin.vue?vue&type=template&id=2bcbf1b2&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/components/auth/Signin.vue?vue&type=template&id=2bcbf1b2& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "elevation-12" },
    [
      _c(
        "v-toolbar",
        { attrs: { color: "indigo", dark: "", flat: "" } },
        [
          _c("v-toolbar-title", [_vm._v("KijiReader - Login")]),
          _vm._v(" "),
          _c("v-spacer")
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-card-text",
        [
          _c(
            "v-form",
            {
              attrs: { id: "signinForm" },
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.onSubmit($event)
                }
              },
              nativeOn: {
                keyup: function($event) {
                  if (
                    !$event.type.indexOf("key") &&
                    _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")
                  ) {
                    return null
                  }
                  return _vm.onSubmit($event)
                }
              }
            },
            [
              _c("v-text-field", {
                attrs: {
                  id: "username",
                  label: "Login",
                  name: "username",
                  "prepend-icon": "mdi-account",
                  type: "text",
                  "error-messages": _vm.usernameErrors,
                  placeholder: "Enter your username",
                  required: "",
                  autofocus: ""
                },
                on: {
                  blur: function($event) {
                    return _vm.$v.username.$touch()
                  }
                },
                model: {
                  value: _vm.username,
                  callback: function($$v) {
                    _vm.username = $$v
                  },
                  expression: "username"
                }
              }),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  id: "password",
                  label: "Password",
                  name: "password",
                  "prepend-icon": "mdi-lock",
                  type: "password",
                  "error-messages": _vm.passwordErrors,
                  placeholder: "Enter your password"
                },
                on: {
                  blur: function($event) {
                    return _vm.$v.password.$touch()
                  }
                },
                model: {
                  value: _vm.password,
                  callback: function($$v) {
                    _vm.password = $$v
                  },
                  expression: "password"
                }
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-card-actions",
        [
          _c("v-spacer"),
          _vm._v(" "),
          _c(
            "v-btn",
            {
              attrs: {
                form: "signinForm",
                type: "submit",
                color: "primary",
                disabled: _vm.$v.$invalid || _vm.disabled,
                loading: _vm.formLoading
              }
            },
            [_vm._v("Login")]
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/auth/Signup.vue?vue&type=template&id=029f44c6&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/components/auth/Signup.vue?vue&type=template&id=029f44c6& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "elevation-12" },
    [
      _c(
        "v-toolbar",
        { attrs: { color: "indigo", dark: "", flat: "" } },
        [
          _c("v-toolbar-title", [_vm._v("Simple CV - Create an account")]),
          _vm._v(" "),
          _c("v-spacer")
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-card-text",
        [
          _c(
            "v-form",
            {
              attrs: { id: "signupForm" },
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.onSubmit($event)
                }
              },
              nativeOn: {
                keyup: function($event) {
                  if (
                    !$event.type.indexOf("key") &&
                    _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")
                  ) {
                    return null
                  }
                  return _vm.onSubmit($event)
                }
              }
            },
            [
              _c("v-text-field", {
                attrs: {
                  id: "username",
                  label: "Login",
                  name: "username",
                  "prepend-icon": "mdi-account",
                  type: "text",
                  "error-messages": _vm.usernameErrors,
                  placeholder: "Enter your username",
                  required: "",
                  autofocus: ""
                },
                on: {
                  blur: function($event) {
                    return _vm.$v.username.$touch()
                  }
                },
                model: {
                  value: _vm.username,
                  callback: function($$v) {
                    _vm.username = $$v
                  },
                  expression: "username"
                }
              }),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  id: "password",
                  label: "Password",
                  name: "password",
                  "prepend-icon": "mdi-lock",
                  type: "password",
                  "error-messages": _vm.passwordErrors,
                  placeholder: "Enter a password",
                  required: ""
                },
                on: {
                  blur: function($event) {
                    return _vm.$v.password.$touch()
                  }
                },
                model: {
                  value: _vm.password,
                  callback: function($$v) {
                    _vm.password = $$v
                  },
                  expression: "password"
                }
              }),
              _vm._v(" "),
              _c("v-progress-linear", {
                attrs: {
                  value: _vm.passwordLengthProgress,
                  color: _vm.colorPassword,
                  height: "7"
                }
              }),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  id: "passwordConfirm",
                  label: "Confirm the password",
                  name: "passwordConfirm",
                  "prepend-icon": "mdi-lock",
                  type: "password",
                  "error-messages": _vm.passwordConfirmErrors,
                  placeholder: "Confirm the password",
                  required: ""
                },
                on: {
                  blur: function($event) {
                    return _vm.$v.passwordConfirm.$touch()
                  }
                },
                model: {
                  value: _vm.passwordConfirm,
                  callback: function($$v) {
                    _vm.passwordConfirm = $$v
                  },
                  expression: "passwordConfirm"
                }
              }),
              _vm._v(" "),
              _c("v-progress-linear", {
                attrs: {
                  value: _vm.passwordConfirmLengthProgress,
                  color: _vm.colorPasswordConfirm,
                  height: "7"
                }
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-card-actions",
        [
          _c(
            "v-card-text",
            { staticClass: "caption" },
            [
              _c("router-link", { attrs: { to: { name: "Auth" } } }, [
                _vm._v("Login to your account")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("v-spacer"),
          _vm._v(" "),
          _c(
            "v-btn",
            {
              attrs: {
                form: "signupForm",
                type: "submit",
                color: "primary",
                loading: _vm.loading,
                disabled: _vm.$v.$invalid
              }
            },
            [_vm._v("Create an account")]
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/views/Admin.vue?vue&type=template&id=74c25147&":
/*!*************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/views/Admin.vue?vue&type=template&id=74c25147& ***!
  \*************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-row",
    { attrs: { align: "top", justify: "center" } },
    [
      _c(
        "v-col",
        { attrs: { cols: "12" } },
        [
          _c(
            "v-toolbar",
            {
              staticClass: "rounded-t-lg",
              attrs: { flat: "" },
              scopedSlots: _vm._u([
                {
                  key: "extension",
                  fn: function() {
                    return [
                      _c(
                        "v-tabs",
                        {
                          attrs: { "slider-color": "amber", centered: "" },
                          model: {
                            value: _vm.tab,
                            callback: function($$v) {
                              _vm.tab = $$v
                            },
                            expression: "tab"
                          }
                        },
                        [
                          _c("v-tab", { attrs: { href: "#tab-information" } }, [
                            _vm._v("My Information")
                          ]),
                          _vm._v(" "),
                          _c("v-tab", { attrs: { href: "#tab-contacts" } }, [
                            _vm._v("Contacts")
                          ]),
                          _vm._v(" "),
                          _c("v-tab", { attrs: { href: "#tab-experiences" } }, [
                            _vm._v("Experiences")
                          ]),
                          _vm._v(" "),
                          _c("v-tab", { attrs: { href: "#tab-trainings" } }, [
                            _vm._v("Trainings")
                          ]),
                          _vm._v(" "),
                          _c("v-tab", { attrs: { href: "#tab-portfolio" } }, [
                            _vm._v("Portfolio")
                          ]),
                          _vm._v(" "),
                          _c("v-tab", { attrs: { href: "#tab-aptitudes" } }, [
                            _vm._v("Aptitudes")
                          ]),
                          _vm._v(" "),
                          _c("v-tab", { attrs: { href: "#tab-skills" } }, [
                            _vm._v("Skills")
                          ]),
                          _vm._v(" "),
                          _c("v-tab", { attrs: { href: "#tab-soft-skills" } }, [
                            _vm._v("Soft skills")
                          ]),
                          _vm._v(" "),
                          _c("v-tab", { attrs: { href: "#tab-interests" } }, [
                            _vm._v("Interests")
                          ]),
                          _vm._v(" "),
                          _c("v-tab", { attrs: { href: "#tab-languages" } }, [
                            _vm._v("Languages")
                          ])
                        ],
                        1
                      )
                    ]
                  },
                  proxy: true
                }
              ])
            },
            [_c("v-toolbar-title", [_vm._v("Admin CV")])],
            1
          ),
          _vm._v(" "),
          _c(
            "v-tabs-items",
            {
              model: {
                value: _vm.tab,
                callback: function($$v) {
                  _vm.tab = $$v
                },
                expression: "tab"
              }
            },
            [
              _c("app-admin-information"),
              _vm._v(" "),
              _c("app-admin-contacts"),
              _vm._v(" "),
              _c("app-admin-experiences"),
              _vm._v(" "),
              _c("app-admin-trainings"),
              _vm._v(" "),
              _c("app-admin-portfolio"),
              _vm._v(" "),
              _c("app-admin-aptitudes"),
              _vm._v(" "),
              _c("app-admin-skills"),
              _vm._v(" "),
              _c("app-admin-soft-skills"),
              _vm._v(" "),
              _c("app-admin-interests"),
              _vm._v(" "),
              _c("app-admin-languages")
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/views/Auth.vue?vue&type=template&id=039e7540&":
/*!************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/views/Auth.vue?vue&type=template&id=039e7540& ***!
  \************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-app",
    [
      _c(
        "v-main",
        [
          _c(
            "v-container",
            { staticClass: "fill-height fluid" },
            [
              _c(
                "v-row",
                { attrs: { align: "center", justify: "center" } },
                [
                  _vm.usercount === null
                    ? _c(
                        "v-col",
                        {
                          staticClass: "text-center",
                          attrs: { cols: "12", sm: "8", md: "6" }
                        },
                        [
                          _c("v-progress-circular", {
                            attrs: { indeterminate: "", color: "amber" }
                          })
                        ],
                        1
                      )
                    : _c(
                        "v-col",
                        { attrs: { cols: "12", sm: "8", md: "6" } },
                        [
                          _vm.usercount == 1
                            ? _c("app-signin")
                            : _c("app-signup")
                        ],
                        1
                      )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/views/Home.vue?vue&type=template&id=e8512dd2&":
/*!************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/views/Home.vue?vue&type=template&id=e8512dd2& ***!
  \************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-row",
    { attrs: { align: "top", justify: "center" } },
    [
      _c(
        "v-col",
        { attrs: { cols: "9" } },
        [
          _c(
            "v-row",
            [
              _c("v-col", { attrs: { cols: "12" } }, [
                _c("p", { staticClass: "text-h3" }, [_vm._v("Nom")]),
                _vm._v(" "),
                _c("p", { staticClass: "text-h4" }, [_vm._v("Title")])
              ]),
              _vm._v(" "),
              _c("v-col", { attrs: { cols: "12" } }, [_c("app-contact")], 1)
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "12" } },
                [
                  _c(
                    "div",
                    {
                      staticClass:
                        "text-h3 font-weight-bold text-uppercase section-title mb-2"
                    },
                    [_vm._v("Expériences")]
                  ),
                  _vm._v(" "),
                  _c("app-experience")
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "12" } },
                [
                  _c(
                    "div",
                    {
                      staticClass:
                        "text-h3 font-weight-bold text-uppercase section-title mb-2"
                    },
                    [_vm._v("Portfolio professionnel")]
                  ),
                  _vm._v(" "),
                  _c("app-portfolio-pro")
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "12" } },
                [
                  _c(
                    "div",
                    {
                      staticClass:
                        "text-h3 font-weight-bold text-uppercase section-title mb-2"
                    },
                    [_vm._v("Portfolio personnel")]
                  ),
                  _vm._v(" "),
                  _c("app-portfolio-perso")
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "12" } },
                [
                  _c(
                    "div",
                    {
                      staticClass:
                        "text-h3 font-weight-bold text-uppercase section-title mb-2"
                    },
                    [_vm._v("Formations")]
                  ),
                  _vm._v(" "),
                  _c("app-training")
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-col",
        { attrs: { cols: "3" } },
        [
          _c(
            "v-col",
            { staticClass: "mb-3", attrs: { cols: "12" } },
            [
              _c("v-img", {
                attrs: {
                  src:
                    "https://www.citationbonheur.fr/wp-content/uploads/2017/08/placeholder-1024x1024.png"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c("app-language"),
          _vm._v(" "),
          _c("app-skill"),
          _vm._v(" "),
          _c("app-soft-skill"),
          _vm._v(" "),
          _c("app-interest"),
          _vm._v(" "),
          _c("app-extra")
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

},[["./assets/vue/index.js","runtime","vendors~app"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvdnVlL0FwcC52dWUiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3Z1ZS9BcHAudnVlP2U2YTUiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3Z1ZS9BcHAudnVlPzk2YTUiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3Z1ZS9jb21wb25lbnRzL2F1dGgvU2lnbmluLnZ1ZSIsIndlYnBhY2s6Ly8vLi9hc3NldHMvdnVlL2NvbXBvbmVudHMvYXV0aC9TaWduaW4udnVlPzVjNjkiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3Z1ZS9jb21wb25lbnRzL2F1dGgvU2lnbmluLnZ1ZT9jYjQwIiwid2VicGFjazovLy8uL2Fzc2V0cy92dWUvY29tcG9uZW50cy9hdXRoL1NpZ251cC52dWUiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3Z1ZS9jb21wb25lbnRzL2F1dGgvU2lnbnVwLnZ1ZT8yMzc2Iiwid2VicGFjazovLy8uL2Fzc2V0cy92dWUvY29tcG9uZW50cy9hdXRoL1NpZ251cC52dWU/ZjAyOCIsIndlYnBhY2s6Ly8vLi9hc3NldHMvdnVlL2luZGV4LmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy92dWUvcGx1Z2lucy92dWV0aWZ5LmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy92dWUvcm91dGVyL2luZGV4LmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy92dWUvc3RvcmUvc3RvcmUuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3Z1ZS92aWV3cy9BZG1pbi52dWUiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3Z1ZS92aWV3cy9BZG1pbi52dWU/OTEzOCIsIndlYnBhY2s6Ly8vLi9hc3NldHMvdnVlL3ZpZXdzL0FkbWluLnZ1ZT81NjkxIiwid2VicGFjazovLy8uL2Fzc2V0cy92dWUvdmlld3MvQXV0aC52dWUiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3Z1ZS92aWV3cy9BdXRoLnZ1ZT82MDdhIiwid2VicGFjazovLy8uL2Fzc2V0cy92dWUvdmlld3MvQXV0aC52dWU/NTdjMSIsIndlYnBhY2s6Ly8vLi9hc3NldHMvdnVlL3ZpZXdzL0hvbWUudnVlIiwid2VicGFjazovLy8uL2Fzc2V0cy92dWUvdmlld3MvSG9tZS52dWU/NTBkZSIsIndlYnBhY2s6Ly8vLi9hc3NldHMvdnVlL3ZpZXdzL0hvbWUudnVlP2Q5MTAiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3Z1ZS92aWV3cy9Ib21lLnZ1ZT8zMDhkIiwid2VicGFjazovLy9hc3NldHMvdnVlL0FwcC52dWUiLCJ3ZWJwYWNrOi8vL2Fzc2V0cy92dWUvY29tcG9uZW50cy9hdXRoL1NpZ25pbi52dWUiLCJ3ZWJwYWNrOi8vL2Fzc2V0cy92dWUvY29tcG9uZW50cy9hdXRoL1NpZ251cC52dWUiLCJ3ZWJwYWNrOi8vL2Fzc2V0cy92dWUvdmlld3MvQWRtaW4udnVlIiwid2VicGFjazovLy9hc3NldHMvdnVlL3ZpZXdzL0F1dGgudnVlIiwid2VicGFjazovLy9hc3NldHMvdnVlL3ZpZXdzL0hvbWUudnVlIiwid2VicGFjazovLy8uL2Fzc2V0cy92dWUvdmlld3MvSG9tZS52dWU/OTZmNyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvdnVlL0FwcC52dWU/OTliZiIsIndlYnBhY2s6Ly8vLi9hc3NldHMvdnVlL2NvbXBvbmVudHMvYXV0aC9TaWduaW4udnVlPzExODMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3Z1ZS9jb21wb25lbnRzL2F1dGgvU2lnbnVwLnZ1ZT8zNzcyIiwid2VicGFjazovLy8uL2Fzc2V0cy92dWUvdmlld3MvQWRtaW4udnVlPzI1ODciLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3Z1ZS92aWV3cy9BdXRoLnZ1ZT9kNjIxIiwid2VicGFjazovLy8uL2Fzc2V0cy92dWUvdmlld3MvSG9tZS52dWU/NzJiMCJdLCJuYW1lcyI6WyJWdWUiLCJ1c2UiLCJWdWVsaWRhdGUiLCJheGlvcyIsImRlZmF1bHRzIiwiYmFzZVVSTCIsIndpbmRvdyIsImxvY2F0aW9uIiwiaG9zdG5hbWUiLCJ0b2tlbiIsInN0b3JlIiwiZ2V0dGVycyIsInVzZXJUb2tlbiIsImludGVyY2VwdG9ycyIsInJlcXVlc3QiLCJjb25maWciLCJoZWFkZXJzIiwiQXV0aG9yaXphdGlvbiIsImVycm9yIiwiUHJvbWlzZSIsInJlamVjdCIsImVsIiwicm91dGVyIiwidnVldGlmeSIsInJlbmRlciIsImgiLCJBcHAiLCJjb21wb25lbnRzIiwidGVtcGxhdGUiLCIkbW91bnQiLCJWdWV0aWZ5Iiwib3B0cyIsInRoZW1lIiwiZGFyayIsIlZ1ZVJvdXRlciIsIm1vZGUiLCJyb3V0ZXMiLCJwYXRoIiwibmFtZSIsImNvbXBvbmVudCIsIkhvbWUiLCJBdXRoIiwiQWRtaW4iLCJtZXRhIiwicmVxdWlyZXNBdXRoIiwiYmVmb3JlRWFjaCIsInRvIiwiZnJvbSIsIm5leHQiLCJtYXRjaGVkIiwic29tZSIsInJlY29yZCIsImxvY2FsU3RvcmFnZSIsImdldEl0ZW0iLCJ1bmRlZmluZWQiLCJWdWV4IiwiU3RvcmUiLCJzdGF0ZSIsImN2RGF0YSIsImF1dGhlbnRpZmljYXRlZCIsInVzZXJjb3VudCIsIm11dGF0aW9ucyIsIlNFVF9DVl9EQVRBIiwicGF5bG9hZCIsIlNFVF9BVVRIRU5USUZJQ0FURUQiLCJTRVRfVVNFUkNPVU5UIiwiYWN0aW9ucyIsIlNJR05VUCIsImF1dGhEYXRhIiwiZGlzcGF0Y2giLCJwb3N0IiwidXNlcm5hbWUiLCJwYXNzd29yZCIsInJvbGVVc2VyIiwicm9sZSIsInRoZW4iLCJyZXMiLCJjb25zb2xlIiwibG9nIiwiTE9HSU4iLCJjb21taXQiLCJpbmZvIiwic2V0SXRlbSIsImRhdGEiLCJwdXNoIiwiTE9HT1VUIiwicmVtb3ZlSXRlbSIsIkZFVENIX0NWX0RBVEEiLCJnZXQiLCJGRVRDSF9VU0VSQ09VTlQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBa0Y7QUFDM0I7QUFDTDs7O0FBR2xEO0FBQzBGO0FBQzFGLGdCQUFnQiwyR0FBVTtBQUMxQixFQUFFLHlFQUFNO0FBQ1IsRUFBRSw4RUFBTTtBQUNSLEVBQUUsdUZBQWU7QUFDakI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDbUc7QUFDaEQ7QUFDQztBQUNNO0FBQ0w7QUFDckQsb0dBQWlCLGFBQWEsc0VBQUksQ0FBQyx1RUFBSSxDQUFDLG1GQUFVLENBQUMseUVBQUssQ0FBQzs7O0FBR3pEO0FBQ0EsSUFBSSxLQUFVLEVBQUUsWUFpQmY7QUFDRDtBQUNlLGdGOzs7Ozs7Ozs7Ozs7QUMvQ2Y7QUFBQTtBQUFBLHdDQUE2SyxDQUFnQiwrT0FBRyxFQUFDLEM7Ozs7Ozs7Ozs7OztBQ0FqTTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBcUY7QUFDM0I7QUFDTDs7O0FBR3JEO0FBQ2dHO0FBQ2hHLGdCQUFnQiwyR0FBVTtBQUMxQixFQUFFLDRFQUFNO0FBQ1IsRUFBRSxpRkFBTTtBQUNSLEVBQUUsMEZBQWU7QUFDakI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDeUc7QUFDdEQ7QUFDRTtBQUNPO0FBQ0g7QUFDSjtBQUNFO0FBQ1E7QUFDSjtBQUNLO0FBQ2hFLG9HQUFpQixhQUFhLHNFQUFJLENBQUMseUVBQUssQ0FBQyx1RkFBWSxDQUFDLGlGQUFTLENBQUMseUVBQUssQ0FBQyw2RUFBTyxDQUFDLHdGQUFVLENBQUMsa0ZBQVEsQ0FBQyw0RkFBYSxDQUFDOzs7QUFHaEg7QUFDQSxJQUFJLEtBQVUsRUFBRSxZQWlCZjtBQUNEO0FBQ2UsZ0Y7Ozs7Ozs7Ozs7OztBQ3BEZjtBQUFBO0FBQUEsd0NBQTRMLENBQWdCLGtQQUFHLEVBQUMsQzs7Ozs7Ozs7Ozs7O0FDQWhOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXFGO0FBQzNCO0FBQ0w7OztBQUdyRDtBQUNnRztBQUNoRyxnQkFBZ0IsMkdBQVU7QUFDMUIsRUFBRSw0RUFBTTtBQUNSLEVBQUUsaUZBQU07QUFDUixFQUFFLDBGQUFlO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ3lHO0FBQ3REO0FBQ0U7QUFDTztBQUNIO0FBQ0o7QUFDb0I7QUFDbEI7QUFDUTtBQUNKO0FBQ0s7QUFDaEUsb0dBQWlCLGFBQWEsc0VBQUksQ0FBQyx5RUFBSyxDQUFDLHVGQUFZLENBQUMsaUZBQVMsQ0FBQyx5RUFBSyxDQUFDLHVHQUFlLENBQUMsNkVBQU8sQ0FBQyx3RkFBVSxDQUFDLG1GQUFRLENBQUMsNkZBQWEsQ0FBQzs7O0FBR2hJO0FBQ0EsSUFBSSxLQUFVLEVBQUUsWUFpQmY7QUFDRDtBQUNlLGdGOzs7Ozs7Ozs7Ozs7QUNyRGY7QUFBQTtBQUFBLHdDQUE0TCxDQUFnQixrUEFBRyxFQUFDLEM7Ozs7Ozs7Ozs7OztBQ0FoTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQUEsMkNBQUcsQ0FBQ0MsR0FBSixDQUFRQyxnREFBUjtBQUVBQyw0Q0FBSyxDQUFDQyxRQUFOLENBQWVDLE9BQWYsR0FBeUIsYUFBYUMsTUFBTSxDQUFDQyxRQUFQLENBQWdCQyxRQUF0RDtBQUVBLElBQU1DLEtBQUssR0FBR0Msb0RBQUssQ0FBQ0MsT0FBTixDQUFjQyxTQUE1QjtBQUVBLElBQUlILEtBQUosRUFBV04sNENBQUssQ0FBQ1UsWUFBTixDQUFtQkMsT0FBbkIsQ0FBMkJiLEdBQTNCLENBQStCLFVBQUNjLE1BQUQsRUFBWTtBQUNwREEsUUFBTSxDQUFDQyxPQUFQLENBQWVDLGFBQWYsb0JBQXlDUixLQUF6QztBQUVBLFNBQU9NLE1BQVA7QUFDRCxDQUpVLEVBSVIsVUFBQUcsS0FBSyxFQUFJO0FBQ1YsU0FBT0MsT0FBTyxDQUFDQyxNQUFSLENBQWVGLEtBQWYsQ0FBUDtBQUNELENBTlU7QUFRWCxJQUFJbEIsMkNBQUosQ0FBUTtBQUNOcUIsSUFBRSxFQUFFLE1BREU7QUFFTkMsUUFBTSxFQUFOQSwrQ0FGTTtBQUdOWixPQUFLLEVBQUxBLG9EQUhNO0FBSU5hLFNBQU8sRUFBUEEsd0RBSk07QUFLTnJCLFdBQVMsRUFBVEEsZ0RBTE07QUFNTnNCLFFBQU0sRUFBRSxnQkFBQUMsQ0FBQztBQUFBLFdBQUlBLENBQUMsQ0FBQ0MsNENBQUQsQ0FBTDtBQUFBLEdBTkg7QUFPTkMsWUFBVSxFQUFFO0FBQUVELE9BQUcsRUFBSEEsNENBQUdBO0FBQUwsR0FQTjtBQVFORSxVQUFRLEVBQUU7QUFSSixDQUFSLEVBU0dDLE1BVEgsQ0FTVSxNQVRWLEU7Ozs7Ozs7Ozs7OztBQ3RCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTdCLDJDQUFHLENBQUNDLEdBQUosQ0FBUTZCLDhDQUFSO0FBRUEsSUFBTUMsSUFBSSxHQUFHO0FBQ1RDLE9BQUssRUFBRTtBQUNIQyxRQUFJLEVBQUU7QUFESDtBQURFLENBQWI7QUFNZSxtRUFBSUgsOENBQUosQ0FBWUMsSUFBWixDQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNaQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0NBRUE7O0FBRUEvQiwyQ0FBRyxDQUFDQyxHQUFKLENBQVFpQyxrREFBUjtBQUVBLElBQU1aLE1BQU0sR0FBRyxJQUFJWSxrREFBSixDQUFjO0FBQzNCQyxNQUFJLEVBQUUsU0FEcUI7QUFFM0JDLFFBQU0sRUFBRSxDQUNOO0FBQUVDLFFBQUksRUFBRSxFQUFSO0FBQVlDLFFBQUksRUFBRSxNQUFsQjtBQUEwQkMsYUFBUyxFQUFFQyxtREFBSUE7QUFBekMsR0FETSxFQUVOO0FBQUVILFFBQUksRUFBRSxPQUFSO0FBQWlCQyxRQUFJLEVBQUUsTUFBdkI7QUFBK0JDLGFBQVMsRUFBRUUsbURBQUlBO0FBQTlDLEdBRk0sRUFHTjtBQUFFSixRQUFJLEVBQUUsUUFBUjtBQUFrQkMsUUFBSSxFQUFFLE9BQXhCO0FBQWlDQyxhQUFTLEVBQUVHLG9EQUE1QztBQUFtREMsUUFBSSxFQUFFO0FBQ3ZEQyxrQkFBWSxFQUFFO0FBRHlDO0FBQXpELEdBSE07QUFGbUIsQ0FBZCxDQUFmO0FBV0F0QixNQUFNLENBQUN1QixVQUFQLENBQW1CLFVBQUNDLEVBQUQsRUFBS0MsSUFBTCxFQUFXQyxJQUFYLEVBQW9CO0FBQ3JDLE1BQUlGLEVBQUUsQ0FBQ0csT0FBSCxDQUFXQyxJQUFYLENBQWdCLFVBQUFDLE1BQU07QUFBQSxXQUFJQSxNQUFNLENBQUNSLElBQVAsQ0FBWUMsWUFBaEI7QUFBQSxHQUF0QixDQUFKLEVBQXlEO0FBQ3ZELFFBQUksQ0FBQ1EsWUFBWSxDQUFDQyxPQUFiLENBQXFCLFdBQXJCLENBQUQsSUFBc0NELFlBQVksQ0FBQ0MsT0FBYixDQUFxQixXQUFyQixLQUFxQ0MsU0FBL0UsRUFBMEY7QUFDeEZOLFVBQUksQ0FBRTtBQUFFVixZQUFJLEVBQUU7QUFBUixPQUFGLENBQUo7QUFDRCxLQUZELE1BRU87QUFDTFUsVUFBSTtBQUNMO0FBQ0YsR0FORCxNQU1PQSxJQUFJO0FBQ1osQ0FSRDtBQVVlMUIscUVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvQkE7Q0FFQTs7QUFDQTtBQUNBO0FBRUF0QiwyQ0FBRyxDQUFDQyxHQUFKLENBQVFzRCw0Q0FBUjtBQUVlLG1FQUFJQSw0Q0FBSSxDQUFDQyxLQUFULENBQWU7QUFDNUJDLE9BQUssRUFBRTtBQUNMQyxVQUFNLEVBQUUsSUFESDtBQUVMQyxtQkFBZSxFQUFFLEtBRlo7QUFHTEMsYUFBUyxFQUFFO0FBSE4sR0FEcUI7QUFPNUJDLFdBQVMsRUFBRTtBQUNUQyxlQURTLHVCQUNHTCxLQURILEVBQ1VNLE9BRFYsRUFDbUI7QUFDMUJOLFdBQUssQ0FBQ0MsTUFBTixHQUFlSyxPQUFPLENBQUNMLE1BQXZCO0FBQ0QsS0FIUTtBQUtUTSx1QkFMUywrQkFLV1AsS0FMWCxFQUtrQk0sT0FMbEIsRUFLMkI7QUFDbENOLFdBQUssQ0FBQ0UsZUFBTixHQUF3QkksT0FBTyxDQUFDSixlQUFoQztBQUNELEtBUFE7QUFTVE0saUJBVFMseUJBU0tSLEtBVEwsRUFTWU0sT0FUWixFQVNxQjtBQUM1Qk4sV0FBSyxDQUFDRyxTQUFOLEdBQWtCRyxPQUFsQjtBQUNEO0FBWFEsR0FQaUI7QUFxQjVCRyxTQUFPLEVBQUU7QUFDREMsVUFEQyx3QkFDb0JDLFFBRHBCLEVBQzhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXRCQyx3QkFBc0IsUUFBdEJBLFFBQXNCO0FBQUE7QUFBQSx1QkFDdEJsRSw0Q0FBSyxDQUNmbUUsSUFEVSxDQUNMLGVBREssRUFDWTtBQUNyQkMsMEJBQVEsRUFBRUgsUUFBUSxDQUFDRyxRQURFO0FBRXJCQywwQkFBUSxFQUFFSixRQUFRLENBQUNJLFFBRkU7QUFHckJDLDBCQUFRLEVBQUVMLFFBQVEsQ0FBQ007QUFIRSxpQkFEWixFQU1WQyxJQU5VLENBTUwsVUFBQ0MsR0FBRCxFQUFTO0FBQ2JDLHlCQUFPLENBQUNDLEdBQVIsQ0FBWUYsR0FBWjtBQUNBUCwwQkFBUSxDQUFDLE9BQUQsRUFBVUQsUUFBVixDQUFSO0FBQ0QsaUJBVFUsV0FVSixVQUFDbEQsS0FBRDtBQUFBLHlCQUFXMkQsT0FBTyxDQUFDQyxHQUFSLENBQVk1RCxLQUFaLENBQVg7QUFBQSxpQkFWSSxDQURzQjs7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWXBDLEtBYk07QUFlRDZELFNBZkMsd0JBZWlCWCxRQWZqQixFQWUyQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFwQlksc0JBQW9CLFNBQXBCQSxNQUFvQjtBQUFBO0FBQUEsdUJBQ2Q3RSw0Q0FBSyxDQUFDbUUsSUFBTixDQUFXLHVCQUFYLEVBQW9DO0FBQ3BEQywwQkFBUSxFQUFFSCxRQUFRLENBQUNHLFFBRGlDO0FBRXBEQywwQkFBUSxFQUFFSixRQUFRLENBQUNJO0FBRmlDLGlCQUFwQyxDQURjOztBQUFBO0FBQzFCSSxtQkFEMEI7QUFNaENDLHVCQUFPLENBQUNJLElBQVIsQ0FBYUwsR0FBYjtBQUVBSSxzQkFBTSxDQUFDLHFCQUFELEVBQXdCLElBQXhCLENBQU47QUFDQTVCLDRCQUFZLENBQUM4QixPQUFiLENBQXFCLFdBQXJCLEVBQWtDTixHQUFHLENBQUNPLElBQUosQ0FBUzFFLEtBQTNDO0FBRUFhLCtEQUFNLENBQUM4RCxJQUFQLENBQVksUUFBWjs7QUFYZ0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFZakMsS0EzQk07QUE2QlBDLFVBN0JPLHlCQStCSjtBQUFBLFVBRERMLE1BQ0MsU0FEREEsTUFDQztBQUNENUIsa0JBQVksQ0FBQ2tDLFVBQWIsQ0FBd0IsV0FBeEI7QUFFQU4sWUFBTSxDQUFDLHFCQUFELEVBQXdCLEtBQXhCLENBQU47QUFFQTFELHFEQUFNLENBQUM4RCxJQUFQLENBQVksR0FBWjtBQUNELEtBckNNO0FBdUNERyxpQkF2Q0MsZ0NBdUN5QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFWUCxzQkFBVSxTQUFWQSxNQUFVO0FBQUE7QUFBQSx1QkFDakI3RSw0Q0FBSyxDQUNmcUYsR0FEVSxDQUNOLFdBRE0sRUFFVmIsSUFGVSxDQUVMLFVBQUNDLEdBQUQsRUFBUztBQUNiSSx3QkFBTSxDQUFDLGFBQUQsRUFBZ0I7QUFDcEJ0QiwwQkFBTSxFQUFFa0I7QUFEWSxtQkFBaEIsQ0FBTjtBQUlBLHlCQUFPQSxHQUFQO0FBQ0QsaUJBUlUsV0FTSixVQUFDMUQsS0FBRDtBQUFBLHlCQUFXMkQsT0FBTyxDQUFDM0QsS0FBUixDQUFjQSxLQUFkLENBQVg7QUFBQSxpQkFUSSxDQURpQjs7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVy9CLEtBbERNO0FBb0REdUUsbUJBcERDLGtDQW9EMkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBVlQsc0JBQVUsU0FBVkEsTUFBVTtBQUFBO0FBQUEsdUJBQ25CN0UsNENBQUssQ0FDZnFGLEdBRFUsQ0FDTixpQkFETSxFQUVWYixJQUZVLENBRUwsVUFBQ0MsR0FBRCxFQUFTO0FBQ2JJLHdCQUFNLENBQUMsZUFBRCxFQUFrQkosR0FBRyxDQUFDTyxJQUF0QixDQUFOO0FBRUEseUJBQU9QLEdBQVA7QUFDRCxpQkFOVSxXQU9KLFVBQUMxRCxLQUFEO0FBQUEseUJBQVcyRCxPQUFPLENBQUMzRCxLQUFSLENBQWNBLEtBQWQsQ0FBWDtBQUFBLGlCQVBJLENBRG1COztBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTakM7QUE3RE0sR0FyQm1CO0FBcUY1QlAsU0FBTyxFQUFFO0FBQ1ArQyxVQURPLGtCQUNBRCxLQURBLEVBQ087QUFDWixhQUFPQSxLQUFLLENBQUNDLE1BQWI7QUFDRCxLQUhNO0FBS1BDLG1CQUxPLDJCQUtTRixLQUxULEVBS2dCO0FBQ3JCLGFBQU9BLEtBQUssQ0FBQ0UsZUFBYjtBQUNELEtBUE07QUFTUEMsYUFUTyxxQkFTR0gsS0FUSCxFQVNVO0FBQ2YsYUFBT0EsS0FBSyxDQUFDRyxTQUFiO0FBQ0QsS0FYTTtBQWFQaEQsYUFiTyx1QkFhSztBQUNWLGFBQU93QyxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsV0FBckIsQ0FBUDtBQUNEO0FBZk07QUFyRm1CLENBQWYsQ0FBZixFOzs7Ozs7Ozs7Ozs7QUNSQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBb0Y7QUFDM0I7QUFDTDs7O0FBR3BEO0FBQzZGO0FBQzdGLGdCQUFnQiwyR0FBVTtBQUMxQixFQUFFLDJFQUFNO0FBQ1IsRUFBRSxnRkFBTTtBQUNSLEVBQUUseUZBQWU7QUFDakI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDc0c7QUFDbEQ7QUFDQTtBQUNBO0FBQ0M7QUFDSztBQUNDO0FBQ0s7QUFDaEUsb0dBQWlCLGFBQWEsdUVBQUksQ0FBQyx1RUFBSSxDQUFDLHVFQUFJLENBQUMseUVBQUssQ0FBQyxtRkFBVSxDQUFDLGtGQUFRLENBQUMsNEZBQWEsQ0FBQzs7O0FBR3JGO0FBQ0EsSUFBSSxLQUFVLEVBQUUsWUFpQmY7QUFDRDtBQUNlLGdGOzs7Ozs7Ozs7Ozs7QUNsRGY7QUFBQTtBQUFBLHdDQUFxTCxDQUFnQixpUEFBRyxFQUFDLEM7Ozs7Ozs7Ozs7OztBQ0F6TTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFtRjtBQUMzQjtBQUNMOzs7QUFHbkQ7QUFDNkY7QUFDN0YsZ0JBQWdCLDJHQUFVO0FBQzFCLEVBQUUsMEVBQU07QUFDUixFQUFFLCtFQUFNO0FBQ1IsRUFBRSx3RkFBZTtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNzRztBQUNuRDtBQUNDO0FBQ007QUFDTDtBQUN3QjtBQUN6QjtBQUNwRCxvR0FBaUIsYUFBYSxzRUFBSSxDQUFDLHVFQUFJLENBQUMsbUZBQVUsQ0FBQyx5RUFBSyxDQUFDLDZHQUFpQixDQUFDLHVFQUFJLENBQUM7OztBQUdoRjtBQUNBLElBQUksS0FBVSxFQUFFLFlBaUJmO0FBQ0Q7QUFDZSxnRjs7Ozs7Ozs7Ozs7O0FDakRmO0FBQUE7QUFBQSx3Q0FBb0wsQ0FBZ0IsZ1BBQUcsRUFBQyxDOzs7Ozs7Ozs7Ozs7QUNBeE07QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQW1GO0FBQzNCO0FBQ0w7QUFDYzs7O0FBR2pFO0FBQzZGO0FBQzdGLGdCQUFnQiwyR0FBVTtBQUMxQixFQUFFLDBFQUFNO0FBQ1IsRUFBRSwrRUFBTTtBQUNSLEVBQUUsd0ZBQWU7QUFDakI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDc0c7QUFDbEQ7QUFDRDtBQUNDO0FBQ3BELG9HQUFpQixhQUFhLHVFQUFJLENBQUMsc0VBQUksQ0FBQyx1RUFBSSxDQUFDOzs7QUFHN0M7QUFDQSxJQUFJLEtBQVUsRUFBRSxZQWlCZjtBQUNEO0FBQ2UsZ0Y7Ozs7Ozs7Ozs7OztBQy9DZjtBQUFBO0FBQUEsd0NBQW9MLENBQWdCLGdQQUFHLEVBQUMsQzs7Ozs7Ozs7Ozs7O0FDQXhNO0FBQUE7QUFBQTtBQUFBO0FBQW1jLENBQWdCLHNkQUFHLEVBQUMsQzs7Ozs7Ozs7Ozs7O0FDQXZkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNhQTtBQUNBO0FBREEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNRQTtBQUtBO0FBQ0EsZ0JBREE7QUFFQSxnQkFGQTtBQUlBLE1BSkEsa0JBSUE7QUFDQTtBQUNBLGtCQURBO0FBRUEsa0JBRkE7QUFHQSx3QkFIQTtBQUlBO0FBSkE7QUFNQSxHQVhBO0FBYUE7QUFDQSxrQkFEQSw0QkFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBUkE7QUFTQSxrQkFUQSw0QkFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWZBLEdBYkE7QUErQkE7QUFDQTtBQUNBLGlGQURBO0FBRUEsaUZBRkE7QUFHQTtBQUhBLEtBREE7QUFNQTtBQUNBLGlGQURBO0FBRUE7QUFGQTtBQU5BLEdBL0JBO0FBMkNBO0FBQ0EsWUFEQSxzQkFDQTtBQUFBOztBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0EsK0JBREE7QUFFQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0EsT0FIQTtBQUlBO0FBbEJBO0FBM0NBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUVBO0FBQ0EsZ0JBREE7QUFFQSxnQkFGQTtBQUlBLE1BSkEsa0JBSUE7QUFDQTtBQUNBLGtCQURBO0FBRUEsa0JBRkE7QUFHQSx5QkFIQTtBQUlBO0FBSkE7QUFNQSxHQVhBO0FBWUE7QUFDQSxrQkFEQSw0QkFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBUkE7QUFVQSxrQkFWQSw0QkFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQWhCQTtBQWtCQSx5QkFsQkEsbUNBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBeEJBO0FBMEJBLDBCQTFCQSxvQ0EwQkE7QUFDQTtBQUNBLEtBNUJBO0FBOEJBLGlDQTlCQSwyQ0E4QkE7QUFDQTtBQUNBLEtBaENBO0FBa0NBLGlCQWxDQSwyQkFrQ0E7QUFDQTtBQUNBLEtBcENBO0FBc0NBLHdCQXRDQSxrQ0FzQ0E7QUFDQTtBQUNBO0FBeENBLEdBWkE7QUF1REE7QUFDQTtBQUNBLGlGQURBO0FBRUEsaUZBRkE7QUFHQTtBQUhBLEtBREE7QUFNQTtBQUNBLGlGQURBO0FBRUE7QUFGQSxLQU5BO0FBVUE7QUFDQSxpRkFEQTtBQUVBO0FBRkE7QUFWQSxHQXZEQTtBQXVFQTtBQUNBLFlBREEsc0JBQ0E7QUFBQTs7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBRUE7QUFDQSwrQkFEQTtBQUVBLCtCQUZBO0FBR0E7QUFIQTtBQUtBO0FBQ0E7QUFDQSxPQUZBO0FBR0E7QUFsQkE7QUF2RUEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDV0E7QUFDQSxlQURBO0FBRUEsb0JBRkE7QUFJQSxNQUpBLGtCQUlBO0FBQ0E7QUFDQTtBQURBO0FBR0EsR0FSQTtBQVVBO0FBQ0E7QUFBQTtBQUFBLEtBREE7QUFFQTtBQUFBO0FBQUEsS0FGQTtBQUdBO0FBQUE7QUFBQSxLQUhBO0FBSUE7QUFBQTtBQUFBLEtBSkE7QUFLQTtBQUFBO0FBQUEsS0FMQTtBQU1BO0FBQUE7QUFBQSxLQU5BO0FBT0E7QUFBQTtBQUFBLEtBUEE7QUFRQTtBQUFBO0FBQUEsS0FSQTtBQVNBO0FBQUE7QUFBQSxLQVRBO0FBVUE7QUFBQTtBQUFBO0FBVkE7QUFWQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNwQkE7QUFDQTtBQUVBO0FBQ0EsY0FEQTtBQUdBO0FBQ0EsYUFEQSx1QkFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBLEdBSEE7QUFVQTtBQUNBLGdGQURBO0FBRUE7QUFGQSxHQVZBO0FBZUE7QUFDQSxhQURBLHVCQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkEsR0FmQTtBQXNCQTtBQUNBO0FBQ0E7QUF4QkEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM0QkE7QUFDQSxjQURBO0FBR0E7QUFDQTtBQUFBO0FBQUEsS0FEQTtBQUVBO0FBQUE7QUFBQSxLQUZBO0FBR0E7QUFBQTtBQUFBLEtBSEE7QUFJQTtBQUFBO0FBQUEsS0FKQTtBQUtBO0FBQUE7QUFBQSxLQUxBO0FBTUE7QUFBQTtBQUFBLEtBTkE7QUFPQTtBQUFBO0FBQUEsS0FQQTtBQVFBO0FBQUE7QUFBQSxLQVJBO0FBU0E7QUFBQTtBQUFBLEtBVEE7QUFVQTtBQUFBO0FBQUE7QUFWQTtBQUhBLEc7Ozs7Ozs7Ozs7O0FDbERBLHVDOzs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLLHNCQUFzQjtBQUMzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLGlDQUFpQyxZQUFZLEVBQUU7QUFDNUQ7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLFNBQVMsWUFBWSxFQUFFO0FBQ3hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUNoQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSyw4QkFBOEI7QUFDbkM7QUFDQTtBQUNBO0FBQ0EsU0FBUyxTQUFTLHNDQUFzQyxFQUFFO0FBQzFEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0IsbUJBQW1CO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDL0hBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUssOEJBQThCO0FBQ25DO0FBQ0E7QUFDQTtBQUNBLFNBQVMsU0FBUyxzQ0FBc0MsRUFBRTtBQUMxRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLG1CQUFtQjtBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLHlCQUF5QjtBQUN0QztBQUNBLGlDQUFpQyxTQUFTLE1BQU0sZUFBZSxFQUFFLEVBQUU7QUFDbkU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDcExBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUssU0FBUyxrQ0FBa0MsRUFBRTtBQUNsRDtBQUNBO0FBQ0E7QUFDQSxTQUFTLFNBQVMsYUFBYSxFQUFFO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0IsV0FBVztBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0NBQWtDLHdDQUF3QztBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QjtBQUM3QjtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0EsdUNBQXVDLFNBQVMsMkJBQTJCLEVBQUU7QUFDN0U7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDLFNBQVMsd0JBQXdCLEVBQUU7QUFDMUU7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDLFNBQVMsMkJBQTJCLEVBQUU7QUFDN0U7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDLFNBQVMseUJBQXlCLEVBQUU7QUFDM0U7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDLFNBQVMseUJBQXlCLEVBQUU7QUFDM0U7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDLFNBQVMseUJBQXlCLEVBQUU7QUFDM0U7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDLFNBQVMsc0JBQXNCLEVBQUU7QUFDeEU7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDLFNBQVMsMkJBQTJCLEVBQUU7QUFDN0U7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDLFNBQVMseUJBQXlCLEVBQUU7QUFDM0U7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDLFNBQVMseUJBQXlCLEVBQUU7QUFDM0U7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUNqSUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxtQ0FBbUM7QUFDaEQ7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLFNBQVMscUNBQXFDLEVBQUU7QUFDakU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0NBQWtDO0FBQ2xDLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0Esb0NBQW9DO0FBQ3BDLDJCQUEyQjtBQUMzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCLFNBQVMsK0JBQStCLEVBQUU7QUFDbkU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDeERBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUssU0FBUyxrQ0FBa0MsRUFBRTtBQUNsRDtBQUNBO0FBQ0E7QUFDQSxTQUFTLFNBQVMsWUFBWSxFQUFFO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCLFNBQVMsYUFBYSxFQUFFO0FBQ25ELHlCQUF5Qix5QkFBeUI7QUFDbEQ7QUFDQSx5QkFBeUIseUJBQXlCO0FBQ2xEO0FBQ0E7QUFDQSwyQkFBMkIsU0FBUyxhQUFhLEVBQUU7QUFDbkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLFNBQVMsYUFBYSxFQUFFO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixTQUFTLGFBQWEsRUFBRTtBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsU0FBUyxhQUFhLEVBQUU7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLFNBQVMsYUFBYSxFQUFFO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVMsU0FBUyxZQUFZLEVBQUU7QUFDaEM7QUFDQTtBQUNBO0FBQ0EsYUFBYSw4QkFBOEIsYUFBYSxFQUFFO0FBQzFEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucyB9IGZyb20gXCIuL0FwcC52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9MTQ3ZjZiMGMmXCJcbmltcG9ydCBzY3JpcHQgZnJvbSBcIi4vQXBwLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIlxuZXhwb3J0ICogZnJvbSBcIi4vQXBwLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIlxuXG5cbi8qIG5vcm1hbGl6ZSBjb21wb25lbnQgKi9cbmltcG9ydCBub3JtYWxpemVyIGZyb20gXCIhLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3J1bnRpbWUvY29tcG9uZW50Tm9ybWFsaXplci5qc1wiXG52YXIgY29tcG9uZW50ID0gbm9ybWFsaXplcihcbiAgc2NyaXB0LFxuICByZW5kZXIsXG4gIHN0YXRpY1JlbmRlckZucyxcbiAgZmFsc2UsXG4gIG51bGwsXG4gIG51bGwsXG4gIG51bGxcbiAgXG4pXG5cbi8qIHZ1ZXRpZnktbG9hZGVyICovXG5pbXBvcnQgaW5zdGFsbENvbXBvbmVudHMgZnJvbSBcIiEuLi8uLi9ub2RlX21vZHVsZXMvdnVldGlmeS1sb2FkZXIvbGliL3J1bnRpbWUvaW5zdGFsbENvbXBvbmVudHMuanNcIlxuaW1wb3J0IHsgVkFwcCB9IGZyb20gJ3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVkFwcCc7XG5pbXBvcnQgeyBWQ29sIH0gZnJvbSAndnVldGlmeS9saWIvY29tcG9uZW50cy9WR3JpZCc7XG5pbXBvcnQgeyBWQ29udGFpbmVyIH0gZnJvbSAndnVldGlmeS9saWIvY29tcG9uZW50cy9WR3JpZCc7XG5pbXBvcnQgeyBWTWFpbiB9IGZyb20gJ3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVk1haW4nO1xuaW5zdGFsbENvbXBvbmVudHMoY29tcG9uZW50LCB7VkFwcCxWQ29sLFZDb250YWluZXIsVk1haW59KVxuXG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7XG4gIHZhciBhcGkgPSByZXF1aXJlKFwiL3Zhci93d3cvaHRtbC9ub2RlX21vZHVsZXMvdnVlLWhvdC1yZWxvYWQtYXBpL2Rpc3QvaW5kZXguanNcIilcbiAgYXBpLmluc3RhbGwocmVxdWlyZSgndnVlJykpXG4gIGlmIChhcGkuY29tcGF0aWJsZSkge1xuICAgIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgICBpZiAoIWFwaS5pc1JlY29yZGVkKCcxNDdmNmIwYycpKSB7XG4gICAgICBhcGkuY3JlYXRlUmVjb3JkKCcxNDdmNmIwYycsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVsb2FkKCcxNDdmNmIwYycsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH1cbiAgICBtb2R1bGUuaG90LmFjY2VwdChcIi4vQXBwLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0xNDdmNmIwYyZcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgYXBpLnJlcmVuZGVyKCcxNDdmNmIwYycsIHtcbiAgICAgICAgcmVuZGVyOiByZW5kZXIsXG4gICAgICAgIHN0YXRpY1JlbmRlckZuczogc3RhdGljUmVuZGVyRm5zXG4gICAgICB9KVxuICAgIH0pXG4gIH1cbn1cbmNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiYXNzZXRzL3Z1ZS9BcHAudnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0wLTAhLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9BcHAudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiOyBleHBvcnQgZGVmYXVsdCBtb2Q7IGV4cG9ydCAqIGZyb20gXCItIS4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTAtMCEuLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL0FwcC52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCIiLCJleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvbG9hZGVycy90ZW1wbGF0ZUxvYWRlci5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vQXBwLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0xNDdmNmIwYyZcIiIsImltcG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0gZnJvbSBcIi4vU2lnbmluLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0yYmNiZjFiMiZcIlxuaW1wb3J0IHNjcmlwdCBmcm9tIFwiLi9TaWduaW4udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9TaWduaW4udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5cblxuLyogbm9ybWFsaXplIGNvbXBvbmVudCAqL1xuaW1wb3J0IG5vcm1hbGl6ZXIgZnJvbSBcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvcnVudGltZS9jb21wb25lbnROb3JtYWxpemVyLmpzXCJcbnZhciBjb21wb25lbnQgPSBub3JtYWxpemVyKFxuICBzY3JpcHQsXG4gIHJlbmRlcixcbiAgc3RhdGljUmVuZGVyRm5zLFxuICBmYWxzZSxcbiAgbnVsbCxcbiAgbnVsbCxcbiAgbnVsbFxuICBcbilcblxuLyogdnVldGlmeS1sb2FkZXIgKi9cbmltcG9ydCBpbnN0YWxsQ29tcG9uZW50cyBmcm9tIFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWV0aWZ5LWxvYWRlci9saWIvcnVudGltZS9pbnN0YWxsQ29tcG9uZW50cy5qc1wiXG5pbXBvcnQgeyBWQnRuIH0gZnJvbSAndnVldGlmeS9saWIvY29tcG9uZW50cy9WQnRuJztcbmltcG9ydCB7IFZDYXJkIH0gZnJvbSAndnVldGlmeS9saWIvY29tcG9uZW50cy9WQ2FyZCc7XG5pbXBvcnQgeyBWQ2FyZEFjdGlvbnMgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZDYXJkJztcbmltcG9ydCB7IFZDYXJkVGV4dCB9IGZyb20gJ3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVkNhcmQnO1xuaW1wb3J0IHsgVkZvcm0gfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZGb3JtJztcbmltcG9ydCB7IFZTcGFjZXIgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZHcmlkJztcbmltcG9ydCB7IFZUZXh0RmllbGQgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZUZXh0RmllbGQnO1xuaW1wb3J0IHsgVlRvb2xiYXIgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZUb29sYmFyJztcbmltcG9ydCB7IFZUb29sYmFyVGl0bGUgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZUb29sYmFyJztcbmluc3RhbGxDb21wb25lbnRzKGNvbXBvbmVudCwge1ZCdG4sVkNhcmQsVkNhcmRBY3Rpb25zLFZDYXJkVGV4dCxWRm9ybSxWU3BhY2VyLFZUZXh0RmllbGQsVlRvb2xiYXIsVlRvb2xiYXJUaXRsZX0pXG5cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHtcbiAgdmFyIGFwaSA9IHJlcXVpcmUoXCIvdmFyL3d3dy9odG1sL25vZGVfbW9kdWxlcy92dWUtaG90LXJlbG9hZC1hcGkvZGlzdC9pbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghYXBpLmlzUmVjb3JkZWQoJzJiY2JmMWIyJykpIHtcbiAgICAgIGFwaS5jcmVhdGVSZWNvcmQoJzJiY2JmMWIyJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfSBlbHNlIHtcbiAgICAgIGFwaS5yZWxvYWQoJzJiY2JmMWIyJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfVxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiLi9TaWduaW4udnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTJiY2JmMWIyJlwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBhcGkucmVyZW5kZXIoJzJiY2JmMWIyJywge1xuICAgICAgICByZW5kZXI6IHJlbmRlcixcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnNcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxufVxuY29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJhc3NldHMvdnVlL2NvbXBvbmVudHMvYXV0aC9TaWduaW4udnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0wLTAhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9TaWduaW4udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiOyBleHBvcnQgZGVmYXVsdCBtb2Q7IGV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTAtMCEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1NpZ25pbi52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCIiLCJleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvbG9hZGVycy90ZW1wbGF0ZUxvYWRlci5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vU2lnbmluLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0yYmNiZjFiMiZcIiIsImltcG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0gZnJvbSBcIi4vU2lnbnVwLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0wMjlmNDRjNiZcIlxuaW1wb3J0IHNjcmlwdCBmcm9tIFwiLi9TaWdudXAudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9TaWdudXAudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5cblxuLyogbm9ybWFsaXplIGNvbXBvbmVudCAqL1xuaW1wb3J0IG5vcm1hbGl6ZXIgZnJvbSBcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvcnVudGltZS9jb21wb25lbnROb3JtYWxpemVyLmpzXCJcbnZhciBjb21wb25lbnQgPSBub3JtYWxpemVyKFxuICBzY3JpcHQsXG4gIHJlbmRlcixcbiAgc3RhdGljUmVuZGVyRm5zLFxuICBmYWxzZSxcbiAgbnVsbCxcbiAgbnVsbCxcbiAgbnVsbFxuICBcbilcblxuLyogdnVldGlmeS1sb2FkZXIgKi9cbmltcG9ydCBpbnN0YWxsQ29tcG9uZW50cyBmcm9tIFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWV0aWZ5LWxvYWRlci9saWIvcnVudGltZS9pbnN0YWxsQ29tcG9uZW50cy5qc1wiXG5pbXBvcnQgeyBWQnRuIH0gZnJvbSAndnVldGlmeS9saWIvY29tcG9uZW50cy9WQnRuJztcbmltcG9ydCB7IFZDYXJkIH0gZnJvbSAndnVldGlmeS9saWIvY29tcG9uZW50cy9WQ2FyZCc7XG5pbXBvcnQgeyBWQ2FyZEFjdGlvbnMgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZDYXJkJztcbmltcG9ydCB7IFZDYXJkVGV4dCB9IGZyb20gJ3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVkNhcmQnO1xuaW1wb3J0IHsgVkZvcm0gfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZGb3JtJztcbmltcG9ydCB7IFZQcm9ncmVzc0xpbmVhciB9IGZyb20gJ3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVlByb2dyZXNzTGluZWFyJztcbmltcG9ydCB7IFZTcGFjZXIgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZHcmlkJztcbmltcG9ydCB7IFZUZXh0RmllbGQgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZUZXh0RmllbGQnO1xuaW1wb3J0IHsgVlRvb2xiYXIgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZUb29sYmFyJztcbmltcG9ydCB7IFZUb29sYmFyVGl0bGUgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZUb29sYmFyJztcbmluc3RhbGxDb21wb25lbnRzKGNvbXBvbmVudCwge1ZCdG4sVkNhcmQsVkNhcmRBY3Rpb25zLFZDYXJkVGV4dCxWRm9ybSxWUHJvZ3Jlc3NMaW5lYXIsVlNwYWNlcixWVGV4dEZpZWxkLFZUb29sYmFyLFZUb29sYmFyVGl0bGV9KVxuXG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7XG4gIHZhciBhcGkgPSByZXF1aXJlKFwiL3Zhci93d3cvaHRtbC9ub2RlX21vZHVsZXMvdnVlLWhvdC1yZWxvYWQtYXBpL2Rpc3QvaW5kZXguanNcIilcbiAgYXBpLmluc3RhbGwocmVxdWlyZSgndnVlJykpXG4gIGlmIChhcGkuY29tcGF0aWJsZSkge1xuICAgIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgICBpZiAoIWFwaS5pc1JlY29yZGVkKCcwMjlmNDRjNicpKSB7XG4gICAgICBhcGkuY3JlYXRlUmVjb3JkKCcwMjlmNDRjNicsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVsb2FkKCcwMjlmNDRjNicsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH1cbiAgICBtb2R1bGUuaG90LmFjY2VwdChcIi4vU2lnbnVwLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0wMjlmNDRjNiZcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgYXBpLnJlcmVuZGVyKCcwMjlmNDRjNicsIHtcbiAgICAgICAgcmVuZGVyOiByZW5kZXIsXG4gICAgICAgIHN0YXRpY1JlbmRlckZuczogc3RhdGljUmVuZGVyRm5zXG4gICAgICB9KVxuICAgIH0pXG4gIH1cbn1cbmNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiYXNzZXRzL3Z1ZS9jb21wb25lbnRzL2F1dGgvU2lnbnVwLnZ1ZVwiXG5leHBvcnQgZGVmYXVsdCBjb21wb25lbnQuZXhwb3J0cyIsImltcG9ydCBtb2QgZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMC0wIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vU2lnbnVwLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIjsgZXhwb3J0IGRlZmF1bHQgbW9kOyBleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0wLTAhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9TaWdudXAudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiIiwiZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2xvYWRlcnMvdGVtcGxhdGVMb2FkZXIuanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1NpZ251cC52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9MDI5ZjQ0YzYmXCIiLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSdcbmltcG9ydCBBcHAgZnJvbSAnLi9BcHAnXG5pbXBvcnQgcm91dGVyIGZyb20gJy4vcm91dGVyJ1xuaW1wb3J0IGF4aW9zIGZyb20gJ2F4aW9zJ1xuaW1wb3J0IHN0b3JlIGZyb20gJy4vc3RvcmUvc3RvcmUnXG5pbXBvcnQgdnVldGlmeSBmcm9tICcuL3BsdWdpbnMvdnVldGlmeSdcbmltcG9ydCBWdWVsaWRhdGUgZnJvbSAndnVlbGlkYXRlJ1xuXG5WdWUudXNlKFZ1ZWxpZGF0ZSlcblxuYXhpb3MuZGVmYXVsdHMuYmFzZVVSTCA9IGBodHRwczovL2AgKyB3aW5kb3cubG9jYXRpb24uaG9zdG5hbWVcblxuY29uc3QgdG9rZW4gPSBzdG9yZS5nZXR0ZXJzLnVzZXJUb2tlblxuXG5pZiAodG9rZW4pIGF4aW9zLmludGVyY2VwdG9ycy5yZXF1ZXN0LnVzZSgoY29uZmlnKSA9PiB7XG4gIGNvbmZpZy5oZWFkZXJzLkF1dGhvcml6YXRpb24gPSBgQmVhcmVyICR7dG9rZW59YFxuXG4gIHJldHVybiBjb25maWdcbn0sIGVycm9yID0+IHtcbiAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKVxufSlcblxubmV3IFZ1ZSh7XG4gIGVsOiAnI2FwcCcsXG4gIHJvdXRlcixcbiAgc3RvcmUsXG4gIHZ1ZXRpZnksXG4gIFZ1ZWxpZGF0ZSxcbiAgcmVuZGVyOiBoID0+IGgoQXBwKSxcbiAgY29tcG9uZW50czogeyBBcHAgfSxcbiAgdGVtcGxhdGU6IFwiPEFwcC8+XCIsXG59KS4kbW91bnQoJyNhcHAnKVxuIiwiaW1wb3J0IFZ1ZSBmcm9tICd2dWUnXG5pbXBvcnQgVnVldGlmeSBmcm9tIFwidnVldGlmeVwiXG5pbXBvcnQgJ3Z1ZXRpZnkvZGlzdC92dWV0aWZ5Lm1pbi5jc3MnXG5cblZ1ZS51c2UoVnVldGlmeSlcblxuY29uc3Qgb3B0cyA9IHtcbiAgICB0aGVtZToge1xuICAgICAgICBkYXJrOiB0cnVlXG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBuZXcgVnVldGlmeShvcHRzKVxuIiwiaW1wb3J0IFZ1ZSBmcm9tIFwidnVlXCJcbmltcG9ydCBWdWVSb3V0ZXIgZnJvbSBcInZ1ZS1yb3V0ZXJcIlxuaW1wb3J0IHN0b3JlIGZyb20gJy4uL3N0b3JlL3N0b3JlJ1xuaW1wb3J0IEhvbWUgZnJvbSBcIi4uL3ZpZXdzL0hvbWVcIlxuaW1wb3J0IEF1dGggZnJvbSBcIi4uL3ZpZXdzL0F1dGhcIlxuaW1wb3J0IEFkbWluIGZyb20gXCIuLi92aWV3cy9BZG1pblwiXG4vLyBpbXBvcnQgYXV0aE1pZGRsZXdhcmUgZnJvbSBcIi4uL21pZGRsZXdhcmUvYXV0aFwiXG5cblZ1ZS51c2UoVnVlUm91dGVyKVxuXG5jb25zdCByb3V0ZXIgPSBuZXcgVnVlUm91dGVyKHtcbiAgbW9kZTogXCJoaXN0b3J5XCIsXG4gIHJvdXRlczogW1xuICAgIHsgcGF0aDogXCJcIiwgbmFtZTogJ0hvbWUnLCBjb21wb25lbnQ6IEhvbWUgfSxcbiAgICB7IHBhdGg6IFwiL2F1dGhcIiwgbmFtZTogJ0F1dGgnLCBjb21wb25lbnQ6IEF1dGggfSxcbiAgICB7IHBhdGg6IFwiL2FkbWluXCIsIG5hbWU6ICdBZG1pbicsIGNvbXBvbmVudDogQWRtaW4sIG1ldGE6IHsgXG4gICAgICByZXF1aXJlc0F1dGg6IHRydWVcbiAgICB9IH0sXG4gIF0sXG59KVxuXG5yb3V0ZXIuYmVmb3JlRWFjaCggKHRvLCBmcm9tLCBuZXh0KSA9PiB7XG4gIGlmICh0by5tYXRjaGVkLnNvbWUocmVjb3JkID0+IHJlY29yZC5tZXRhLnJlcXVpcmVzQXV0aCkpIHtcbiAgICBpZiAoIWxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VyVG9rZW4nKSB8fCBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlclRva2VuJykgPT0gdW5kZWZpbmVkKSB7XG4gICAgICBuZXh0KCB7IG5hbWU6ICdBdXRoJyB9IClcbiAgICB9IGVsc2Uge1xuICAgICAgbmV4dCgpXG4gICAgfVxuICB9IGVsc2UgbmV4dCgpXG59KVxuXG5leHBvcnQgZGVmYXVsdCByb3V0ZXJcbiIsImltcG9ydCBWdWUgZnJvbSAndnVlJ1xuaW1wb3J0IFZ1ZXggZnJvbSAndnVleCdcbi8vIGltcG9ydCB7IGF4aW9zIH0gZnJvbSAnLi4vcGx1Z2lucy9heGlvcydcbmltcG9ydCBheGlvcyBmcm9tICdheGlvcydcbmltcG9ydCByb3V0ZXIgZnJvbSAnLi4vcm91dGVyJ1xuXG5WdWUudXNlKFZ1ZXgpXG5cbmV4cG9ydCBkZWZhdWx0IG5ldyBWdWV4LlN0b3JlKHtcbiAgc3RhdGU6IHtcbiAgICBjdkRhdGE6IG51bGwsXG4gICAgYXV0aGVudGlmaWNhdGVkOiBmYWxzZSxcbiAgICB1c2VyY291bnQ6IG51bGwsXG4gIH0sXG5cbiAgbXV0YXRpb25zOiB7XG4gICAgU0VUX0NWX0RBVEEoc3RhdGUsIHBheWxvYWQpIHtcbiAgICAgIHN0YXRlLmN2RGF0YSA9IHBheWxvYWQuY3ZEYXRhO1xuICAgIH0sXG5cbiAgICBTRVRfQVVUSEVOVElGSUNBVEVEKHN0YXRlLCBwYXlsb2FkKSB7XG4gICAgICBzdGF0ZS5hdXRoZW50aWZpY2F0ZWQgPSBwYXlsb2FkLmF1dGhlbnRpZmljYXRlZFxuICAgIH0sXG5cbiAgICBTRVRfVVNFUkNPVU5UKHN0YXRlLCBwYXlsb2FkKSB7XG4gICAgICBzdGF0ZS51c2VyY291bnQgPSBwYXlsb2FkXG4gICAgfSxcbiAgfSxcblxuICBhY3Rpb25zOiB7XG4gICAgYXN5bmMgU0lHTlVQKHsgZGlzcGF0Y2ggfSwgYXV0aERhdGEpIHtcbiAgICAgIHJldHVybiBhd2FpdCBheGlvc1xuICAgICAgICAucG9zdChcIi9hcGkvdXNlci9uZXdcIiwge1xuICAgICAgICAgIHVzZXJuYW1lOiBhdXRoRGF0YS51c2VybmFtZSxcbiAgICAgICAgICBwYXNzd29yZDogYXV0aERhdGEucGFzc3dvcmQsXG4gICAgICAgICAgcm9sZVVzZXI6IGF1dGhEYXRhLnJvbGUsXG4gICAgICAgIH0pXG4gICAgICAgIC50aGVuKChyZXMpID0+IHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhyZXMpO1xuICAgICAgICAgIGRpc3BhdGNoKFwiTE9HSU5cIiwgYXV0aERhdGEpO1xuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuICAgIH0sXG5cbiAgICBhc3luYyBMT0dJTih7IGNvbW1pdCB9LCBhdXRoRGF0YSkge1xuICAgICAgY29uc3QgcmVzID0gYXdhaXQgYXhpb3MucG9zdChcIi9hcGkvYXV0aC9sb2dpbl9jaGVja1wiLCB7XG4gICAgICAgIHVzZXJuYW1lOiBhdXRoRGF0YS51c2VybmFtZSxcbiAgICAgICAgcGFzc3dvcmQ6IGF1dGhEYXRhLnBhc3N3b3JkLFxuICAgICAgfSlcblxuICAgICAgY29uc29sZS5pbmZvKHJlcylcbiAgICAgIFxuICAgICAgY29tbWl0KFwiU0VUX0FVVEhFTlRJRklDQVRFRFwiLCB0cnVlKVxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJ1c2VyVG9rZW5cIiwgcmVzLmRhdGEudG9rZW4pXG5cbiAgICAgIHJvdXRlci5wdXNoKFwiL2FkbWluXCIpXG4gICAgfSxcblxuICAgIExPR09VVCh7XG4gICAgICBjb21taXRcbiAgICB9KSB7XG4gICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSgndXNlclRva2VuJylcblxuICAgICAgY29tbWl0KFwiU0VUX0FVVEhFTlRJRklDQVRFRFwiLCBmYWxzZSlcblxuICAgICAgcm91dGVyLnB1c2goJy8nKVxuICAgIH0sXG5cbiAgICBhc3luYyBGRVRDSF9DVl9EQVRBKHsgY29tbWl0IH0pIHtcbiAgICAgIHJldHVybiBhd2FpdCBheGlvc1xuICAgICAgICAuZ2V0KFwiL2dldC9kYXRhXCIpXG4gICAgICAgIC50aGVuKChyZXMpID0+IHtcbiAgICAgICAgICBjb21taXQoXCJTRVRfQ1ZfREFUQVwiLCB7XG4gICAgICAgICAgICBjdkRhdGE6IHJlcyxcbiAgICAgICAgICB9KTtcblxuICAgICAgICAgIHJldHVybiByZXM7XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IGNvbnNvbGUuZXJyb3IoZXJyb3IpKVxuICAgIH0sXG5cbiAgICBhc3luYyBGRVRDSF9VU0VSQ09VTlQoeyBjb21taXQgfSkge1xuICAgICAgcmV0dXJuIGF3YWl0IGF4aW9zXG4gICAgICAgIC5nZXQoXCIvYXBpL3VzZXIvY291bnRcIilcbiAgICAgICAgLnRoZW4oKHJlcykgPT4ge1xuICAgICAgICAgIGNvbW1pdCgnU0VUX1VTRVJDT1VOVCcsIHJlcy5kYXRhKVxuXG4gICAgICAgICAgcmV0dXJuIHJlc1xuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiBjb25zb2xlLmVycm9yKGVycm9yKSlcbiAgICB9XG4gIH0sXG5cbiAgZ2V0dGVyczoge1xuICAgIGN2RGF0YShzdGF0ZSkge1xuICAgICAgcmV0dXJuIHN0YXRlLmN2RGF0YTtcbiAgICB9LFxuXG4gICAgYXV0aGVudGlmaWNhdGVkKHN0YXRlKSB7XG4gICAgICByZXR1cm4gc3RhdGUuYXV0aGVudGlmaWNhdGVkO1xuICAgIH0sXG5cbiAgICB1c2VyY291bnQoc3RhdGUpIHtcbiAgICAgIHJldHVybiBzdGF0ZS51c2VyY291bnQ7XG4gICAgfSxcblxuICAgIHVzZXJUb2tlbigpIHtcbiAgICAgIHJldHVybiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInVzZXJUb2tlblwiKVxuICAgIH1cbiAgfSxcbn0pO1xuIiwiaW1wb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSBmcm9tIFwiLi9BZG1pbi52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9NzRjMjUxNDcmXCJcbmltcG9ydCBzY3JpcHQgZnJvbSBcIi4vQWRtaW4udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9BZG1pbi52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcblxuXG4vKiBub3JtYWxpemUgY29tcG9uZW50ICovXG5pbXBvcnQgbm9ybWFsaXplciBmcm9tIFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanNcIlxudmFyIGNvbXBvbmVudCA9IG5vcm1hbGl6ZXIoXG4gIHNjcmlwdCxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZhbHNlLFxuICBudWxsLFxuICBudWxsLFxuICBudWxsXG4gIFxuKVxuXG4vKiB2dWV0aWZ5LWxvYWRlciAqL1xuaW1wb3J0IGluc3RhbGxDb21wb25lbnRzIGZyb20gXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZXRpZnktbG9hZGVyL2xpYi9ydW50aW1lL2luc3RhbGxDb21wb25lbnRzLmpzXCJcbmltcG9ydCB7IFZDb2wgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZHcmlkJztcbmltcG9ydCB7IFZSb3cgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZHcmlkJztcbmltcG9ydCB7IFZUYWIgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZUYWJzJztcbmltcG9ydCB7IFZUYWJzIH0gZnJvbSAndnVldGlmeS9saWIvY29tcG9uZW50cy9WVGFicyc7XG5pbXBvcnQgeyBWVGFic0l0ZW1zIH0gZnJvbSAndnVldGlmeS9saWIvY29tcG9uZW50cy9WVGFicyc7XG5pbXBvcnQgeyBWVG9vbGJhciB9IGZyb20gJ3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVlRvb2xiYXInO1xuaW1wb3J0IHsgVlRvb2xiYXJUaXRsZSB9IGZyb20gJ3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVlRvb2xiYXInO1xuaW5zdGFsbENvbXBvbmVudHMoY29tcG9uZW50LCB7VkNvbCxWUm93LFZUYWIsVlRhYnMsVlRhYnNJdGVtcyxWVG9vbGJhcixWVG9vbGJhclRpdGxlfSlcblxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkge1xuICB2YXIgYXBpID0gcmVxdWlyZShcIi92YXIvd3d3L2h0bWwvbm9kZV9tb2R1bGVzL3Z1ZS1ob3QtcmVsb2FkLWFwaS9kaXN0L2luZGV4LmpzXCIpXG4gIGFwaS5pbnN0YWxsKHJlcXVpcmUoJ3Z1ZScpKVxuICBpZiAoYXBpLmNvbXBhdGlibGUpIHtcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gICAgaWYgKCFhcGkuaXNSZWNvcmRlZCgnNzRjMjUxNDcnKSkge1xuICAgICAgYXBpLmNyZWF0ZVJlY29yZCgnNzRjMjUxNDcnLCBjb21wb25lbnQub3B0aW9ucylcbiAgICB9IGVsc2Uge1xuICAgICAgYXBpLnJlbG9hZCgnNzRjMjUxNDcnLCBjb21wb25lbnQub3B0aW9ucylcbiAgICB9XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIuL0FkbWluLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD03NGMyNTE0NyZcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgYXBpLnJlcmVuZGVyKCc3NGMyNTE0NycsIHtcbiAgICAgICAgcmVuZGVyOiByZW5kZXIsXG4gICAgICAgIHN0YXRpY1JlbmRlckZuczogc3RhdGljUmVuZGVyRm5zXG4gICAgICB9KVxuICAgIH0pXG4gIH1cbn1cbmNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiYXNzZXRzL3Z1ZS92aWV3cy9BZG1pbi52dWVcIlxuZXhwb3J0IGRlZmF1bHQgY29tcG9uZW50LmV4cG9ydHMiLCJpbXBvcnQgbW9kIGZyb20gXCItIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTAtMCEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL0FkbWluLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIjsgZXhwb3J0IGRlZmF1bHQgbW9kOyBleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0wLTAhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9BZG1pbi52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCIiLCJleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvbG9hZGVycy90ZW1wbGF0ZUxvYWRlci5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vQWRtaW4udnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTc0YzI1MTQ3JlwiIiwiaW1wb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSBmcm9tIFwiLi9BdXRoLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0wMzllNzU0MCZcIlxuaW1wb3J0IHNjcmlwdCBmcm9tIFwiLi9BdXRoLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIlxuZXhwb3J0ICogZnJvbSBcIi4vQXV0aC52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcblxuXG4vKiBub3JtYWxpemUgY29tcG9uZW50ICovXG5pbXBvcnQgbm9ybWFsaXplciBmcm9tIFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanNcIlxudmFyIGNvbXBvbmVudCA9IG5vcm1hbGl6ZXIoXG4gIHNjcmlwdCxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZhbHNlLFxuICBudWxsLFxuICBudWxsLFxuICBudWxsXG4gIFxuKVxuXG4vKiB2dWV0aWZ5LWxvYWRlciAqL1xuaW1wb3J0IGluc3RhbGxDb21wb25lbnRzIGZyb20gXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZXRpZnktbG9hZGVyL2xpYi9ydW50aW1lL2luc3RhbGxDb21wb25lbnRzLmpzXCJcbmltcG9ydCB7IFZBcHAgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZBcHAnO1xuaW1wb3J0IHsgVkNvbCB9IGZyb20gJ3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVkdyaWQnO1xuaW1wb3J0IHsgVkNvbnRhaW5lciB9IGZyb20gJ3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVkdyaWQnO1xuaW1wb3J0IHsgVk1haW4gfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZNYWluJztcbmltcG9ydCB7IFZQcm9ncmVzc0NpcmN1bGFyIH0gZnJvbSAndnVldGlmeS9saWIvY29tcG9uZW50cy9WUHJvZ3Jlc3NDaXJjdWxhcic7XG5pbXBvcnQgeyBWUm93IH0gZnJvbSAndnVldGlmeS9saWIvY29tcG9uZW50cy9WR3JpZCc7XG5pbnN0YWxsQ29tcG9uZW50cyhjb21wb25lbnQsIHtWQXBwLFZDb2wsVkNvbnRhaW5lcixWTWFpbixWUHJvZ3Jlc3NDaXJjdWxhcixWUm93fSlcblxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkge1xuICB2YXIgYXBpID0gcmVxdWlyZShcIi92YXIvd3d3L2h0bWwvbm9kZV9tb2R1bGVzL3Z1ZS1ob3QtcmVsb2FkLWFwaS9kaXN0L2luZGV4LmpzXCIpXG4gIGFwaS5pbnN0YWxsKHJlcXVpcmUoJ3Z1ZScpKVxuICBpZiAoYXBpLmNvbXBhdGlibGUpIHtcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gICAgaWYgKCFhcGkuaXNSZWNvcmRlZCgnMDM5ZTc1NDAnKSkge1xuICAgICAgYXBpLmNyZWF0ZVJlY29yZCgnMDM5ZTc1NDAnLCBjb21wb25lbnQub3B0aW9ucylcbiAgICB9IGVsc2Uge1xuICAgICAgYXBpLnJlbG9hZCgnMDM5ZTc1NDAnLCBjb21wb25lbnQub3B0aW9ucylcbiAgICB9XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIuL0F1dGgudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTAzOWU3NTQwJlwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBhcGkucmVyZW5kZXIoJzAzOWU3NTQwJywge1xuICAgICAgICByZW5kZXI6IHJlbmRlcixcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnNcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxufVxuY29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJhc3NldHMvdnVlL3ZpZXdzL0F1dGgudnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0wLTAhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9BdXRoLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIjsgZXhwb3J0IGRlZmF1bHQgbW9kOyBleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0wLTAhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9BdXRoLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIiIsImV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9sb2FkZXJzL3RlbXBsYXRlTG9hZGVyLmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9BdXRoLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0wMzllNzU0MCZcIiIsImltcG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0gZnJvbSBcIi4vSG9tZS52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9ZTg1MTJkZDImXCJcbmltcG9ydCBzY3JpcHQgZnJvbSBcIi4vSG9tZS52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcbmV4cG9ydCAqIGZyb20gXCIuL0hvbWUudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5pbXBvcnQgc3R5bGUwIGZyb20gXCIuL0hvbWUudnVlP3Z1ZSZ0eXBlPXN0eWxlJmluZGV4PTAmbGFuZz1zYXNzJlwiXG5cblxuLyogbm9ybWFsaXplIGNvbXBvbmVudCAqL1xuaW1wb3J0IG5vcm1hbGl6ZXIgZnJvbSBcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvcnVudGltZS9jb21wb25lbnROb3JtYWxpemVyLmpzXCJcbnZhciBjb21wb25lbnQgPSBub3JtYWxpemVyKFxuICBzY3JpcHQsXG4gIHJlbmRlcixcbiAgc3RhdGljUmVuZGVyRm5zLFxuICBmYWxzZSxcbiAgbnVsbCxcbiAgbnVsbCxcbiAgbnVsbFxuICBcbilcblxuLyogdnVldGlmeS1sb2FkZXIgKi9cbmltcG9ydCBpbnN0YWxsQ29tcG9uZW50cyBmcm9tIFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWV0aWZ5LWxvYWRlci9saWIvcnVudGltZS9pbnN0YWxsQ29tcG9uZW50cy5qc1wiXG5pbXBvcnQgeyBWQ29sIH0gZnJvbSAndnVldGlmeS9saWIvY29tcG9uZW50cy9WR3JpZCc7XG5pbXBvcnQgeyBWSW1nIH0gZnJvbSAndnVldGlmeS9saWIvY29tcG9uZW50cy9WSW1nJztcbmltcG9ydCB7IFZSb3cgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZHcmlkJztcbmluc3RhbGxDb21wb25lbnRzKGNvbXBvbmVudCwge1ZDb2wsVkltZyxWUm93fSlcblxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkge1xuICB2YXIgYXBpID0gcmVxdWlyZShcIi92YXIvd3d3L2h0bWwvbm9kZV9tb2R1bGVzL3Z1ZS1ob3QtcmVsb2FkLWFwaS9kaXN0L2luZGV4LmpzXCIpXG4gIGFwaS5pbnN0YWxsKHJlcXVpcmUoJ3Z1ZScpKVxuICBpZiAoYXBpLmNvbXBhdGlibGUpIHtcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gICAgaWYgKCFhcGkuaXNSZWNvcmRlZCgnZTg1MTJkZDInKSkge1xuICAgICAgYXBpLmNyZWF0ZVJlY29yZCgnZTg1MTJkZDInLCBjb21wb25lbnQub3B0aW9ucylcbiAgICB9IGVsc2Uge1xuICAgICAgYXBpLnJlbG9hZCgnZTg1MTJkZDInLCBjb21wb25lbnQub3B0aW9ucylcbiAgICB9XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIuL0hvbWUudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPWU4NTEyZGQyJlwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBhcGkucmVyZW5kZXIoJ2U4NTEyZGQyJywge1xuICAgICAgICByZW5kZXI6IHJlbmRlcixcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnNcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxufVxuY29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJhc3NldHMvdnVlL3ZpZXdzL0hvbWUudnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0wLTAhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Ib21lLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIjsgZXhwb3J0IGRlZmF1bHQgbW9kOyBleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0wLTAhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Ib21lLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIiIsImltcG9ydCBtb2QgZnJvbSBcIi0hLi4vLi4vLi4vbm9kZV9tb2R1bGVzL21pbmktY3NzLWV4dHJhY3QtcGx1Z2luL2Rpc3QvbG9hZGVyLmpzIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvY2pzLmpzPz9yZWYtLTQtb25lT2YtMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9sb2FkZXJzL3N0eWxlUG9zdExvYWRlci5qcyEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcmVzb2x2ZS11cmwtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTQtb25lT2YtMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9kaXN0L2Nqcy5qcz8/cmVmLS00LW9uZU9mLTEtMyEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL0hvbWUudnVlP3Z1ZSZ0eXBlPXN0eWxlJmluZGV4PTAmbGFuZz1zYXNzJlwiOyBleHBvcnQgZGVmYXVsdCBtb2Q7IGV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9taW5pLWNzcy1leHRyYWN0LXBsdWdpbi9kaXN0L2xvYWRlci5qcyEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9kaXN0L2Nqcy5qcz8/cmVmLS00LW9uZU9mLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvbG9hZGVycy9zdHlsZVBvc3RMb2FkZXIuanMhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Jlc29sdmUtdXJsLWxvYWRlci9pbmRleC5qcz8/cmVmLS00LW9uZU9mLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvZGlzdC9janMuanM/P3JlZi0tNC1vbmVPZi0xLTMhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Ib21lLnZ1ZT92dWUmdHlwZT1zdHlsZSZpbmRleD0wJmxhbmc9c2FzcyZcIiIsImV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9sb2FkZXJzL3RlbXBsYXRlTG9hZGVyLmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Ib21lLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD1lODUxMmRkMiZcIiIsIjx0ZW1wbGF0ZT5cbiAgPHYtYXBwIGNsYXNzPVwibWItNlwiPlxuICAgIDx2LW1haW4+XG4gICAgICA8di1jb2wgY29scz1cIjlcIiBjbGFzcz1cIm14LWF1dG9cIj5cbiAgICAgICAgPHYtY29udGFpbmVyIGZsdWlkPlxuICAgICAgICAgIDxyb3V0ZXItdmlldz48L3JvdXRlci12aWV3PlxuICAgICAgICA8L3YtY29udGFpbmVyPlxuICAgICAgPC92LWNvbD5cbiAgICA8L3YtbWFpbj5cbiAgPC92LWFwcD5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG4gIG5hbWU6IFwiQXBwXCIsXG59O1xuPC9zY3JpcHQ+XG4iLCI8dGVtcGxhdGU+XG4gICAgPHYtY2FyZCBjbGFzcz1cImVsZXZhdGlvbi0xMlwiPlxuICAgICAgICA8di10b29sYmFyIGNvbG9yPVwiaW5kaWdvXCIgZGFyayBmbGF0PlxuICAgICAgICAgICAgPHYtdG9vbGJhci10aXRsZT5LaWppUmVhZGVyIC0gTG9naW48L3YtdG9vbGJhci10aXRsZT5cbiAgICAgICAgICAgIDx2LXNwYWNlciAvPlxuICAgICAgICA8L3YtdG9vbGJhcj5cbiAgICAgICAgPHYtY2FyZC10ZXh0PlxuICAgICAgICAgICAgPHYtZm9ybSBpZD1cInNpZ25pbkZvcm1cIiBAa2V5dXAubmF0aXZlLmVudGVyPVwib25TdWJtaXRcIiBAc3VibWl0LnByZXZlbnQ9XCJvblN1Ym1pdFwiPlxuICAgICAgICAgICAgICAgIDx2LXRleHQtZmllbGQgaWQ9XCJ1c2VybmFtZVwiIGxhYmVsPVwiTG9naW5cIiBuYW1lPVwidXNlcm5hbWVcIiBwcmVwZW5kLWljb249XCJtZGktYWNjb3VudFwiIHR5cGU9XCJ0ZXh0XCIgdi1tb2RlbD1cInVzZXJuYW1lXCIgQGJsdXI9XCIkdi51c2VybmFtZS4kdG91Y2goKVwiIDplcnJvci1tZXNzYWdlcz1cInVzZXJuYW1lRXJyb3JzXCIgcGxhY2Vob2xkZXI9XCJFbnRlciB5b3VyIHVzZXJuYW1lXCIgcmVxdWlyZWQgYXV0b2ZvY3VzPjwvdi10ZXh0LWZpZWxkPlxuXG4gICAgICAgICAgICAgICAgPHYtdGV4dC1maWVsZCBpZD1cInBhc3N3b3JkXCIgbGFiZWw9XCJQYXNzd29yZFwiIG5hbWU9XCJwYXNzd29yZFwiIHByZXBlbmQtaWNvbj1cIm1kaS1sb2NrXCIgdHlwZT1cInBhc3N3b3JkXCIgdi1tb2RlbD1cInBhc3N3b3JkXCIgQGJsdXI9XCIkdi5wYXNzd29yZC4kdG91Y2goKVwiIDplcnJvci1tZXNzYWdlcz1cInBhc3N3b3JkRXJyb3JzXCIgcGxhY2Vob2xkZXI9XCJFbnRlciB5b3VyIHBhc3N3b3JkXCI+PC92LXRleHQtZmllbGQ+XG4gICAgICAgICAgICA8L3YtZm9ybT5cbiAgICAgICAgPC92LWNhcmQtdGV4dD5cbiAgICAgICAgPHYtY2FyZC1hY3Rpb25zPlxuICAgICAgICAgICAgPHYtc3BhY2VyIC8+XG4gICAgICAgICAgICA8di1idG4gZm9ybT1cInNpZ25pbkZvcm1cIiB0eXBlPVwic3VibWl0XCIgY29sb3I9XCJwcmltYXJ5XCIgOmRpc2FibGVkPVwiJHYuJGludmFsaWQgfHwgZGlzYWJsZWRcIiA6bG9hZGluZz1cImZvcm1Mb2FkaW5nXCI+TG9naW48L3YtYnRuPlxuICAgICAgICA8L3YtY2FyZC1hY3Rpb25zPlxuICAgIDwvdi1jYXJkPlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmltcG9ydCB7XG4gIHJlcXVpcmVkLFxuICBhbHBoYU51bSxcbiAgbWluTGVuZ3RoXG59IGZyb20gJ3Z1ZWxpZGF0ZS9saWIvdmFsaWRhdG9ycydcbmV4cG9ydCBkZWZhdWx0IHtcbiAgbmFtZTogJ1NpZ25pbicsXG4gIGxheW91dDogJ2F1dGgnLFxuXG4gIGRhdGEoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHVzZXJuYW1lOiAnJyxcbiAgICAgIHBhc3N3b3JkOiAnJyxcbiAgICAgIGZvcm1Mb2FkaW5nOiBmYWxzZSxcbiAgICAgIGRpc2FibGVkOiBmYWxzZVxuICAgIH1cbiAgfSxcblxuICBjb21wdXRlZDoge1xuICAgIHVzZXJuYW1lRXJyb3JzICgpIHtcbiAgICAgIGNvbnN0IGVycm9ycyA9IFtdXG4gICAgICBpZiAoIXRoaXMuJHYudXNlcm5hbWUuJGRpcnR5KSByZXR1cm4gZXJyb3JzXG4gICAgICAhdGhpcy4kdi51c2VybmFtZS5yZXF1aXJlZCAmJiBlcnJvcnMucHVzaCgnVXNlcm5hbWUgaXMgcmVxdWlyZWQuJylcbiAgICAgICF0aGlzLiR2LnVzZXJuYW1lLmFscGhhTnVtICYmIGVycm9ycy5wdXNoKCdVc2VybmFtZSBtdXN0IGJlIGFscGhhYmV0aWNhbCBhbmQvb3IgbnVtZXJpY2FsLicpXG4gICAgICAhdGhpcy4kdi51c2VybmFtZS5taW5MZW5ndGggJiYgZXJyb3JzLnB1c2goJ1VzZXJuYW1lIG11c3QgYmUgNCBjaGFyYWN0ZXJzIG1pbmltdW0uJylcbiAgICAgIHJldHVybiBlcnJvcnNcbiAgICB9LFxuICAgIHBhc3N3b3JkRXJyb3JzKCkge1xuICAgICAgY29uc3QgZXJyb3JzID0gW11cbiAgICAgIGlmICghdGhpcy4kdi5wYXNzd29yZC4kZGlydHkpIHJldHVybiBlcnJvcnNcbiAgICAgICF0aGlzLiR2LnBhc3N3b3JkLnJlcXVpcmVkICYmIGVycm9ycy5wdXNoKCdQYXNzd29yZCBpcyByZXF1aXJlZC4nKVxuICAgICAgIXRoaXMuJHYucGFzc3dvcmQubWluTGVuZ3RoICYmIGVycm9ycy5wdXNoKCdQYXNzd29yZCBtdXN0IGJlIDggY2hhcmFjdGVycyBtaW5pbXVtLicpXG4gICAgICByZXR1cm4gZXJyb3JzXG4gICAgfVxuICB9LFxuXG4gIHZhbGlkYXRpb25zOiB7XG4gICAgdXNlcm5hbWU6IHtcbiAgICAgIHJlcXVpcmVkLFxuICAgICAgYWxwaGFOdW0sXG4gICAgICBtaW5MZW5ndGg6IG1pbkxlbmd0aCg0KVxuICAgIH0sXG4gICAgcGFzc3dvcmQ6IHtcbiAgICAgIHJlcXVpcmVkLFxuICAgICAgbWluTGVuZ3RoOiBtaW5MZW5ndGgoNilcbiAgICB9XG4gIH0sXG5cbiAgbWV0aG9kczoge1xuICAgIG9uU3VibWl0KCkge1xuICAgICAgdGhpcy4kdi4kdG91Y2goKVxuICAgICAgdGhpcy5mb3JtTG9hZGluZyA9IHRydWVcbiAgICAgIHRoaXMuZGlzYWJsZWQgPSB0cnVlXG4gICAgICBpZiAodGhpcy4kdi4kZXJyb3IpIHtcbiAgICAgICAgdGhpcy5mb3JtTG9hZGluZyA9IGZhbHNlXG4gICAgICAgIHRoaXMuZGlzYWJsZWQgPSBmYWxzZVxuICAgICAgICByZXR1cm5cbiAgICAgIH1cbiAgICAgIGNvbnN0IGZvcm1EYXRhID0ge1xuICAgICAgICB1c2VybmFtZTogdGhpcy51c2VybmFtZSxcbiAgICAgICAgcGFzc3dvcmQ6IHRoaXMucGFzc3dvcmRcbiAgICAgIH1cbiAgICAgIHRoaXMuJHN0b3JlLmRpc3BhdGNoKCdMT0dJTicsIGZvcm1EYXRhKS50aGVuKCgpID0+IHtcbiAgICAgICAgdGhpcy5mb3JtTG9hZGluZyA9IGZhbHNlXG4gICAgICAgIHRoaXMuZGlzYWJsZWQgPSBmYWxzZVxuICAgICAgfSlcbiAgICB9XG4gIH0sXG59XG48L3NjcmlwdD5cbiIsIjx0ZW1wbGF0ZT5cbiAgPHYtY2FyZCBjbGFzcz1cImVsZXZhdGlvbi0xMlwiPlxuICAgIDx2LXRvb2xiYXIgY29sb3I9XCJpbmRpZ29cIiBkYXJrIGZsYXQ+XG4gICAgICA8di10b29sYmFyLXRpdGxlPlNpbXBsZSBDViAtIENyZWF0ZSBhbiBhY2NvdW50PC92LXRvb2xiYXItdGl0bGU+XG4gICAgICA8di1zcGFjZXIgLz5cbiAgICA8L3YtdG9vbGJhcj5cbiAgICA8di1jYXJkLXRleHQ+XG4gICAgICA8di1mb3JtIGlkPVwic2lnbnVwRm9ybVwiIEBrZXl1cC5uYXRpdmUuZW50ZXI9XCJvblN1Ym1pdFwiIEBzdWJtaXQucHJldmVudD1cIm9uU3VibWl0XCI+XG4gICAgICAgIDx2LXRleHQtZmllbGQgaWQ9XCJ1c2VybmFtZVwiIGxhYmVsPVwiTG9naW5cIiBuYW1lPVwidXNlcm5hbWVcIiBwcmVwZW5kLWljb249XCJtZGktYWNjb3VudFwiIHR5cGU9XCJ0ZXh0XCIgdi1tb2RlbD1cInVzZXJuYW1lXCIgQGJsdXI9XCIkdi51c2VybmFtZS4kdG91Y2goKVwiIDplcnJvci1tZXNzYWdlcz1cInVzZXJuYW1lRXJyb3JzXCIgcGxhY2Vob2xkZXI9XCJFbnRlciB5b3VyIHVzZXJuYW1lXCIgcmVxdWlyZWQgYXV0b2ZvY3VzPjwvdi10ZXh0LWZpZWxkPlxuXG4gICAgICAgIDx2LXRleHQtZmllbGQgaWQ9XCJwYXNzd29yZFwiIGxhYmVsPVwiUGFzc3dvcmRcIiBuYW1lPVwicGFzc3dvcmRcIiBwcmVwZW5kLWljb249XCJtZGktbG9ja1wiIHR5cGU9XCJwYXNzd29yZFwiIHYtbW9kZWw9XCJwYXNzd29yZFwiIEBibHVyPVwiJHYucGFzc3dvcmQuJHRvdWNoKClcIiA6ZXJyb3ItbWVzc2FnZXM9XCJwYXNzd29yZEVycm9yc1wiIHBsYWNlaG9sZGVyPVwiRW50ZXIgYSBwYXNzd29yZFwiIHJlcXVpcmVkPjwvdi10ZXh0LWZpZWxkPlxuICAgICAgICA8di1wcm9ncmVzcy1saW5lYXIgOnZhbHVlPVwicGFzc3dvcmRMZW5ndGhQcm9ncmVzc1wiIDpjb2xvcj1cImNvbG9yUGFzc3dvcmRcIiBoZWlnaHQ9XCI3XCI+PC92LXByb2dyZXNzLWxpbmVhcj5cblxuICAgICAgICA8di10ZXh0LWZpZWxkIGlkPVwicGFzc3dvcmRDb25maXJtXCIgbGFiZWw9XCJDb25maXJtIHRoZSBwYXNzd29yZFwiIG5hbWU9XCJwYXNzd29yZENvbmZpcm1cIiBwcmVwZW5kLWljb249XCJtZGktbG9ja1wiIHR5cGU9XCJwYXNzd29yZFwiIHYtbW9kZWw9XCJwYXNzd29yZENvbmZpcm1cIiBAYmx1cj1cIiR2LnBhc3N3b3JkQ29uZmlybS4kdG91Y2goKVwiIDplcnJvci1tZXNzYWdlcz1cInBhc3N3b3JkQ29uZmlybUVycm9yc1wiIHBsYWNlaG9sZGVyPVwiQ29uZmlybSB0aGUgcGFzc3dvcmRcIiByZXF1aXJlZD48L3YtdGV4dC1maWVsZD5cbiAgICAgICAgPHYtcHJvZ3Jlc3MtbGluZWFyIDp2YWx1ZT1cInBhc3N3b3JkQ29uZmlybUxlbmd0aFByb2dyZXNzXCIgOmNvbG9yPVwiY29sb3JQYXNzd29yZENvbmZpcm1cIiBoZWlnaHQ9XCI3XCIgLz5cbiAgICAgIDwvdi1mb3JtPlxuICAgIDwvdi1jYXJkLXRleHQ+XG4gICAgPHYtY2FyZC1hY3Rpb25zPlxuICAgICAgPHYtY2FyZC10ZXh0IGNsYXNzPVwiY2FwdGlvblwiPjxyb3V0ZXItbGluayA6dG89XCJ7IG5hbWU6ICdBdXRoJyB9XCI+TG9naW4gdG8geW91ciBhY2NvdW50PC9yb3V0ZXItbGluaz48L3YtY2FyZC10ZXh0PlxuICAgICAgPHYtc3BhY2VyPjwvdi1zcGFjZXI+XG4gICAgICA8di1idG4gZm9ybT1cInNpZ251cEZvcm1cIiB0eXBlPVwic3VibWl0XCIgY29sb3I9XCJwcmltYXJ5XCIgOmxvYWRpbmc9XCJsb2FkaW5nXCIgOmRpc2FibGVkPVwiJHYuJGludmFsaWRcIj5DcmVhdGUgYW4gYWNjb3VudDwvdi1idG4+XG4gICAgPC92LWNhcmQtYWN0aW9ucz5cbiAgPC92LWNhcmQ+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuaW1wb3J0IHsgcmVxdWlyZWQsIGFscGhhTnVtLCBlbWFpbCwgbWluTGVuZ3RoLCBzYW1lQXMgfSBmcm9tICd2dWVsaWRhdGUvbGliL3ZhbGlkYXRvcnMnXG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgbmFtZTogJ1NpZ251cCcsXG4gIGxheW91dDogJ2F1dGgnLFxuXG4gIGRhdGEoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHVzZXJuYW1lOiAnJyxcbiAgICAgIHBhc3N3b3JkOiAnJyxcbiAgICAgIHBhc3N3b3JkQ29uZmlybTogJycsXG4gICAgICBsb2FkaW5nOiBmYWxzZVxuICAgIH1cbiAgfSxcbiAgY29tcHV0ZWQ6IHtcbiAgICB1c2VybmFtZUVycm9ycyAoKSB7XG4gICAgICBjb25zdCBlcnJvcnMgPSBbXVxuICAgICAgaWYgKCF0aGlzLiR2LnVzZXJuYW1lLiRkaXJ0eSkgcmV0dXJuIGVycm9yc1xuICAgICAgIXRoaXMuJHYudXNlcm5hbWUucmVxdWlyZWQgJiYgZXJyb3JzLnB1c2goJ0FuIHVzZXJuYW1lIGlzIHJlcXVpcmVkLicpXG4gICAgICAhdGhpcy4kdi51c2VybmFtZS5hbHBoYU51bSAmJiBlcnJvcnMucHVzaCgnVGhlIHVzZXJuYW1lIG11c3QgYmUgYWxwaGFiZXRpY2FsIGFuZC9vciBudW1lcmljYWwuJylcbiAgICAgICF0aGlzLiR2LnVzZXJuYW1lLm1pbkxlbmd0aCAmJiBlcnJvcnMucHVzaCgnVGhlIHVzZXJuYW1lIG11c3QgYmUgNCBjaGFyYWN0ZXJzIG1pbmltdW0uJylcbiAgICAgIHJldHVybiBlcnJvcnNcbiAgICB9LFxuXG4gICAgcGFzc3dvcmRFcnJvcnMoKSB7XG4gICAgICAgIGNvbnN0IGVycm9ycyA9IFtdXG4gICAgICBpZiAoIXRoaXMuJHYucGFzc3dvcmQuJGRpcnR5KSByZXR1cm4gZXJyb3JzXG4gICAgICAhdGhpcy4kdi5wYXNzd29yZC5yZXF1aXJlZCAmJiBlcnJvcnMucHVzaCgnUGFzc3dvcmQgaXMgcmVxdWlyZWQuJylcbiAgICAgICF0aGlzLiR2LnBhc3N3b3JkLm1pbkxlbmd0aCAmJiBlcnJvcnMucHVzaCgnUGFzc3dvcmQgbXVzdCBiZSA4IGNoYXJhY3RlcnMgbWluaW11bS4nKVxuICAgICAgcmV0dXJuIGVycm9yc1xuICAgIH0sXG5cbiAgICBwYXNzd29yZENvbmZpcm1FcnJvcnMoKSB7XG4gICAgICBjb25zdCBlcnJvcnMgPSBbXVxuICAgICAgaWYgKCF0aGlzLiR2LnBhc3N3b3JkQ29uZmlybS4kZGlydHkpIHJldHVybiBlcnJvcnNcbiAgICAgICF0aGlzLiR2LnBhc3N3b3JkQ29uZmlybS5yZXF1aXJlZCAmJiBlcnJvcnMucHVzaCgnQ29uZmlybSB0aGUgcGFzc3dvcmQuJylcbiAgICAgICF0aGlzLiR2LnBhc3N3b3JkQ29uZmlybS5zYW1lQXNQYXNzd29yZCAmJiBlcnJvcnMucHVzaCgnVGhpcyBmaWVsZCBtdXN0IGJlIHRoZSBzYW1lIGFzIHRoZSBwYXNzd29yZCcpXG4gICAgICAgIHJldHVybiBlcnJvcnNcbiAgICB9LFxuXG4gICAgcGFzc3dvcmRMZW5ndGhQcm9ncmVzcygpIHtcbiAgICAgIHJldHVybiBNYXRoLm1pbigxMDAsIHRoaXMucGFzc3dvcmQubGVuZ3RoICogMTIuNSlcbiAgICB9LFxuXG4gICAgcGFzc3dvcmRDb25maXJtTGVuZ3RoUHJvZ3Jlc3MoKSB7XG4gICAgICByZXR1cm4gTWF0aC5taW4oMTAwLCB0aGlzLnBhc3N3b3JkQ29uZmlybS5sZW5ndGggKiAxMi41KVxuICAgIH0sXG5cbiAgICBjb2xvclBhc3N3b3JkKCkge1xuICAgICAgcmV0dXJuIFsnZXJyb3InLCAnd2FybmluZycsICdzdWNjZXNzJ11bTWF0aC5mbG9vcih0aGlzLnBhc3N3b3JkTGVuZ3RoUHJvZ3Jlc3MgLyA0MCldXG4gICAgfSxcblxuICAgIGNvbG9yUGFzc3dvcmRDb25maXJtKCkge1xuICAgICAgcmV0dXJuIFsnZXJyb3InLCAnd2FybmluZycsICdzdWNjZXNzJ11bTWF0aC5mbG9vcih0aGlzLnBhc3N3b3JkQ29uZmlybUxlbmd0aFByb2dyZXNzIC8gNDApXVxuICAgIH0sXG4gIH0sXG5cbiAgdmFsaWRhdGlvbnM6IHtcbiAgICB1c2VybmFtZToge1xuICAgICAgcmVxdWlyZWQsXG4gICAgICBhbHBoYU51bSxcbiAgICAgIG1pbkxlbmd0aDogbWluTGVuZ3RoKDQpXG4gICAgfSxcbiAgICBwYXNzd29yZDoge1xuICAgICAgcmVxdWlyZWQsXG4gICAgICBtaW5MZW5ndGg6IG1pbkxlbmd0aCg2KVxuICAgIH0sXG4gICAgcGFzc3dvcmRDb25maXJtOiB7XG4gICAgICByZXF1aXJlZCxcbiAgICAgIHNhbWVBc1Bhc3N3b3JkOiBzYW1lQXMoJ3Bhc3N3b3JkJylcbiAgICB9XG4gIH0sXG5cbiAgbWV0aG9kczoge1xuICAgIG9uU3VibWl0KCkge1xuICAgICAgdGhpcy4kdi4kdG91Y2goKVxuICAgICAgdGhpcy5sb2FkaW5nID0gIXRoaXMubG9hZGluZ1xuICAgICAgaWYgKHRoaXMuJHYuJGVycm9yKSB7XG4gICAgICAgIHRoaXMubG9hZGluZyA9ICF0aGlzLmxvYWRpbmdcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG4gICAgICBpZiAodGhpcy4kdi4kZXJyb3IpIHJldHVyblxuICAgICAgXG4gICAgICBjb25zdCBmb3JtRGF0YSA9IHtcbiAgICAgICAgdXNlcm5hbWU6IHRoaXMudXNlcm5hbWUsXG4gICAgICAgIHBhc3N3b3JkOiB0aGlzLnBhc3N3b3JkLFxuICAgICAgICByb2xlOiAnVXNlcidcbiAgICAgIH1cbiAgICAgIHRoaXMuJHN0b3JlLmRpc3BhdGNoKCdTSUdOVVAnLCBmb3JtRGF0YSkudGhlbigoKSA9PiB7XG4gICAgICAgIHRoaXMubG9hZGluZyA9ICF0aGlzLmxvYWRpbmdcbiAgICAgIH0pXG4gICAgfVxuICB9XG59XG48L3NjcmlwdD5cbiIsIjx0ZW1wbGF0ZT5cbiAgPHYtcm93IGFsaWduPVwidG9wXCIganVzdGlmeT1cImNlbnRlclwiPlxuICAgIDx2LWNvbCBjb2xzPVwiMTJcIj5cbiAgICAgIDx2LXRvb2xiYXIgY2xhc3M9XCJyb3VuZGVkLXQtbGdcIiBmbGF0PlxuICAgICAgICA8di10b29sYmFyLXRpdGxlPkFkbWluIENWPC92LXRvb2xiYXItdGl0bGU+XG5cbiAgICAgICAgPHRlbXBsYXRlIHYtc2xvdDpleHRlbnNpb24+XG4gICAgICAgICAgPHYtdGFicyBzbGlkZXItY29sb3I9XCJhbWJlclwiIHYtbW9kZWw9XCJ0YWJcIiBjZW50ZXJlZD5cbiAgICAgICAgICAgIDx2LXRhYiBocmVmPVwiI3RhYi1pbmZvcm1hdGlvblwiPk15IEluZm9ybWF0aW9uPC92LXRhYj5cbiAgICAgICAgICAgIDx2LXRhYiBocmVmPVwiI3RhYi1jb250YWN0c1wiPkNvbnRhY3RzPC92LXRhYj5cbiAgICAgICAgICAgIDx2LXRhYiBocmVmPVwiI3RhYi1leHBlcmllbmNlc1wiPkV4cGVyaWVuY2VzPC92LXRhYj5cbiAgICAgICAgICAgIDx2LXRhYiBocmVmPVwiI3RhYi10cmFpbmluZ3NcIj5UcmFpbmluZ3M8L3YtdGFiPlxuICAgICAgICAgICAgPHYtdGFiIGhyZWY9XCIjdGFiLXBvcnRmb2xpb1wiPlBvcnRmb2xpbzwvdi10YWI+XG4gICAgICAgICAgICA8di10YWIgaHJlZj1cIiN0YWItYXB0aXR1ZGVzXCI+QXB0aXR1ZGVzPC92LXRhYj5cbiAgICAgICAgICAgIDx2LXRhYiBocmVmPVwiI3RhYi1za2lsbHNcIj5Ta2lsbHM8L3YtdGFiPlxuICAgICAgICAgICAgPHYtdGFiIGhyZWY9XCIjdGFiLXNvZnQtc2tpbGxzXCI+U29mdCBza2lsbHM8L3YtdGFiPlxuICAgICAgICAgICAgPHYtdGFiIGhyZWY9XCIjdGFiLWludGVyZXN0c1wiPkludGVyZXN0czwvdi10YWI+XG4gICAgICAgICAgICA8di10YWIgaHJlZj1cIiN0YWItbGFuZ3VhZ2VzXCI+TGFuZ3VhZ2VzPC92LXRhYj5cbiAgICAgICAgICA8L3YtdGFicz5cbiAgICAgICAgPC90ZW1wbGF0ZT5cbiAgICAgIDwvdi10b29sYmFyPlxuXG4gICAgICA8di10YWJzLWl0ZW1zIHYtbW9kZWw9XCJ0YWJcIj5cbiAgICAgICAgPGFwcC1hZG1pbi1pbmZvcm1hdGlvbiAvPlxuICAgICAgICA8YXBwLWFkbWluLWNvbnRhY3RzIC8+XG4gICAgICAgIDxhcHAtYWRtaW4tZXhwZXJpZW5jZXMgLz5cbiAgICAgICAgPGFwcC1hZG1pbi10cmFpbmluZ3MgLz5cbiAgICAgICAgPGFwcC1hZG1pbi1wb3J0Zm9saW8gLz5cbiAgICAgICAgPGFwcC1hZG1pbi1hcHRpdHVkZXMgLz5cbiAgICAgICAgPGFwcC1hZG1pbi1za2lsbHMgLz5cbiAgICAgICAgPGFwcC1hZG1pbi1zb2Z0LXNraWxscyAvPlxuICAgICAgICA8YXBwLWFkbWluLWludGVyZXN0cyAvPlxuICAgICAgICA8YXBwLWFkbWluLWxhbmd1YWdlcyAvPlxuICAgICAgPC92LXRhYnMtaXRlbXM+XG4gICAgPC92LWNvbD5cbiAgPC92LXJvdz5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG4gIG5hbWU6IFwiQWRtaW5cIixcbiAgbWlkZGxld2FyZTogJ2F1dGgnLFxuXG4gIGRhdGEoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHRhYjogbnVsbFxuICAgIH1cbiAgfSxcblxuICBjb21wb25lbnRzOiB7XG4gICAgYXBwQWRtaW5JbmZvcm1hdGlvbjogKCkgPT4gaW1wb3J0KCcuLi9jb21wb25lbnRzL2FkbWluL0luZm9ybWF0aW9uJyksXG4gICAgYXBwQWRtaW5Db250YWN0czogKCkgPT4gaW1wb3J0KCcuLi9jb21wb25lbnRzL2FkbWluL0NvbnRhY3QnKSxcbiAgICBhcHBBZG1pbkV4cGVyaWVuY2VzOiAoKSA9PiBpbXBvcnQoJy4uL2NvbXBvbmVudHMvYWRtaW4vRXhwZXJpZW5jZScpLFxuICAgIGFwcEFkbWluVHJhaW5pbmdzOiAoKSA9PiBpbXBvcnQoJy4uL2NvbXBvbmVudHMvYWRtaW4vVHJhaW5pbmcnKSxcbiAgICBhcHBBZG1pblBvcnRmb2xpbzogKCkgPT4gaW1wb3J0KCcuLi9jb21wb25lbnRzL2FkbWluL1BvcnRmb2xpbycpLFxuICAgIGFwcEFkbWluQXB0aXR1ZGVzOiAoKSA9PiBpbXBvcnQoJy4uL2NvbXBvbmVudHMvYWRtaW4vQXB0aXR1ZGUnKSxcbiAgICBhcHBBZG1pblNraWxsczogKCkgPT4gaW1wb3J0KCcuLi9jb21wb25lbnRzL2FkbWluL1NraWxsJyksXG4gICAgYXBwQWRtaW5Tb2Z0U2tpbGxzOiAoKSA9PiBpbXBvcnQoJy4uL2NvbXBvbmVudHMvYWRtaW4vU29mdFNraWxsJyksXG4gICAgYXBwQWRtaW5JbnRlcmVzdHM6ICgpID0+IGltcG9ydCgnLi4vY29tcG9uZW50cy9hZG1pbi9JbnRlcmVzdCcpLFxuICAgIGFwcEFkbWluTGFuZ3VhZ2VzOiAoKSA9PiBpbXBvcnQoJy4uL2NvbXBvbmVudHMvYWRtaW4vTGFuZ3VhZ2UnKSxcbiAgfVxufVxuPC9zY3JpcHQ+XG4iLCI8dGVtcGxhdGU+XG4gIDx2LWFwcD5cbiAgICA8di1tYWluPlxuICAgICAgPHYtY29udGFpbmVyIGNsYXNzPVwiZmlsbC1oZWlnaHQgZmx1aWRcIj5cbiAgICAgICAgPHYtcm93IGFsaWduPVwiY2VudGVyXCIganVzdGlmeT1cImNlbnRlclwiPlxuICAgICAgICAgIDx2LWNvbCBjb2xzPVwiMTJcIiBzbT1cIjhcIiBtZD1cIjZcIiBjbGFzcz1cInRleHQtY2VudGVyXCIgdi1pZj1cInVzZXJjb3VudCA9PT0gbnVsbFwiPlxuICAgICAgICAgICAgPHYtcHJvZ3Jlc3MtY2lyY3VsYXIgaW5kZXRlcm1pbmF0ZSBjb2xvcj1cImFtYmVyXCIgLz5cbiAgICAgICAgICA8L3YtY29sPlxuICAgICAgICAgIDx2LWNvbCBjb2xzPVwiMTJcIiBzbT1cIjhcIiBtZD1cIjZcIiB2LWVsc2U+XG4gICAgICAgICAgICA8YXBwLXNpZ25pbiB2LWlmPVwidXNlcmNvdW50ID09IDFcIj48L2FwcC1zaWduaW4+XG4gICAgICAgICAgICA8YXBwLXNpZ251cCB2LWVsc2UgPjwvYXBwLXNpZ251cD5cbiAgICAgICAgICA8L3YtY29sPlxuICAgICAgICA8L3Ytcm93PlxuICAgICAgPC92LWNvbnRhaW5lcj5cbiAgICA8L3YtbWFpbj5cbiAgPC92LWFwcD5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5pbXBvcnQgU2lnbmluIGZyb20gJy4uL2NvbXBvbmVudHMvYXV0aC9TaWduaW4nXG5pbXBvcnQgU2lnbnVwIGZyb20gJy4uL2NvbXBvbmVudHMvYXV0aC9TaWdudXAnXG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgbmFtZTogXCJBdXRoXCIsXG5cbiAgY29tcHV0ZWQ6IHtcbiAgICB1c2VyY291bnQoKSB7XG4gICAgICBjb25zb2xlLmxvZyh0aGlzLiRzdG9yZS5nZXR0ZXJzLnVzZXJjb3VudClcbiAgICAgIHJldHVybiB0aGlzLiRzdG9yZS5nZXR0ZXJzLnVzZXJjb3VudFxuICAgIH1cbiAgfSxcblxuICBjb21wb25lbnRzOiB7XG4gICAgJ2FwcFNpZ25pbic6IFNpZ25pbixcbiAgICAnYXBwU2lnbnVwJzogU2lnbnVwXG4gIH0sXG5cbiAgbWV0aG9kczoge1xuICAgIGZldGNoRGF0YSgpIHtcbiAgICAgIGNvbnNvbGUubG9nKCdjaGVjayBkYXRhJylcbiAgICAgIHRoaXMuJHN0b3JlLmRpc3BhdGNoKCdGRVRDSF9VU0VSQ09VTlQnKVxuICAgIH1cbiAgfSxcblxuICBjcmVhdGVkOiBmdW5jdGlvbigpIHtcbiAgICB0aGlzLmZldGNoRGF0YSgpXG4gIH1cbn1cbjwvc2NyaXB0PlxuIiwiPHRlbXBsYXRlPlxuICA8di1yb3cgYWxpZ249XCJ0b3BcIiBqdXN0aWZ5PVwiY2VudGVyXCI+XG4gICAgPHYtY29sIGNvbHM9XCI5XCI+XG4gICAgICA8di1yb3c+XG4gICAgICAgIDx2LWNvbCBjb2xzPVwiMTJcIj5cbiAgICAgICAgICA8cCBjbGFzcz1cInRleHQtaDNcIj5Ob208L3A+XG4gICAgICAgICAgPHAgY2xhc3M9XCJ0ZXh0LWg0XCI+VGl0bGU8L3A+XG4gICAgICAgIDwvdi1jb2w+XG4gICAgICAgIDx2LWNvbCBjb2xzPVwiMTJcIj5cbiAgICAgICAgICA8YXBwLWNvbnRhY3QgLz5cbiAgICAgICAgPC92LWNvbD5cbiAgICAgIDwvdi1yb3c+XG5cbiAgICAgIDx2LXJvdz5cbiAgICAgICAgPHYtY29sIGNvbHM9XCIxMlwiPlxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0ZXh0LWgzIGZvbnQtd2VpZ2h0LWJvbGQgdGV4dC11cHBlcmNhc2Ugc2VjdGlvbi10aXRsZSBtYi0yXCI+RXhww6lyaWVuY2VzPC9kaXY+XG4gICAgICAgICAgPGFwcC1leHBlcmllbmNlIC8+XG4gICAgICAgIDwvdi1jb2w+XG5cbiAgICAgICAgPHYtY29sIGNvbHM9XCIxMlwiPlxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0ZXh0LWgzIGZvbnQtd2VpZ2h0LWJvbGQgdGV4dC11cHBlcmNhc2Ugc2VjdGlvbi10aXRsZSBtYi0yXCI+UG9ydGZvbGlvIHByb2Zlc3Npb25uZWw8L2Rpdj5cbiAgICAgICAgICA8YXBwLXBvcnRmb2xpby1wcm8gLz5cbiAgICAgICAgPC92LWNvbD5cblxuICAgICAgICA8di1jb2wgY29scz1cIjEyXCI+XG4gICAgICAgICAgPGRpdiBjbGFzcz1cInRleHQtaDMgZm9udC13ZWlnaHQtYm9sZCB0ZXh0LXVwcGVyY2FzZSBzZWN0aW9uLXRpdGxlIG1iLTJcIj5Qb3J0Zm9saW8gcGVyc29ubmVsPC9kaXY+XG4gICAgICAgICAgPGFwcC1wb3J0Zm9saW8tcGVyc28gLz5cbiAgICAgICAgPC92LWNvbD5cblxuICAgICAgICA8di1jb2wgY29scz1cIjEyXCI+XG4gICAgICAgICAgPGRpdiBjbGFzcz1cInRleHQtaDMgZm9udC13ZWlnaHQtYm9sZCB0ZXh0LXVwcGVyY2FzZSBzZWN0aW9uLXRpdGxlIG1iLTJcIj5Gb3JtYXRpb25zPC9kaXY+XG4gICAgICAgICAgPGFwcC10cmFpbmluZyAvPlxuICAgICAgICA8L3YtY29sPlxuICAgICAgPC92LXJvdz5cbiAgICA8L3YtY29sPlxuICAgIDx2LWNvbCBjb2xzPVwiM1wiPlxuICAgICAgPHYtY29sIGNvbHM9XCIxMlwiIGNsYXNzPVwibWItM1wiPlxuICAgICAgICA8di1pbWcgc3JjPVwiaHR0cHM6Ly93d3cuY2l0YXRpb25ib25oZXVyLmZyL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE3LzA4L3BsYWNlaG9sZGVyLTEwMjR4MTAyNC5wbmdcIiAvPlxuICAgICAgPC92LWNvbD5cblxuICAgICAgPGFwcC1sYW5ndWFnZSAvPlxuICAgICAgPGFwcC1za2lsbCAvPlxuICAgICAgPGFwcC1zb2Z0LXNraWxsIC8+XG4gICAgICA8YXBwLWludGVyZXN0IC8+XG4gICAgICA8YXBwLWV4dHJhIC8+XG4gICAgPC92LWNvbD5cbiAgPC92LXJvdz5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG4gIG5hbWU6IFwiSG9tZVwiLFxuXG4gIGNvbXBvbmVudHM6IHtcbiAgICBhcHBDb250YWN0OiAoKSA9PiBpbXBvcnQoJy4uL2NvbXBvbmVudHMvQ29udGFjdCcpLFxuICAgIGFwcEV4cGVyaWVuY2U6ICgpID0+IGltcG9ydCgnLi4vY29tcG9uZW50cy9FeHBlcmllbmNlJyksXG4gICAgYXBwUG9ydGZvbGlvUHJvOiAoKSA9PiBpbXBvcnQoJy4uL2NvbXBvbmVudHMvUG9ydGZvbGlvUHJvJyksXG4gICAgYXBwUG9ydGZvbGlvUGVyc286ICgpID0+IGltcG9ydCgnLi4vY29tcG9uZW50cy9Qb3J0Zm9saW9QZXJzbycpLFxuICAgIGFwcFRyYWluaW5nOiAoKSA9PiBpbXBvcnQoJy4uL2NvbXBvbmVudHMvVHJhaW5pbmcnKSxcbiAgICBhcHBMYW5ndWFnZTogKCkgPT4gaW1wb3J0KCcuLi9jb21wb25lbnRzL0xhbmd1YWdlJyksXG4gICAgYXBwU2tpbGw6ICgpID0+IGltcG9ydCgnLi4vY29tcG9uZW50cy9Ta2lsbCcpLFxuICAgIGFwcFNvZnRTa2lsbDogKCkgPT4gaW1wb3J0KCcuLi9jb21wb25lbnRzL1NvZnRTa2lsbCcpLFxuICAgIGFwcEludGVyZXN0OiAoKSA9PiBpbXBvcnQoJy4uL2NvbXBvbmVudHMvSW50ZXJlc3QnKSxcbiAgICBhcHBFeHRyYTogKCkgPT4gaW1wb3J0KCcuLi9jb21wb25lbnRzL0V4dHJhJyksXG4gIH1cbn07XG48L3NjcmlwdD5cblxuPHN0eWxlIGxhbmc9XCJzYXNzXCI+XG4uc2VjdGlvbi10aXRsZSB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBnb2xkO1xufVxuPC9zdHlsZT5cbiIsIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX2MoXG4gICAgXCJ2LWFwcFwiLFxuICAgIHsgc3RhdGljQ2xhc3M6IFwibWItNlwiIH0sXG4gICAgW1xuICAgICAgX2MoXG4gICAgICAgIFwidi1tYWluXCIsXG4gICAgICAgIFtcbiAgICAgICAgICBfYyhcbiAgICAgICAgICAgIFwidi1jb2xcIixcbiAgICAgICAgICAgIHsgc3RhdGljQ2xhc3M6IFwibXgtYXV0b1wiLCBhdHRyczogeyBjb2xzOiBcIjlcIiB9IH0sXG4gICAgICAgICAgICBbXG4gICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgIFwidi1jb250YWluZXJcIixcbiAgICAgICAgICAgICAgICB7IGF0dHJzOiB7IGZsdWlkOiBcIlwiIH0gfSxcbiAgICAgICAgICAgICAgICBbX2MoXCJyb3V0ZXItdmlld1wiKV0sXG4gICAgICAgICAgICAgICAgMVxuICAgICAgICAgICAgICApXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgMVxuICAgICAgICAgIClcbiAgICAgICAgXSxcbiAgICAgICAgMVxuICAgICAgKVxuICAgIF0sXG4gICAgMVxuICApXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW11cbnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuXG5leHBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucyB9IiwidmFyIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3ZtID0gdGhpc1xuICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gIHJldHVybiBfYyhcbiAgICBcInYtY2FyZFwiLFxuICAgIHsgc3RhdGljQ2xhc3M6IFwiZWxldmF0aW9uLTEyXCIgfSxcbiAgICBbXG4gICAgICBfYyhcbiAgICAgICAgXCJ2LXRvb2xiYXJcIixcbiAgICAgICAgeyBhdHRyczogeyBjb2xvcjogXCJpbmRpZ29cIiwgZGFyazogXCJcIiwgZmxhdDogXCJcIiB9IH0sXG4gICAgICAgIFtcbiAgICAgICAgICBfYyhcInYtdG9vbGJhci10aXRsZVwiLCBbX3ZtLl92KFwiS2lqaVJlYWRlciAtIExvZ2luXCIpXSksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcInYtc3BhY2VyXCIpXG4gICAgICAgIF0sXG4gICAgICAgIDFcbiAgICAgICksXG4gICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgX2MoXG4gICAgICAgIFwidi1jYXJkLXRleHRcIixcbiAgICAgICAgW1xuICAgICAgICAgIF9jKFxuICAgICAgICAgICAgXCJ2LWZvcm1cIixcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgYXR0cnM6IHsgaWQ6IFwic2lnbmluRm9ybVwiIH0sXG4gICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgc3VibWl0OiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLm9uU3VibWl0KCRldmVudClcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIG5hdGl2ZU9uOiB7XG4gICAgICAgICAgICAgICAga2V5dXA6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgaWYgKFxuICAgICAgICAgICAgICAgICAgICAhJGV2ZW50LnR5cGUuaW5kZXhPZihcImtleVwiKSAmJlxuICAgICAgICAgICAgICAgICAgICBfdm0uX2soJGV2ZW50LmtleUNvZGUsIFwiZW50ZXJcIiwgMTMsICRldmVudC5rZXksIFwiRW50ZXJcIilcbiAgICAgICAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbnVsbFxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgcmV0dXJuIF92bS5vblN1Ym1pdCgkZXZlbnQpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgW1xuICAgICAgICAgICAgICBfYyhcInYtdGV4dC1maWVsZFwiLCB7XG4gICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgIGlkOiBcInVzZXJuYW1lXCIsXG4gICAgICAgICAgICAgICAgICBsYWJlbDogXCJMb2dpblwiLFxuICAgICAgICAgICAgICAgICAgbmFtZTogXCJ1c2VybmFtZVwiLFxuICAgICAgICAgICAgICAgICAgXCJwcmVwZW5kLWljb25cIjogXCJtZGktYWNjb3VudFwiLFxuICAgICAgICAgICAgICAgICAgdHlwZTogXCJ0ZXh0XCIsXG4gICAgICAgICAgICAgICAgICBcImVycm9yLW1lc3NhZ2VzXCI6IF92bS51c2VybmFtZUVycm9ycyxcbiAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBcIkVudGVyIHlvdXIgdXNlcm5hbWVcIixcbiAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBcIlwiLFxuICAgICAgICAgICAgICAgICAgYXV0b2ZvY3VzOiBcIlwiXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgYmx1cjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfdm0uJHYudXNlcm5hbWUuJHRvdWNoKClcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIG1vZGVsOiB7XG4gICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLnVzZXJuYW1lLFxuICAgICAgICAgICAgICAgICAgY2FsbGJhY2s6IGZ1bmN0aW9uKCQkdikge1xuICAgICAgICAgICAgICAgICAgICBfdm0udXNlcm5hbWUgPSAkJHZcbiAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcInVzZXJuYW1lXCJcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICBfYyhcInYtdGV4dC1maWVsZFwiLCB7XG4gICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgIGlkOiBcInBhc3N3b3JkXCIsXG4gICAgICAgICAgICAgICAgICBsYWJlbDogXCJQYXNzd29yZFwiLFxuICAgICAgICAgICAgICAgICAgbmFtZTogXCJwYXNzd29yZFwiLFxuICAgICAgICAgICAgICAgICAgXCJwcmVwZW5kLWljb25cIjogXCJtZGktbG9ja1wiLFxuICAgICAgICAgICAgICAgICAgdHlwZTogXCJwYXNzd29yZFwiLFxuICAgICAgICAgICAgICAgICAgXCJlcnJvci1tZXNzYWdlc1wiOiBfdm0ucGFzc3dvcmRFcnJvcnMsXG4gICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcjogXCJFbnRlciB5b3VyIHBhc3N3b3JkXCJcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICBibHVyOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF92bS4kdi5wYXNzd29yZC4kdG91Y2goKVxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgbW9kZWw6IHtcbiAgICAgICAgICAgICAgICAgIHZhbHVlOiBfdm0ucGFzc3dvcmQsXG4gICAgICAgICAgICAgICAgICBjYWxsYmFjazogZnVuY3Rpb24oJCR2KSB7XG4gICAgICAgICAgICAgICAgICAgIF92bS5wYXNzd29yZCA9ICQkdlxuICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246IFwicGFzc3dvcmRcIlxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAxXG4gICAgICAgICAgKVxuICAgICAgICBdLFxuICAgICAgICAxXG4gICAgICApLFxuICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgIF9jKFxuICAgICAgICBcInYtY2FyZC1hY3Rpb25zXCIsXG4gICAgICAgIFtcbiAgICAgICAgICBfYyhcInYtc3BhY2VyXCIpLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX2MoXG4gICAgICAgICAgICBcInYtYnRuXCIsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgZm9ybTogXCJzaWduaW5Gb3JtXCIsXG4gICAgICAgICAgICAgICAgdHlwZTogXCJzdWJtaXRcIixcbiAgICAgICAgICAgICAgICBjb2xvcjogXCJwcmltYXJ5XCIsXG4gICAgICAgICAgICAgICAgZGlzYWJsZWQ6IF92bS4kdi4kaW52YWxpZCB8fCBfdm0uZGlzYWJsZWQsXG4gICAgICAgICAgICAgICAgbG9hZGluZzogX3ZtLmZvcm1Mb2FkaW5nXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBbX3ZtLl92KFwiTG9naW5cIildXG4gICAgICAgICAgKVxuICAgICAgICBdLFxuICAgICAgICAxXG4gICAgICApXG4gICAgXSxcbiAgICAxXG4gIClcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5cbmV4cG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0iLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFxuICAgIFwidi1jYXJkXCIsXG4gICAgeyBzdGF0aWNDbGFzczogXCJlbGV2YXRpb24tMTJcIiB9LFxuICAgIFtcbiAgICAgIF9jKFxuICAgICAgICBcInYtdG9vbGJhclwiLFxuICAgICAgICB7IGF0dHJzOiB7IGNvbG9yOiBcImluZGlnb1wiLCBkYXJrOiBcIlwiLCBmbGF0OiBcIlwiIH0gfSxcbiAgICAgICAgW1xuICAgICAgICAgIF9jKFwidi10b29sYmFyLXRpdGxlXCIsIFtfdm0uX3YoXCJTaW1wbGUgQ1YgLSBDcmVhdGUgYW4gYWNjb3VudFwiKV0pLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX2MoXCJ2LXNwYWNlclwiKVxuICAgICAgICBdLFxuICAgICAgICAxXG4gICAgICApLFxuICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgIF9jKFxuICAgICAgICBcInYtY2FyZC10ZXh0XCIsXG4gICAgICAgIFtcbiAgICAgICAgICBfYyhcbiAgICAgICAgICAgIFwidi1mb3JtXCIsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGF0dHJzOiB7IGlkOiBcInNpZ251cEZvcm1cIiB9LFxuICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgIHN1Ym1pdDogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgICAgcmV0dXJuIF92bS5vblN1Ym1pdCgkZXZlbnQpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBuYXRpdmVPbjoge1xuICAgICAgICAgICAgICAgIGtleXVwOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgICAgICAgISRldmVudC50eXBlLmluZGV4T2YoXCJrZXlcIikgJiZcbiAgICAgICAgICAgICAgICAgICAgX3ZtLl9rKCRldmVudC5rZXlDb2RlLCBcImVudGVyXCIsIDEzLCAkZXZlbnQua2V5LCBcIkVudGVyXCIpXG4gICAgICAgICAgICAgICAgICApIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG51bGxcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIHJldHVybiBfdm0ub25TdWJtaXQoJGV2ZW50KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgX2MoXCJ2LXRleHQtZmllbGRcIiwge1xuICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICBpZDogXCJ1c2VybmFtZVwiLFxuICAgICAgICAgICAgICAgICAgbGFiZWw6IFwiTG9naW5cIixcbiAgICAgICAgICAgICAgICAgIG5hbWU6IFwidXNlcm5hbWVcIixcbiAgICAgICAgICAgICAgICAgIFwicHJlcGVuZC1pY29uXCI6IFwibWRpLWFjY291bnRcIixcbiAgICAgICAgICAgICAgICAgIHR5cGU6IFwidGV4dFwiLFxuICAgICAgICAgICAgICAgICAgXCJlcnJvci1tZXNzYWdlc1wiOiBfdm0udXNlcm5hbWVFcnJvcnMsXG4gICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcjogXCJFbnRlciB5b3VyIHVzZXJuYW1lXCIsXG4gICAgICAgICAgICAgICAgICByZXF1aXJlZDogXCJcIixcbiAgICAgICAgICAgICAgICAgIGF1dG9mb2N1czogXCJcIlxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgICAgIGJsdXI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLiR2LnVzZXJuYW1lLiR0b3VjaCgpXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBtb2RlbDoge1xuICAgICAgICAgICAgICAgICAgdmFsdWU6IF92bS51c2VybmFtZSxcbiAgICAgICAgICAgICAgICAgIGNhbGxiYWNrOiBmdW5jdGlvbigkJHYpIHtcbiAgICAgICAgICAgICAgICAgICAgX3ZtLnVzZXJuYW1lID0gJCR2XG4gICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgZXhwcmVzc2lvbjogXCJ1c2VybmFtZVwiXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgX2MoXCJ2LXRleHQtZmllbGRcIiwge1xuICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICBpZDogXCJwYXNzd29yZFwiLFxuICAgICAgICAgICAgICAgICAgbGFiZWw6IFwiUGFzc3dvcmRcIixcbiAgICAgICAgICAgICAgICAgIG5hbWU6IFwicGFzc3dvcmRcIixcbiAgICAgICAgICAgICAgICAgIFwicHJlcGVuZC1pY29uXCI6IFwibWRpLWxvY2tcIixcbiAgICAgICAgICAgICAgICAgIHR5cGU6IFwicGFzc3dvcmRcIixcbiAgICAgICAgICAgICAgICAgIFwiZXJyb3ItbWVzc2FnZXNcIjogX3ZtLnBhc3N3b3JkRXJyb3JzLFxuICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IFwiRW50ZXIgYSBwYXNzd29yZFwiLFxuICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IFwiXCJcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICBibHVyOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF92bS4kdi5wYXNzd29yZC4kdG91Y2goKVxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgbW9kZWw6IHtcbiAgICAgICAgICAgICAgICAgIHZhbHVlOiBfdm0ucGFzc3dvcmQsXG4gICAgICAgICAgICAgICAgICBjYWxsYmFjazogZnVuY3Rpb24oJCR2KSB7XG4gICAgICAgICAgICAgICAgICAgIF92bS5wYXNzd29yZCA9ICQkdlxuICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246IFwicGFzc3dvcmRcIlxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgIF9jKFwidi1wcm9ncmVzcy1saW5lYXJcIiwge1xuICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLnBhc3N3b3JkTGVuZ3RoUHJvZ3Jlc3MsXG4gICAgICAgICAgICAgICAgICBjb2xvcjogX3ZtLmNvbG9yUGFzc3dvcmQsXG4gICAgICAgICAgICAgICAgICBoZWlnaHQ6IFwiN1wiXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgX2MoXCJ2LXRleHQtZmllbGRcIiwge1xuICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICBpZDogXCJwYXNzd29yZENvbmZpcm1cIixcbiAgICAgICAgICAgICAgICAgIGxhYmVsOiBcIkNvbmZpcm0gdGhlIHBhc3N3b3JkXCIsXG4gICAgICAgICAgICAgICAgICBuYW1lOiBcInBhc3N3b3JkQ29uZmlybVwiLFxuICAgICAgICAgICAgICAgICAgXCJwcmVwZW5kLWljb25cIjogXCJtZGktbG9ja1wiLFxuICAgICAgICAgICAgICAgICAgdHlwZTogXCJwYXNzd29yZFwiLFxuICAgICAgICAgICAgICAgICAgXCJlcnJvci1tZXNzYWdlc1wiOiBfdm0ucGFzc3dvcmRDb25maXJtRXJyb3JzLFxuICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IFwiQ29uZmlybSB0aGUgcGFzc3dvcmRcIixcbiAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBcIlwiXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgYmx1cjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfdm0uJHYucGFzc3dvcmRDb25maXJtLiR0b3VjaCgpXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBtb2RlbDoge1xuICAgICAgICAgICAgICAgICAgdmFsdWU6IF92bS5wYXNzd29yZENvbmZpcm0sXG4gICAgICAgICAgICAgICAgICBjYWxsYmFjazogZnVuY3Rpb24oJCR2KSB7XG4gICAgICAgICAgICAgICAgICAgIF92bS5wYXNzd29yZENvbmZpcm0gPSAkJHZcbiAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcInBhc3N3b3JkQ29uZmlybVwiXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgX2MoXCJ2LXByb2dyZXNzLWxpbmVhclwiLCB7XG4gICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgIHZhbHVlOiBfdm0ucGFzc3dvcmRDb25maXJtTGVuZ3RoUHJvZ3Jlc3MsXG4gICAgICAgICAgICAgICAgICBjb2xvcjogX3ZtLmNvbG9yUGFzc3dvcmRDb25maXJtLFxuICAgICAgICAgICAgICAgICAgaGVpZ2h0OiBcIjdcIlxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAxXG4gICAgICAgICAgKVxuICAgICAgICBdLFxuICAgICAgICAxXG4gICAgICApLFxuICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgIF9jKFxuICAgICAgICBcInYtY2FyZC1hY3Rpb25zXCIsXG4gICAgICAgIFtcbiAgICAgICAgICBfYyhcbiAgICAgICAgICAgIFwidi1jYXJkLXRleHRcIixcbiAgICAgICAgICAgIHsgc3RhdGljQ2xhc3M6IFwiY2FwdGlvblwiIH0sXG4gICAgICAgICAgICBbXG4gICAgICAgICAgICAgIF9jKFwicm91dGVyLWxpbmtcIiwgeyBhdHRyczogeyB0bzogeyBuYW1lOiBcIkF1dGhcIiB9IH0gfSwgW1xuICAgICAgICAgICAgICAgIF92bS5fdihcIkxvZ2luIHRvIHlvdXIgYWNjb3VudFwiKVxuICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIDFcbiAgICAgICAgICApLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX2MoXCJ2LXNwYWNlclwiKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFxuICAgICAgICAgICAgXCJ2LWJ0blwiLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgIGZvcm06IFwic2lnbnVwRm9ybVwiLFxuICAgICAgICAgICAgICAgIHR5cGU6IFwic3VibWl0XCIsXG4gICAgICAgICAgICAgICAgY29sb3I6IFwicHJpbWFyeVwiLFxuICAgICAgICAgICAgICAgIGxvYWRpbmc6IF92bS5sb2FkaW5nLFxuICAgICAgICAgICAgICAgIGRpc2FibGVkOiBfdm0uJHYuJGludmFsaWRcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFtfdm0uX3YoXCJDcmVhdGUgYW4gYWNjb3VudFwiKV1cbiAgICAgICAgICApXG4gICAgICAgIF0sXG4gICAgICAgIDFcbiAgICAgIClcbiAgICBdLFxuICAgIDFcbiAgKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtdXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcblxuZXhwb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX2MoXG4gICAgXCJ2LXJvd1wiLFxuICAgIHsgYXR0cnM6IHsgYWxpZ246IFwidG9wXCIsIGp1c3RpZnk6IFwiY2VudGVyXCIgfSB9LFxuICAgIFtcbiAgICAgIF9jKFxuICAgICAgICBcInYtY29sXCIsXG4gICAgICAgIHsgYXR0cnM6IHsgY29sczogXCIxMlwiIH0gfSxcbiAgICAgICAgW1xuICAgICAgICAgIF9jKFxuICAgICAgICAgICAgXCJ2LXRvb2xiYXJcIixcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwicm91bmRlZC10LWxnXCIsXG4gICAgICAgICAgICAgIGF0dHJzOiB7IGZsYXQ6IFwiXCIgfSxcbiAgICAgICAgICAgICAgc2NvcGVkU2xvdHM6IF92bS5fdShbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAga2V5OiBcImV4dGVuc2lvblwiLFxuICAgICAgICAgICAgICAgICAgZm46IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gW1xuICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJ2LXRhYnNcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHsgXCJzbGlkZXItY29sb3JcIjogXCJhbWJlclwiLCBjZW50ZXJlZDogXCJcIiB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICBtb2RlbDoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBfdm0udGFiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrOiBmdW5jdGlvbigkJHYpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS50YWIgPSAkJHZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246IFwidGFiXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJ2LXRhYlwiLCB7IGF0dHJzOiB7IGhyZWY6IFwiI3RhYi1pbmZvcm1hdGlvblwiIH0gfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIk15IEluZm9ybWF0aW9uXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcInYtdGFiXCIsIHsgYXR0cnM6IHsgaHJlZjogXCIjdGFiLWNvbnRhY3RzXCIgfSB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiQ29udGFjdHNcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwidi10YWJcIiwgeyBhdHRyczogeyBocmVmOiBcIiN0YWItZXhwZXJpZW5jZXNcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCJFeHBlcmllbmNlc1wiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJ2LXRhYlwiLCB7IGF0dHJzOiB7IGhyZWY6IFwiI3RhYi10cmFpbmluZ3NcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCJUcmFpbmluZ3NcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwidi10YWJcIiwgeyBhdHRyczogeyBocmVmOiBcIiN0YWItcG9ydGZvbGlvXCIgfSB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiUG9ydGZvbGlvXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcInYtdGFiXCIsIHsgYXR0cnM6IHsgaHJlZjogXCIjdGFiLWFwdGl0dWRlc1wiIH0gfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIkFwdGl0dWRlc1wiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJ2LXRhYlwiLCB7IGF0dHJzOiB7IGhyZWY6IFwiI3RhYi1za2lsbHNcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCJTa2lsbHNcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwidi10YWJcIiwgeyBhdHRyczogeyBocmVmOiBcIiN0YWItc29mdC1za2lsbHNcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCJTb2Z0IHNraWxsc1wiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJ2LXRhYlwiLCB7IGF0dHJzOiB7IGhyZWY6IFwiI3RhYi1pbnRlcmVzdHNcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCJJbnRlcmVzdHNcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwidi10YWJcIiwgeyBhdHRyczogeyBocmVmOiBcIiN0YWItbGFuZ3VhZ2VzXCIgfSB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiTGFuZ3VhZ2VzXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgMVxuICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgIHByb3h5OiB0cnVlXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFtfYyhcInYtdG9vbGJhci10aXRsZVwiLCBbX3ZtLl92KFwiQWRtaW4gQ1ZcIildKV0sXG4gICAgICAgICAgICAxXG4gICAgICAgICAgKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFxuICAgICAgICAgICAgXCJ2LXRhYnMtaXRlbXNcIixcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbW9kZWw6IHtcbiAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLnRhYixcbiAgICAgICAgICAgICAgICBjYWxsYmFjazogZnVuY3Rpb24oJCR2KSB7XG4gICAgICAgICAgICAgICAgICBfdm0udGFiID0gJCR2XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcInRhYlwiXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBbXG4gICAgICAgICAgICAgIF9jKFwiYXBwLWFkbWluLWluZm9ybWF0aW9uXCIpLFxuICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICBfYyhcImFwcC1hZG1pbi1jb250YWN0c1wiKSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgX2MoXCJhcHAtYWRtaW4tZXhwZXJpZW5jZXNcIiksXG4gICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgIF9jKFwiYXBwLWFkbWluLXRyYWluaW5nc1wiKSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgX2MoXCJhcHAtYWRtaW4tcG9ydGZvbGlvXCIpLFxuICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICBfYyhcImFwcC1hZG1pbi1hcHRpdHVkZXNcIiksXG4gICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgIF9jKFwiYXBwLWFkbWluLXNraWxsc1wiKSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgX2MoXCJhcHAtYWRtaW4tc29mdC1za2lsbHNcIiksXG4gICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgIF9jKFwiYXBwLWFkbWluLWludGVyZXN0c1wiKSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgX2MoXCJhcHAtYWRtaW4tbGFuZ3VhZ2VzXCIpXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgMVxuICAgICAgICAgIClcbiAgICAgICAgXSxcbiAgICAgICAgMVxuICAgICAgKVxuICAgIF0sXG4gICAgMVxuICApXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW11cbnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuXG5leHBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucyB9IiwidmFyIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3ZtID0gdGhpc1xuICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gIHJldHVybiBfYyhcbiAgICBcInYtYXBwXCIsXG4gICAgW1xuICAgICAgX2MoXG4gICAgICAgIFwidi1tYWluXCIsXG4gICAgICAgIFtcbiAgICAgICAgICBfYyhcbiAgICAgICAgICAgIFwidi1jb250YWluZXJcIixcbiAgICAgICAgICAgIHsgc3RhdGljQ2xhc3M6IFwiZmlsbC1oZWlnaHQgZmx1aWRcIiB9LFxuICAgICAgICAgICAgW1xuICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICBcInYtcm93XCIsXG4gICAgICAgICAgICAgICAgeyBhdHRyczogeyBhbGlnbjogXCJjZW50ZXJcIiwganVzdGlmeTogXCJjZW50ZXJcIiB9IH0sXG4gICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgX3ZtLnVzZXJjb3VudCA9PT0gbnVsbFxuICAgICAgICAgICAgICAgICAgICA/IF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJ2LWNvbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWNlbnRlclwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczogeyBjb2xzOiBcIjEyXCIsIHNtOiBcIjhcIiwgbWQ6IFwiNlwiIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwidi1wcm9ncmVzcy1jaXJjdWxhclwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHsgaW5kZXRlcm1pbmF0ZTogXCJcIiwgY29sb3I6IFwiYW1iZXJcIiB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgMVxuICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgOiBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidi1jb2xcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgYXR0cnM6IHsgY29sczogXCIxMlwiLCBzbTogXCI4XCIsIG1kOiBcIjZcIiB9IH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF92bS51c2VyY291bnQgPT0gMVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgID8gX2MoXCJhcHAtc2lnbmluXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBfYyhcImFwcC1zaWdudXBcIilcbiAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAxXG4gICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgMVxuICAgICAgICAgICAgICApXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgMVxuICAgICAgICAgIClcbiAgICAgICAgXSxcbiAgICAgICAgMVxuICAgICAgKVxuICAgIF0sXG4gICAgMVxuICApXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW11cbnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuXG5leHBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucyB9IiwidmFyIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3ZtID0gdGhpc1xuICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gIHJldHVybiBfYyhcbiAgICBcInYtcm93XCIsXG4gICAgeyBhdHRyczogeyBhbGlnbjogXCJ0b3BcIiwganVzdGlmeTogXCJjZW50ZXJcIiB9IH0sXG4gICAgW1xuICAgICAgX2MoXG4gICAgICAgIFwidi1jb2xcIixcbiAgICAgICAgeyBhdHRyczogeyBjb2xzOiBcIjlcIiB9IH0sXG4gICAgICAgIFtcbiAgICAgICAgICBfYyhcbiAgICAgICAgICAgIFwidi1yb3dcIixcbiAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgX2MoXCJ2LWNvbFwiLCB7IGF0dHJzOiB7IGNvbHM6IFwiMTJcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICBfYyhcInBcIiwgeyBzdGF0aWNDbGFzczogXCJ0ZXh0LWgzXCIgfSwgW192bS5fdihcIk5vbVwiKV0pLFxuICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgX2MoXCJwXCIsIHsgc3RhdGljQ2xhc3M6IFwidGV4dC1oNFwiIH0sIFtfdm0uX3YoXCJUaXRsZVwiKV0pXG4gICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICBfYyhcInYtY29sXCIsIHsgYXR0cnM6IHsgY29sczogXCIxMlwiIH0gfSwgW19jKFwiYXBwLWNvbnRhY3RcIildLCAxKVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIDFcbiAgICAgICAgICApLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX2MoXG4gICAgICAgICAgICBcInYtcm93XCIsXG4gICAgICAgICAgICBbXG4gICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgIFwidi1jb2xcIixcbiAgICAgICAgICAgICAgICB7IGF0dHJzOiB7IGNvbHM6IFwiMTJcIiB9IH0sXG4gICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidGV4dC1oMyBmb250LXdlaWdodC1ib2xkIHRleHQtdXBwZXJjYXNlIHNlY3Rpb24tdGl0bGUgbWItMlwiXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoXCJFeHDDqXJpZW5jZXNcIildXG4gICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgIF9jKFwiYXBwLWV4cGVyaWVuY2VcIilcbiAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgIDFcbiAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgXCJ2LWNvbFwiLFxuICAgICAgICAgICAgICAgIHsgYXR0cnM6IHsgY29sczogXCIxMlwiIH0gfSxcbiAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0ZXh0LWgzIGZvbnQtd2VpZ2h0LWJvbGQgdGV4dC11cHBlcmNhc2Ugc2VjdGlvbi10aXRsZSBtYi0yXCJcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgW192bS5fdihcIlBvcnRmb2xpbyBwcm9mZXNzaW9ubmVsXCIpXVxuICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICBfYyhcImFwcC1wb3J0Zm9saW8tcHJvXCIpXG4gICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAxXG4gICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgIFwidi1jb2xcIixcbiAgICAgICAgICAgICAgICB7IGF0dHJzOiB7IGNvbHM6IFwiMTJcIiB9IH0sXG4gICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidGV4dC1oMyBmb250LXdlaWdodC1ib2xkIHRleHQtdXBwZXJjYXNlIHNlY3Rpb24tdGl0bGUgbWItMlwiXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoXCJQb3J0Zm9saW8gcGVyc29ubmVsXCIpXVxuICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICBfYyhcImFwcC1wb3J0Zm9saW8tcGVyc29cIilcbiAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgIDFcbiAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgXCJ2LWNvbFwiLFxuICAgICAgICAgICAgICAgIHsgYXR0cnM6IHsgY29sczogXCIxMlwiIH0gfSxcbiAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0ZXh0LWgzIGZvbnQtd2VpZ2h0LWJvbGQgdGV4dC11cHBlcmNhc2Ugc2VjdGlvbi10aXRsZSBtYi0yXCJcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgW192bS5fdihcIkZvcm1hdGlvbnNcIildXG4gICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgIF9jKFwiYXBwLXRyYWluaW5nXCIpXG4gICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAxXG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAxXG4gICAgICAgICAgKVxuICAgICAgICBdLFxuICAgICAgICAxXG4gICAgICApLFxuICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgIF9jKFxuICAgICAgICBcInYtY29sXCIsXG4gICAgICAgIHsgYXR0cnM6IHsgY29sczogXCIzXCIgfSB9LFxuICAgICAgICBbXG4gICAgICAgICAgX2MoXG4gICAgICAgICAgICBcInYtY29sXCIsXG4gICAgICAgICAgICB7IHN0YXRpY0NsYXNzOiBcIm1iLTNcIiwgYXR0cnM6IHsgY29sczogXCIxMlwiIH0gfSxcbiAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgX2MoXCJ2LWltZ1wiLCB7XG4gICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgIHNyYzpcbiAgICAgICAgICAgICAgICAgICAgXCJodHRwczovL3d3dy5jaXRhdGlvbmJvbmhldXIuZnIvd3AtY29udGVudC91cGxvYWRzLzIwMTcvMDgvcGxhY2Vob2xkZXItMTAyNHgxMDI0LnBuZ1wiXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIDFcbiAgICAgICAgICApLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX2MoXCJhcHAtbGFuZ3VhZ2VcIiksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcImFwcC1za2lsbFwiKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFwiYXBwLXNvZnQtc2tpbGxcIiksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcImFwcC1pbnRlcmVzdFwiKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFwiYXBwLWV4dHJhXCIpXG4gICAgICAgIF0sXG4gICAgICAgIDFcbiAgICAgIClcbiAgICBdLFxuICAgIDFcbiAgKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtdXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcblxuZXhwb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSJdLCJzb3VyY2VSb290IjoiIn0=