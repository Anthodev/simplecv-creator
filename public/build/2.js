(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./assets/vue/components/Language.vue":
/*!********************************************!*\
  !*** ./assets/vue/components/Language.vue ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Language_vue_vue_type_template_id_590dacca___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Language.vue?vue&type=template&id=590dacca& */ "./assets/vue/components/Language.vue?vue&type=template&id=590dacca&");
/* harmony import */ var _Language_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Language.vue?vue&type=script&lang=js& */ "./assets/vue/components/Language.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vuetify-loader/lib/runtime/installComponents.js */ "./node_modules/vuetify-loader/lib/runtime/installComponents.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuetify/lib/components/VGrid */ "./node_modules/vuetify/lib/components/VGrid/index.js");
/* harmony import */ var vuetify_lib_components_VRating__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vuetify/lib/components/VRating */ "./node_modules/vuetify/lib/components/VRating/index.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Language_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Language_vue_vue_type_template_id_590dacca___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Language_vue_vue_type_template_id_590dacca___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* vuetify-loader */




_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default()(component, {VCol: vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_4__["VCol"],VRating: vuetify_lib_components_VRating__WEBPACK_IMPORTED_MODULE_5__["VRating"],VRow: vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_4__["VRow"]})


/* hot reload */
if (false) { var api; }
component.options.__file = "assets/vue/components/Language.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./assets/vue/components/Language.vue?vue&type=script&lang=js&":
/*!*********************************************************************!*\
  !*** ./assets/vue/components/Language.vue?vue&type=script&lang=js& ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Language_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--0-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Language.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/Language.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Language_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./assets/vue/components/Language.vue?vue&type=template&id=590dacca&":
/*!***************************************************************************!*\
  !*** ./assets/vue/components/Language.vue?vue&type=template&id=590dacca& ***!
  \***************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Language_vue_vue_type_template_id_590dacca___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Language.vue?vue&type=template&id=590dacca& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/Language.vue?vue&type=template&id=590dacca&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Language_vue_vue_type_template_id_590dacca___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Language_vue_vue_type_template_id_590dacca___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/Language.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/components/Language.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      rating: 3
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/Language.vue?vue&type=template&id=590dacca&":
/*!*********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/components/Language.vue?vue&type=template&id=590dacca& ***!
  \*********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-row",
    { staticClass: "mb-2" },
    [
      _c(
        "v-col",
        { attrs: { cols: "12" } },
        [
          _c(
            "div",
            {
              staticClass:
                "text-h4 font-weight-bold text-uppercase section-title mb-2"
            },
            [_vm._v("Compétences")]
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "12" } },
                [
                  _c(
                    "div",
                    {
                      staticClass:
                        "text-h6 font-weight-bold text-uppercase mb-2"
                    },
                    [_vm._v("Language")]
                  ),
                  _vm._v(" "),
                  _c("v-rating", {
                    attrs: {
                      "empty-icon": "mdi-checkbox-blank-circle-outline",
                      "full-icon": "mdi-checkbox-blank-circle",
                      color: "amber",
                      "background-color": "white",
                      readonly: ""
                    },
                    model: {
                      value: _vm.rating,
                      callback: function($$v) {
                        _vm.rating = $$v
                      },
                      expression: "rating"
                    }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vuetify/lib/components/VRating/VRating.js":
/*!****************************************************************!*\
  !*** ./node_modules/vuetify/lib/components/VRating/VRating.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _src_components_VRating_VRating_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../src/components/VRating/VRating.sass */ "./node_modules/vuetify/src/components/VRating/VRating.sass");
/* harmony import */ var _src_components_VRating_VRating_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VRating_VRating_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _VIcon__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../VIcon */ "./node_modules/vuetify/lib/components/VIcon/index.js");
/* harmony import */ var _mixins_colorable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../mixins/colorable */ "./node_modules/vuetify/lib/mixins/colorable/index.js");
/* harmony import */ var _mixins_delayable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../mixins/delayable */ "./node_modules/vuetify/lib/mixins/delayable/index.js");
/* harmony import */ var _mixins_sizeable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../mixins/sizeable */ "./node_modules/vuetify/lib/mixins/sizeable/index.js");
/* harmony import */ var _mixins_rippleable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../mixins/rippleable */ "./node_modules/vuetify/lib/mixins/rippleable/index.js");
/* harmony import */ var _mixins_themeable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../mixins/themeable */ "./node_modules/vuetify/lib/mixins/themeable/index.js");
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../util/helpers */ "./node_modules/vuetify/lib/util/helpers.js");
/* harmony import */ var _util_mixins__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../util/mixins */ "./node_modules/vuetify/lib/util/mixins.js");
// Styles
 // Components

 // Mixins





 // Utilities



/* @vue/component */

/* harmony default export */ __webpack_exports__["default"] = (Object(_util_mixins__WEBPACK_IMPORTED_MODULE_8__["default"])(_mixins_colorable__WEBPACK_IMPORTED_MODULE_2__["default"], _mixins_delayable__WEBPACK_IMPORTED_MODULE_3__["default"], _mixins_rippleable__WEBPACK_IMPORTED_MODULE_5__["default"], _mixins_sizeable__WEBPACK_IMPORTED_MODULE_4__["default"], _mixins_themeable__WEBPACK_IMPORTED_MODULE_6__["default"]).extend({
  name: 'v-rating',
  props: {
    backgroundColor: {
      type: String,
      default: 'accent'
    },
    color: {
      type: String,
      default: 'primary'
    },
    clearable: Boolean,
    dense: Boolean,
    emptyIcon: {
      type: String,
      default: '$ratingEmpty'
    },
    fullIcon: {
      type: String,
      default: '$ratingFull'
    },
    halfIcon: {
      type: String,
      default: '$ratingHalf'
    },
    halfIncrements: Boolean,
    hover: Boolean,
    length: {
      type: [Number, String],
      default: 5
    },
    readonly: Boolean,
    size: [Number, String],
    value: {
      type: Number,
      default: 0
    }
  },

  data() {
    return {
      hoverIndex: -1,
      internalValue: this.value
    };
  },

  computed: {
    directives() {
      if (this.readonly || !this.ripple) return [];
      return [{
        name: 'ripple',
        value: {
          circle: true
        }
      }];
    },

    iconProps() {
      const {
        dark,
        large,
        light,
        medium,
        small,
        size,
        xLarge,
        xSmall
      } = this.$props;
      return {
        dark,
        large,
        light,
        medium,
        size,
        small,
        xLarge,
        xSmall
      };
    },

    isHovering() {
      return this.hover && this.hoverIndex >= 0;
    }

  },
  watch: {
    internalValue(val) {
      val !== this.value && this.$emit('input', val);
    },

    value(val) {
      this.internalValue = val;
    }

  },
  methods: {
    createClickFn(i) {
      return e => {
        if (this.readonly) return;
        const newValue = this.genHoverIndex(e, i);

        if (this.clearable && this.internalValue === newValue) {
          this.internalValue = 0;
        } else {
          this.internalValue = newValue;
        }
      };
    },

    createProps(i) {
      const props = {
        index: i,
        value: this.internalValue,
        click: this.createClickFn(i),
        isFilled: Math.floor(this.internalValue) > i,
        isHovered: Math.floor(this.hoverIndex) > i
      };

      if (this.halfIncrements) {
        props.isHalfHovered = !props.isHovered && (this.hoverIndex - i) % 1 > 0;
        props.isHalfFilled = !props.isFilled && (this.internalValue - i) % 1 > 0;
      }

      return props;
    },

    genHoverIndex(e, i) {
      let isHalf = this.isHalfEvent(e);

      if (this.halfIncrements && this.$vuetify.rtl) {
        isHalf = !isHalf;
      }

      return i + (isHalf ? 0.5 : 1);
    },

    getIconName(props) {
      const isFull = this.isHovering ? props.isHovered : props.isFilled;
      const isHalf = this.isHovering ? props.isHalfHovered : props.isHalfFilled;
      return isFull ? this.fullIcon : isHalf ? this.halfIcon : this.emptyIcon;
    },

    getColor(props) {
      if (this.isHovering) {
        if (props.isHovered || props.isHalfHovered) return this.color;
      } else {
        if (props.isFilled || props.isHalfFilled) return this.color;
      }

      return this.backgroundColor;
    },

    isHalfEvent(e) {
      if (this.halfIncrements) {
        const rect = e.target && e.target.getBoundingClientRect();
        if (rect && e.pageX - rect.left < rect.width / 2) return true;
      }

      return false;
    },

    onMouseEnter(e, i) {
      this.runDelay('open', () => {
        this.hoverIndex = this.genHoverIndex(e, i);
      });
    },

    onMouseLeave() {
      this.runDelay('close', () => this.hoverIndex = -1);
    },

    genItem(i) {
      const props = this.createProps(i);
      if (this.$scopedSlots.item) return this.$scopedSlots.item(props);
      const listeners = {
        click: props.click
      };

      if (this.hover) {
        listeners.mouseenter = e => this.onMouseEnter(e, i);

        listeners.mouseleave = this.onMouseLeave;

        if (this.halfIncrements) {
          listeners.mousemove = e => this.onMouseEnter(e, i);
        }
      }

      return this.$createElement(_VIcon__WEBPACK_IMPORTED_MODULE_1__["default"], this.setTextColor(this.getColor(props), {
        attrs: {
          tabindex: -1
        },
        directives: this.directives,
        props: this.iconProps,
        on: listeners
      }), [this.getIconName(props)]);
    }

  },

  render(h) {
    const children = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_7__["createRange"])(Number(this.length)).map(i => this.genItem(i));
    return h('div', {
      staticClass: 'v-rating',
      class: {
        'v-rating--readonly': this.readonly,
        'v-rating--dense': this.dense
      }
    }, children);
  }

}));
//# sourceMappingURL=VRating.js.map

/***/ }),

/***/ "./node_modules/vuetify/lib/components/VRating/index.js":
/*!**************************************************************!*\
  !*** ./node_modules/vuetify/lib/components/VRating/index.js ***!
  \**************************************************************/
/*! exports provided: VRating, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _VRating__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./VRating */ "./node_modules/vuetify/lib/components/VRating/VRating.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "VRating", function() { return _VRating__WEBPACK_IMPORTED_MODULE_0__["default"]; });



/* harmony default export */ __webpack_exports__["default"] = (_VRating__WEBPACK_IMPORTED_MODULE_0__["default"]);
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/vuetify/lib/mixins/delayable/index.js":
/*!************************************************************!*\
  !*** ./node_modules/vuetify/lib/mixins/delayable/index.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

/**
 * Delayable
 *
 * @mixin
 *
 * Changes the open or close delay time for elements
 */

/* harmony default export */ __webpack_exports__["default"] = (vue__WEBPACK_IMPORTED_MODULE_0__["default"].extend().extend({
  name: 'delayable',
  props: {
    openDelay: {
      type: [Number, String],
      default: 0
    },
    closeDelay: {
      type: [Number, String],
      default: 0
    }
  },
  data: () => ({
    openTimeout: undefined,
    closeTimeout: undefined
  }),
  methods: {
    /**
     * Clear any pending delay timers from executing
     */
    clearDelay() {
      clearTimeout(this.openTimeout);
      clearTimeout(this.closeTimeout);
    },

    /**
     * Runs callback after a specified delay
     */
    runDelay(type, cb) {
      this.clearDelay();
      const delay = parseInt(this[`${type}Delay`], 10);
      this[`${type}Timeout`] = setTimeout(cb || (() => {
        this.isActive = {
          open: true,
          close: false
        }[type];
      }), delay);
    }

  }
}));
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/vuetify/lib/mixins/rippleable/index.js":
/*!*************************************************************!*\
  !*** ./node_modules/vuetify/lib/mixins/rippleable/index.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _directives_ripple__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../directives/ripple */ "./node_modules/vuetify/lib/directives/ripple/index.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
// Directives
 // Types


/* harmony default export */ __webpack_exports__["default"] = (vue__WEBPACK_IMPORTED_MODULE_1__["default"].extend({
  name: 'rippleable',
  directives: {
    ripple: _directives_ripple__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: {
    ripple: {
      type: [Boolean, Object],
      default: true
    }
  },
  methods: {
    genRipple(data = {}) {
      if (!this.ripple) return null;
      data.staticClass = 'v-input--selection-controls__ripple';
      data.directives = data.directives || [];
      data.directives.push({
        name: 'ripple',
        value: {
          center: true
        }
      });
      return this.$createElement('div', data);
    }

  }
}));
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/vuetify/src/components/VRating/VRating.sass":
/*!******************************************************************!*\
  !*** ./node_modules/vuetify/src/components/VRating/VRating.sass ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvdnVlL2NvbXBvbmVudHMvTGFuZ3VhZ2UudnVlIiwid2VicGFjazovLy8uL2Fzc2V0cy92dWUvY29tcG9uZW50cy9MYW5ndWFnZS52dWU/ODVhNyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvdnVlL2NvbXBvbmVudHMvTGFuZ3VhZ2UudnVlPzFjOGIiLCJ3ZWJwYWNrOi8vL2Fzc2V0cy92dWUvY29tcG9uZW50cy9MYW5ndWFnZS52dWUiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3Z1ZS9jb21wb25lbnRzL0xhbmd1YWdlLnZ1ZT9kZDcxIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy92dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZSYXRpbmcvVlJhdGluZy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvdnVldGlmeS9saWIvY29tcG9uZW50cy9WUmF0aW5nL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy92dWV0aWZ5L2xpYi9taXhpbnMvZGVsYXlhYmxlL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy92dWV0aWZ5L2xpYi9taXhpbnMvcmlwcGxlYWJsZS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvdnVldGlmeS9zcmMvY29tcG9uZW50cy9WUmF0aW5nL1ZSYXRpbmcuc2FzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF1RjtBQUMzQjtBQUNMOzs7QUFHdkQ7QUFDNkY7QUFDN0YsZ0JBQWdCLDJHQUFVO0FBQzFCLEVBQUUsOEVBQU07QUFDUixFQUFFLG1GQUFNO0FBQ1IsRUFBRSw0RkFBZTtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNzRztBQUNsRDtBQUNLO0FBQ0w7QUFDcEQsb0dBQWlCLGFBQWEsdUVBQUksQ0FBQywrRUFBTyxDQUFDLHVFQUFJLENBQUM7OztBQUdoRDtBQUNBLElBQUksS0FBVSxFQUFFLFlBaUJmO0FBQ0Q7QUFDZSxnRjs7Ozs7Ozs7Ozs7O0FDOUNmO0FBQUE7QUFBQSx3Q0FBd0wsQ0FBZ0Isb1BBQUcsRUFBQyxDOzs7Ozs7Ozs7Ozs7QUNBNU07QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN1QkE7QUFDQSxNQURBLGtCQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFMQSxHOzs7Ozs7Ozs7Ozs7QUN2QkE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSyxzQkFBc0I7QUFDM0I7QUFDQTtBQUNBO0FBQ0EsU0FBUyxTQUFTLGFBQWEsRUFBRTtBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixTQUFTLGFBQWEsRUFBRTtBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUI7QUFDdkI7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDbkVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNzRDs7QUFFekI7O0FBRWtCO0FBQ0E7QUFDRjtBQUNJO0FBQ0Y7O0FBRUU7QUFDVjtBQUN2Qzs7QUFFZSwySEFBTSxDQUFDLHlEQUFTLEVBQUUseURBQVMsRUFBRSwwREFBVSxFQUFFLHdEQUFRLEVBQUUseURBQVM7QUFDM0U7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxLQUFLOztBQUVMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLOztBQUVMO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGlDQUFpQyw4Q0FBSztBQUN0QztBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQSxHQUFHOztBQUVIO0FBQ0EscUJBQXFCLGlFQUFXO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQSxDQUFDLENBQUMsRUFBQztBQUNILG1DOzs7Ozs7Ozs7Ozs7QUNuT0E7QUFBQTtBQUFBO0FBQUE7QUFBZ0M7QUFDYjtBQUNKLCtHQUFPLEVBQUM7QUFDdkIsaUM7Ozs7Ozs7Ozs7OztBQ0hBO0FBQUE7QUFBc0I7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRWUsMEdBQUc7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQ0FBcUMsS0FBSztBQUMxQyxjQUFjLEtBQUs7QUFDbkI7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULE9BQU87QUFDUDs7QUFFQTtBQUNBLENBQUMsQ0FBQyxFQUFDO0FBQ0gsaUM7Ozs7Ozs7Ozs7OztBQ2xEQTtBQUFBO0FBQUE7QUFBQTtBQUM2Qzs7QUFFdkI7QUFDUCwwR0FBRztBQUNsQjtBQUNBO0FBQ0EsSUFBSSxrRUFBTTtBQUNWLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBLENBQUMsQ0FBQyxFQUFDO0FBQ0gsaUM7Ozs7Ozs7Ozs7O0FDL0JBLHVDIiwiZmlsZSI6IjIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucyB9IGZyb20gXCIuL0xhbmd1YWdlLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD01OTBkYWNjYSZcIlxuaW1wb3J0IHNjcmlwdCBmcm9tIFwiLi9MYW5ndWFnZS52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcbmV4cG9ydCAqIGZyb20gXCIuL0xhbmd1YWdlLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIlxuXG5cbi8qIG5vcm1hbGl6ZSBjb21wb25lbnQgKi9cbmltcG9ydCBub3JtYWxpemVyIGZyb20gXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3J1bnRpbWUvY29tcG9uZW50Tm9ybWFsaXplci5qc1wiXG52YXIgY29tcG9uZW50ID0gbm9ybWFsaXplcihcbiAgc2NyaXB0LFxuICByZW5kZXIsXG4gIHN0YXRpY1JlbmRlckZucyxcbiAgZmFsc2UsXG4gIG51bGwsXG4gIG51bGwsXG4gIG51bGxcbiAgXG4pXG5cbi8qIHZ1ZXRpZnktbG9hZGVyICovXG5pbXBvcnQgaW5zdGFsbENvbXBvbmVudHMgZnJvbSBcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVldGlmeS1sb2FkZXIvbGliL3J1bnRpbWUvaW5zdGFsbENvbXBvbmVudHMuanNcIlxuaW1wb3J0IHsgVkNvbCB9IGZyb20gJ3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVkdyaWQnO1xuaW1wb3J0IHsgVlJhdGluZyB9IGZyb20gJ3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVlJhdGluZyc7XG5pbXBvcnQgeyBWUm93IH0gZnJvbSAndnVldGlmeS9saWIvY29tcG9uZW50cy9WR3JpZCc7XG5pbnN0YWxsQ29tcG9uZW50cyhjb21wb25lbnQsIHtWQ29sLFZSYXRpbmcsVlJvd30pXG5cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHtcbiAgdmFyIGFwaSA9IHJlcXVpcmUoXCIvdmFyL3d3dy9odG1sL25vZGVfbW9kdWxlcy92dWUtaG90LXJlbG9hZC1hcGkvZGlzdC9pbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghYXBpLmlzUmVjb3JkZWQoJzU5MGRhY2NhJykpIHtcbiAgICAgIGFwaS5jcmVhdGVSZWNvcmQoJzU5MGRhY2NhJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfSBlbHNlIHtcbiAgICAgIGFwaS5yZWxvYWQoJzU5MGRhY2NhJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfVxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiLi9MYW5ndWFnZS52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9NTkwZGFjY2EmXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGFwaS5yZXJlbmRlcignNTkwZGFjY2EnLCB7XG4gICAgICAgIHJlbmRlcjogcmVuZGVyLFxuICAgICAgICBzdGF0aWNSZW5kZXJGbnM6IHN0YXRpY1JlbmRlckZuc1xuICAgICAgfSlcbiAgICB9KVxuICB9XG59XG5jb21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcImFzc2V0cy92dWUvY29tcG9uZW50cy9MYW5ndWFnZS52dWVcIlxuZXhwb3J0IGRlZmF1bHQgY29tcG9uZW50LmV4cG9ydHMiLCJpbXBvcnQgbW9kIGZyb20gXCItIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTAtMCEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL0xhbmd1YWdlLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIjsgZXhwb3J0IGRlZmF1bHQgbW9kOyBleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0wLTAhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9MYW5ndWFnZS52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCIiLCJleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvbG9hZGVycy90ZW1wbGF0ZUxvYWRlci5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vTGFuZ3VhZ2UudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTU5MGRhY2NhJlwiIiwiPHRlbXBsYXRlPlxuICA8di1yb3cgY2xhc3M9XCJtYi0yXCI+XG4gICAgPHYtY29sIGNvbHM9XCIxMlwiPlxuICAgICAgPGRpdiBjbGFzcz1cInRleHQtaDQgZm9udC13ZWlnaHQtYm9sZCB0ZXh0LXVwcGVyY2FzZSBzZWN0aW9uLXRpdGxlIG1iLTJcIj5Db21ww6l0ZW5jZXM8L2Rpdj5cblxuICAgICAgPHYtcm93PlxuICAgICAgICA8di1jb2wgY29scz1cIjEyXCI+XG4gICAgICAgICAgPGRpdiBjbGFzcz1cInRleHQtaDYgZm9udC13ZWlnaHQtYm9sZCB0ZXh0LXVwcGVyY2FzZSBtYi0yXCI+TGFuZ3VhZ2U8L2Rpdj5cbiAgICAgICAgICA8di1yYXRpbmdcbiAgICAgICAgICAgIHYtbW9kZWw9XCJyYXRpbmdcIlxuICAgICAgICAgICAgZW1wdHktaWNvbj1cIm1kaS1jaGVja2JveC1ibGFuay1jaXJjbGUtb3V0bGluZVwiXG4gICAgICAgICAgICBmdWxsLWljb249XCJtZGktY2hlY2tib3gtYmxhbmstY2lyY2xlXCJcbiAgICAgICAgICAgIGNvbG9yPVwiYW1iZXJcIlxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcj1cIndoaXRlXCJcbiAgICAgICAgICAgIHJlYWRvbmx5XG4gICAgICAgICAgLz5cbiAgICAgICAgPC92LWNvbD5cbiAgICAgIDwvdi1yb3c+XG4gICAgPC92LWNvbD5cbiAgPC92LXJvdz5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG4gIGRhdGEoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHJhdGluZzogMyxcbiAgICB9XG4gIH1cbn1cbjwvc2NyaXB0PlxuIiwidmFyIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3ZtID0gdGhpc1xuICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gIHJldHVybiBfYyhcbiAgICBcInYtcm93XCIsXG4gICAgeyBzdGF0aWNDbGFzczogXCJtYi0yXCIgfSxcbiAgICBbXG4gICAgICBfYyhcbiAgICAgICAgXCJ2LWNvbFwiLFxuICAgICAgICB7IGF0dHJzOiB7IGNvbHM6IFwiMTJcIiB9IH0sXG4gICAgICAgIFtcbiAgICAgICAgICBfYyhcbiAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgIFwidGV4dC1oNCBmb250LXdlaWdodC1ib2xkIHRleHQtdXBwZXJjYXNlIHNlY3Rpb24tdGl0bGUgbWItMlwiXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgW192bS5fdihcIkNvbXDDqXRlbmNlc1wiKV1cbiAgICAgICAgICApLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX2MoXG4gICAgICAgICAgICBcInYtcm93XCIsXG4gICAgICAgICAgICBbXG4gICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgIFwidi1jb2xcIixcbiAgICAgICAgICAgICAgICB7IGF0dHJzOiB7IGNvbHM6IFwiMTJcIiB9IH0sXG4gICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidGV4dC1oNiBmb250LXdlaWdodC1ib2xkIHRleHQtdXBwZXJjYXNlIG1iLTJcIlxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBbX3ZtLl92KFwiTGFuZ3VhZ2VcIildXG4gICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgIF9jKFwidi1yYXRpbmdcIiwge1xuICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgIFwiZW1wdHktaWNvblwiOiBcIm1kaS1jaGVja2JveC1ibGFuay1jaXJjbGUtb3V0bGluZVwiLFxuICAgICAgICAgICAgICAgICAgICAgIFwiZnVsbC1pY29uXCI6IFwibWRpLWNoZWNrYm94LWJsYW5rLWNpcmNsZVwiLFxuICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiBcImFtYmVyXCIsXG4gICAgICAgICAgICAgICAgICAgICAgXCJiYWNrZ3JvdW5kLWNvbG9yXCI6IFwid2hpdGVcIixcbiAgICAgICAgICAgICAgICAgICAgICByZWFkb25seTogXCJcIlxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBtb2RlbDoge1xuICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBfdm0ucmF0aW5nLFxuICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrOiBmdW5jdGlvbigkJHYpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIF92bS5yYXRpbmcgPSAkJHZcbiAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246IFwicmF0aW5nXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgIDFcbiAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIDFcbiAgICAgICAgICApXG4gICAgICAgIF0sXG4gICAgICAgIDFcbiAgICAgIClcbiAgICBdLFxuICAgIDFcbiAgKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtdXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcblxuZXhwb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSIsIi8vIFN0eWxlc1xuaW1wb3J0IFwiLi4vLi4vLi4vc3JjL2NvbXBvbmVudHMvVlJhdGluZy9WUmF0aW5nLnNhc3NcIjsgLy8gQ29tcG9uZW50c1xuXG5pbXBvcnQgVkljb24gZnJvbSAnLi4vVkljb24nOyAvLyBNaXhpbnNcblxuaW1wb3J0IENvbG9yYWJsZSBmcm9tICcuLi8uLi9taXhpbnMvY29sb3JhYmxlJztcbmltcG9ydCBEZWxheWFibGUgZnJvbSAnLi4vLi4vbWl4aW5zL2RlbGF5YWJsZSc7XG5pbXBvcnQgU2l6ZWFibGUgZnJvbSAnLi4vLi4vbWl4aW5zL3NpemVhYmxlJztcbmltcG9ydCBSaXBwbGVhYmxlIGZyb20gJy4uLy4uL21peGlucy9yaXBwbGVhYmxlJztcbmltcG9ydCBUaGVtZWFibGUgZnJvbSAnLi4vLi4vbWl4aW5zL3RoZW1lYWJsZSc7IC8vIFV0aWxpdGllc1xuXG5pbXBvcnQgeyBjcmVhdGVSYW5nZSB9IGZyb20gJy4uLy4uL3V0aWwvaGVscGVycyc7XG5pbXBvcnQgbWl4aW5zIGZyb20gJy4uLy4uL3V0aWwvbWl4aW5zJztcbi8qIEB2dWUvY29tcG9uZW50ICovXG5cbmV4cG9ydCBkZWZhdWx0IG1peGlucyhDb2xvcmFibGUsIERlbGF5YWJsZSwgUmlwcGxlYWJsZSwgU2l6ZWFibGUsIFRoZW1lYWJsZSkuZXh0ZW5kKHtcbiAgbmFtZTogJ3YtcmF0aW5nJyxcbiAgcHJvcHM6IHtcbiAgICBiYWNrZ3JvdW5kQ29sb3I6IHtcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6ICdhY2NlbnQnXG4gICAgfSxcbiAgICBjb2xvcjoge1xuICAgICAgdHlwZTogU3RyaW5nLFxuICAgICAgZGVmYXVsdDogJ3ByaW1hcnknXG4gICAgfSxcbiAgICBjbGVhcmFibGU6IEJvb2xlYW4sXG4gICAgZGVuc2U6IEJvb2xlYW4sXG4gICAgZW1wdHlJY29uOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiAnJHJhdGluZ0VtcHR5J1xuICAgIH0sXG4gICAgZnVsbEljb246IHtcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6ICckcmF0aW5nRnVsbCdcbiAgICB9LFxuICAgIGhhbGZJY29uOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiAnJHJhdGluZ0hhbGYnXG4gICAgfSxcbiAgICBoYWxmSW5jcmVtZW50czogQm9vbGVhbixcbiAgICBob3ZlcjogQm9vbGVhbixcbiAgICBsZW5ndGg6IHtcbiAgICAgIHR5cGU6IFtOdW1iZXIsIFN0cmluZ10sXG4gICAgICBkZWZhdWx0OiA1XG4gICAgfSxcbiAgICByZWFkb25seTogQm9vbGVhbixcbiAgICBzaXplOiBbTnVtYmVyLCBTdHJpbmddLFxuICAgIHZhbHVlOiB7XG4gICAgICB0eXBlOiBOdW1iZXIsXG4gICAgICBkZWZhdWx0OiAwXG4gICAgfVxuICB9LFxuXG4gIGRhdGEoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGhvdmVySW5kZXg6IC0xLFxuICAgICAgaW50ZXJuYWxWYWx1ZTogdGhpcy52YWx1ZVxuICAgIH07XG4gIH0sXG5cbiAgY29tcHV0ZWQ6IHtcbiAgICBkaXJlY3RpdmVzKCkge1xuICAgICAgaWYgKHRoaXMucmVhZG9ubHkgfHwgIXRoaXMucmlwcGxlKSByZXR1cm4gW107XG4gICAgICByZXR1cm4gW3tcbiAgICAgICAgbmFtZTogJ3JpcHBsZScsXG4gICAgICAgIHZhbHVlOiB7XG4gICAgICAgICAgY2lyY2xlOiB0cnVlXG4gICAgICAgIH1cbiAgICAgIH1dO1xuICAgIH0sXG5cbiAgICBpY29uUHJvcHMoKSB7XG4gICAgICBjb25zdCB7XG4gICAgICAgIGRhcmssXG4gICAgICAgIGxhcmdlLFxuICAgICAgICBsaWdodCxcbiAgICAgICAgbWVkaXVtLFxuICAgICAgICBzbWFsbCxcbiAgICAgICAgc2l6ZSxcbiAgICAgICAgeExhcmdlLFxuICAgICAgICB4U21hbGxcbiAgICAgIH0gPSB0aGlzLiRwcm9wcztcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGRhcmssXG4gICAgICAgIGxhcmdlLFxuICAgICAgICBsaWdodCxcbiAgICAgICAgbWVkaXVtLFxuICAgICAgICBzaXplLFxuICAgICAgICBzbWFsbCxcbiAgICAgICAgeExhcmdlLFxuICAgICAgICB4U21hbGxcbiAgICAgIH07XG4gICAgfSxcblxuICAgIGlzSG92ZXJpbmcoKSB7XG4gICAgICByZXR1cm4gdGhpcy5ob3ZlciAmJiB0aGlzLmhvdmVySW5kZXggPj0gMDtcbiAgICB9XG5cbiAgfSxcbiAgd2F0Y2g6IHtcbiAgICBpbnRlcm5hbFZhbHVlKHZhbCkge1xuICAgICAgdmFsICE9PSB0aGlzLnZhbHVlICYmIHRoaXMuJGVtaXQoJ2lucHV0JywgdmFsKTtcbiAgICB9LFxuXG4gICAgdmFsdWUodmFsKSB7XG4gICAgICB0aGlzLmludGVybmFsVmFsdWUgPSB2YWw7XG4gICAgfVxuXG4gIH0sXG4gIG1ldGhvZHM6IHtcbiAgICBjcmVhdGVDbGlja0ZuKGkpIHtcbiAgICAgIHJldHVybiBlID0+IHtcbiAgICAgICAgaWYgKHRoaXMucmVhZG9ubHkpIHJldHVybjtcbiAgICAgICAgY29uc3QgbmV3VmFsdWUgPSB0aGlzLmdlbkhvdmVySW5kZXgoZSwgaSk7XG5cbiAgICAgICAgaWYgKHRoaXMuY2xlYXJhYmxlICYmIHRoaXMuaW50ZXJuYWxWYWx1ZSA9PT0gbmV3VmFsdWUpIHtcbiAgICAgICAgICB0aGlzLmludGVybmFsVmFsdWUgPSAwO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMuaW50ZXJuYWxWYWx1ZSA9IG5ld1ZhbHVlO1xuICAgICAgICB9XG4gICAgICB9O1xuICAgIH0sXG5cbiAgICBjcmVhdGVQcm9wcyhpKSB7XG4gICAgICBjb25zdCBwcm9wcyA9IHtcbiAgICAgICAgaW5kZXg6IGksXG4gICAgICAgIHZhbHVlOiB0aGlzLmludGVybmFsVmFsdWUsXG4gICAgICAgIGNsaWNrOiB0aGlzLmNyZWF0ZUNsaWNrRm4oaSksXG4gICAgICAgIGlzRmlsbGVkOiBNYXRoLmZsb29yKHRoaXMuaW50ZXJuYWxWYWx1ZSkgPiBpLFxuICAgICAgICBpc0hvdmVyZWQ6IE1hdGguZmxvb3IodGhpcy5ob3ZlckluZGV4KSA+IGlcbiAgICAgIH07XG5cbiAgICAgIGlmICh0aGlzLmhhbGZJbmNyZW1lbnRzKSB7XG4gICAgICAgIHByb3BzLmlzSGFsZkhvdmVyZWQgPSAhcHJvcHMuaXNIb3ZlcmVkICYmICh0aGlzLmhvdmVySW5kZXggLSBpKSAlIDEgPiAwO1xuICAgICAgICBwcm9wcy5pc0hhbGZGaWxsZWQgPSAhcHJvcHMuaXNGaWxsZWQgJiYgKHRoaXMuaW50ZXJuYWxWYWx1ZSAtIGkpICUgMSA+IDA7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBwcm9wcztcbiAgICB9LFxuXG4gICAgZ2VuSG92ZXJJbmRleChlLCBpKSB7XG4gICAgICBsZXQgaXNIYWxmID0gdGhpcy5pc0hhbGZFdmVudChlKTtcblxuICAgICAgaWYgKHRoaXMuaGFsZkluY3JlbWVudHMgJiYgdGhpcy4kdnVldGlmeS5ydGwpIHtcbiAgICAgICAgaXNIYWxmID0gIWlzSGFsZjtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGkgKyAoaXNIYWxmID8gMC41IDogMSk7XG4gICAgfSxcblxuICAgIGdldEljb25OYW1lKHByb3BzKSB7XG4gICAgICBjb25zdCBpc0Z1bGwgPSB0aGlzLmlzSG92ZXJpbmcgPyBwcm9wcy5pc0hvdmVyZWQgOiBwcm9wcy5pc0ZpbGxlZDtcbiAgICAgIGNvbnN0IGlzSGFsZiA9IHRoaXMuaXNIb3ZlcmluZyA/IHByb3BzLmlzSGFsZkhvdmVyZWQgOiBwcm9wcy5pc0hhbGZGaWxsZWQ7XG4gICAgICByZXR1cm4gaXNGdWxsID8gdGhpcy5mdWxsSWNvbiA6IGlzSGFsZiA/IHRoaXMuaGFsZkljb24gOiB0aGlzLmVtcHR5SWNvbjtcbiAgICB9LFxuXG4gICAgZ2V0Q29sb3IocHJvcHMpIHtcbiAgICAgIGlmICh0aGlzLmlzSG92ZXJpbmcpIHtcbiAgICAgICAgaWYgKHByb3BzLmlzSG92ZXJlZCB8fCBwcm9wcy5pc0hhbGZIb3ZlcmVkKSByZXR1cm4gdGhpcy5jb2xvcjtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmIChwcm9wcy5pc0ZpbGxlZCB8fCBwcm9wcy5pc0hhbGZGaWxsZWQpIHJldHVybiB0aGlzLmNvbG9yO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy5iYWNrZ3JvdW5kQ29sb3I7XG4gICAgfSxcblxuICAgIGlzSGFsZkV2ZW50KGUpIHtcbiAgICAgIGlmICh0aGlzLmhhbGZJbmNyZW1lbnRzKSB7XG4gICAgICAgIGNvbnN0IHJlY3QgPSBlLnRhcmdldCAmJiBlLnRhcmdldC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcbiAgICAgICAgaWYgKHJlY3QgJiYgZS5wYWdlWCAtIHJlY3QubGVmdCA8IHJlY3Qud2lkdGggLyAyKSByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH0sXG5cbiAgICBvbk1vdXNlRW50ZXIoZSwgaSkge1xuICAgICAgdGhpcy5ydW5EZWxheSgnb3BlbicsICgpID0+IHtcbiAgICAgICAgdGhpcy5ob3ZlckluZGV4ID0gdGhpcy5nZW5Ib3ZlckluZGV4KGUsIGkpO1xuICAgICAgfSk7XG4gICAgfSxcblxuICAgIG9uTW91c2VMZWF2ZSgpIHtcbiAgICAgIHRoaXMucnVuRGVsYXkoJ2Nsb3NlJywgKCkgPT4gdGhpcy5ob3ZlckluZGV4ID0gLTEpO1xuICAgIH0sXG5cbiAgICBnZW5JdGVtKGkpIHtcbiAgICAgIGNvbnN0IHByb3BzID0gdGhpcy5jcmVhdGVQcm9wcyhpKTtcbiAgICAgIGlmICh0aGlzLiRzY29wZWRTbG90cy5pdGVtKSByZXR1cm4gdGhpcy4kc2NvcGVkU2xvdHMuaXRlbShwcm9wcyk7XG4gICAgICBjb25zdCBsaXN0ZW5lcnMgPSB7XG4gICAgICAgIGNsaWNrOiBwcm9wcy5jbGlja1xuICAgICAgfTtcblxuICAgICAgaWYgKHRoaXMuaG92ZXIpIHtcbiAgICAgICAgbGlzdGVuZXJzLm1vdXNlZW50ZXIgPSBlID0+IHRoaXMub25Nb3VzZUVudGVyKGUsIGkpO1xuXG4gICAgICAgIGxpc3RlbmVycy5tb3VzZWxlYXZlID0gdGhpcy5vbk1vdXNlTGVhdmU7XG5cbiAgICAgICAgaWYgKHRoaXMuaGFsZkluY3JlbWVudHMpIHtcbiAgICAgICAgICBsaXN0ZW5lcnMubW91c2Vtb3ZlID0gZSA9PiB0aGlzLm9uTW91c2VFbnRlcihlLCBpKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy4kY3JlYXRlRWxlbWVudChWSWNvbiwgdGhpcy5zZXRUZXh0Q29sb3IodGhpcy5nZXRDb2xvcihwcm9wcyksIHtcbiAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICB0YWJpbmRleDogLTFcbiAgICAgICAgfSxcbiAgICAgICAgZGlyZWN0aXZlczogdGhpcy5kaXJlY3RpdmVzLFxuICAgICAgICBwcm9wczogdGhpcy5pY29uUHJvcHMsXG4gICAgICAgIG9uOiBsaXN0ZW5lcnNcbiAgICAgIH0pLCBbdGhpcy5nZXRJY29uTmFtZShwcm9wcyldKTtcbiAgICB9XG5cbiAgfSxcblxuICByZW5kZXIoaCkge1xuICAgIGNvbnN0IGNoaWxkcmVuID0gY3JlYXRlUmFuZ2UoTnVtYmVyKHRoaXMubGVuZ3RoKSkubWFwKGkgPT4gdGhpcy5nZW5JdGVtKGkpKTtcbiAgICByZXR1cm4gaCgnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6ICd2LXJhdGluZycsXG4gICAgICBjbGFzczoge1xuICAgICAgICAndi1yYXRpbmctLXJlYWRvbmx5JzogdGhpcy5yZWFkb25seSxcbiAgICAgICAgJ3YtcmF0aW5nLS1kZW5zZSc6IHRoaXMuZGVuc2VcbiAgICAgIH1cbiAgICB9LCBjaGlsZHJlbik7XG4gIH1cblxufSk7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1WUmF0aW5nLmpzLm1hcCIsImltcG9ydCBWUmF0aW5nIGZyb20gJy4vVlJhdGluZyc7XG5leHBvcnQgeyBWUmF0aW5nIH07XG5leHBvcnQgZGVmYXVsdCBWUmF0aW5nO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aW5kZXguanMubWFwIiwiaW1wb3J0IFZ1ZSBmcm9tICd2dWUnO1xuLyoqXG4gKiBEZWxheWFibGVcbiAqXG4gKiBAbWl4aW5cbiAqXG4gKiBDaGFuZ2VzIHRoZSBvcGVuIG9yIGNsb3NlIGRlbGF5IHRpbWUgZm9yIGVsZW1lbnRzXG4gKi9cblxuZXhwb3J0IGRlZmF1bHQgVnVlLmV4dGVuZCgpLmV4dGVuZCh7XG4gIG5hbWU6ICdkZWxheWFibGUnLFxuICBwcm9wczoge1xuICAgIG9wZW5EZWxheToge1xuICAgICAgdHlwZTogW051bWJlciwgU3RyaW5nXSxcbiAgICAgIGRlZmF1bHQ6IDBcbiAgICB9LFxuICAgIGNsb3NlRGVsYXk6IHtcbiAgICAgIHR5cGU6IFtOdW1iZXIsIFN0cmluZ10sXG4gICAgICBkZWZhdWx0OiAwXG4gICAgfVxuICB9LFxuICBkYXRhOiAoKSA9PiAoe1xuICAgIG9wZW5UaW1lb3V0OiB1bmRlZmluZWQsXG4gICAgY2xvc2VUaW1lb3V0OiB1bmRlZmluZWRcbiAgfSksXG4gIG1ldGhvZHM6IHtcbiAgICAvKipcbiAgICAgKiBDbGVhciBhbnkgcGVuZGluZyBkZWxheSB0aW1lcnMgZnJvbSBleGVjdXRpbmdcbiAgICAgKi9cbiAgICBjbGVhckRlbGF5KCkge1xuICAgICAgY2xlYXJUaW1lb3V0KHRoaXMub3BlblRpbWVvdXQpO1xuICAgICAgY2xlYXJUaW1lb3V0KHRoaXMuY2xvc2VUaW1lb3V0KTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogUnVucyBjYWxsYmFjayBhZnRlciBhIHNwZWNpZmllZCBkZWxheVxuICAgICAqL1xuICAgIHJ1bkRlbGF5KHR5cGUsIGNiKSB7XG4gICAgICB0aGlzLmNsZWFyRGVsYXkoKTtcbiAgICAgIGNvbnN0IGRlbGF5ID0gcGFyc2VJbnQodGhpc1tgJHt0eXBlfURlbGF5YF0sIDEwKTtcbiAgICAgIHRoaXNbYCR7dHlwZX1UaW1lb3V0YF0gPSBzZXRUaW1lb3V0KGNiIHx8ICgoKSA9PiB7XG4gICAgICAgIHRoaXMuaXNBY3RpdmUgPSB7XG4gICAgICAgICAgb3BlbjogdHJ1ZSxcbiAgICAgICAgICBjbG9zZTogZmFsc2VcbiAgICAgICAgfVt0eXBlXTtcbiAgICAgIH0pLCBkZWxheSk7XG4gICAgfVxuXG4gIH1cbn0pO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aW5kZXguanMubWFwIiwiLy8gRGlyZWN0aXZlc1xuaW1wb3J0IHJpcHBsZSBmcm9tICcuLi8uLi9kaXJlY3RpdmVzL3JpcHBsZSc7IC8vIFR5cGVzXG5cbmltcG9ydCBWdWUgZnJvbSAndnVlJztcbmV4cG9ydCBkZWZhdWx0IFZ1ZS5leHRlbmQoe1xuICBuYW1lOiAncmlwcGxlYWJsZScsXG4gIGRpcmVjdGl2ZXM6IHtcbiAgICByaXBwbGVcbiAgfSxcbiAgcHJvcHM6IHtcbiAgICByaXBwbGU6IHtcbiAgICAgIHR5cGU6IFtCb29sZWFuLCBPYmplY3RdLFxuICAgICAgZGVmYXVsdDogdHJ1ZVxuICAgIH1cbiAgfSxcbiAgbWV0aG9kczoge1xuICAgIGdlblJpcHBsZShkYXRhID0ge30pIHtcbiAgICAgIGlmICghdGhpcy5yaXBwbGUpIHJldHVybiBudWxsO1xuICAgICAgZGF0YS5zdGF0aWNDbGFzcyA9ICd2LWlucHV0LS1zZWxlY3Rpb24tY29udHJvbHNfX3JpcHBsZSc7XG4gICAgICBkYXRhLmRpcmVjdGl2ZXMgPSBkYXRhLmRpcmVjdGl2ZXMgfHwgW107XG4gICAgICBkYXRhLmRpcmVjdGl2ZXMucHVzaCh7XG4gICAgICAgIG5hbWU6ICdyaXBwbGUnLFxuICAgICAgICB2YWx1ZToge1xuICAgICAgICAgIGNlbnRlcjogdHJ1ZVxuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIHJldHVybiB0aGlzLiRjcmVhdGVFbGVtZW50KCdkaXYnLCBkYXRhKTtcbiAgICB9XG5cbiAgfVxufSk7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1pbmRleC5qcy5tYXAiLCIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iXSwic291cmNlUm9vdCI6IiJ9