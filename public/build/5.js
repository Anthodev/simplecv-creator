(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./assets/vue/components/admin/Skill.vue":
/*!***********************************************!*\
  !*** ./assets/vue/components/admin/Skill.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Skill_vue_vue_type_template_id_d6d2e6e2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Skill.vue?vue&type=template&id=d6d2e6e2& */ "./assets/vue/components/admin/Skill.vue?vue&type=template&id=d6d2e6e2&");
/* harmony import */ var _Skill_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Skill.vue?vue&type=script&lang=js& */ "./assets/vue/components/admin/Skill.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vuetify-loader/lib/runtime/installComponents.js */ "./node_modules/vuetify-loader/lib/runtime/installComponents.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vuetify_lib_components_VBtn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuetify/lib/components/VBtn */ "./node_modules/vuetify/lib/components/VBtn/index.js");
/* harmony import */ var vuetify_lib_components_VCard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vuetify/lib/components/VCard */ "./node_modules/vuetify/lib/components/VCard/index.js");
/* harmony import */ var vuetify_lib_components_VRating__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vuetify/lib/components/VRating */ "./node_modules/vuetify/lib/components/VRating/index.js");
/* harmony import */ var vuetify_lib_components_VTabs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! vuetify/lib/components/VTabs */ "./node_modules/vuetify/lib/components/VTabs/index.js");
/* harmony import */ var vuetify_lib_components_VTextField__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! vuetify/lib/components/VTextField */ "./node_modules/vuetify/lib/components/VTextField/index.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Skill_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Skill_vue_vue_type_template_id_d6d2e6e2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Skill_vue_vue_type_template_id_d6d2e6e2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* vuetify-loader */







_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default()(component, {VBtn: vuetify_lib_components_VBtn__WEBPACK_IMPORTED_MODULE_4__["VBtn"],VCard: vuetify_lib_components_VCard__WEBPACK_IMPORTED_MODULE_5__["VCard"],VCardText: vuetify_lib_components_VCard__WEBPACK_IMPORTED_MODULE_5__["VCardText"],VRating: vuetify_lib_components_VRating__WEBPACK_IMPORTED_MODULE_6__["VRating"],VTabItem: vuetify_lib_components_VTabs__WEBPACK_IMPORTED_MODULE_7__["VTabItem"],VTextField: vuetify_lib_components_VTextField__WEBPACK_IMPORTED_MODULE_8__["VTextField"]})


/* hot reload */
if (false) { var api; }
component.options.__file = "assets/vue/components/admin/Skill.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./assets/vue/components/admin/Skill.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./assets/vue/components/admin/Skill.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Skill_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Skill.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/admin/Skill.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Skill_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./assets/vue/components/admin/Skill.vue?vue&type=template&id=d6d2e6e2&":
/*!******************************************************************************!*\
  !*** ./assets/vue/components/admin/Skill.vue?vue&type=template&id=d6d2e6e2& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Skill_vue_vue_type_template_id_d6d2e6e2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Skill.vue?vue&type=template&id=d6d2e6e2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/admin/Skill.vue?vue&type=template&id=d6d2e6e2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Skill_vue_vue_type_template_id_d6d2e6e2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Skill_vue_vue_type_template_id_d6d2e6e2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/admin/Skill.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/components/admin/Skill.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      rating: 0
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/admin/Skill.vue?vue&type=template&id=d6d2e6e2&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/components/admin/Skill.vue?vue&type=template&id=d6d2e6e2& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-tab-item",
    { attrs: { value: "tab-skills" } },
    [
      _c(
        "v-card",
        { attrs: { flat: "" } },
        [
          _c(
            "v-card-text",
            [
              _c("v-text-field", {
                attrs: { placeholder: "Name", filled: "" }
              }),
              _vm._v("\n      Level: "),
              _c("v-rating", {
                attrs: {
                  "empty-icon": "mdi-checkbox-blank-circle-outline",
                  "full-icon": "mdi-checkbox-blank-circle",
                  color: "amber",
                  "background-color": "white"
                },
                model: {
                  value: _vm.rating,
                  callback: function($$v) {
                    _vm.rating = $$v
                  },
                  expression: "rating"
                }
              }),
              _vm._v(" "),
              _c("v-btn", { attrs: { color: "primary" } }, [_vm._v("Add")])
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vuetify/lib/components/VRating/VRating.js":
/*!****************************************************************!*\
  !*** ./node_modules/vuetify/lib/components/VRating/VRating.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _src_components_VRating_VRating_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../src/components/VRating/VRating.sass */ "./node_modules/vuetify/src/components/VRating/VRating.sass");
/* harmony import */ var _src_components_VRating_VRating_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VRating_VRating_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _VIcon__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../VIcon */ "./node_modules/vuetify/lib/components/VIcon/index.js");
/* harmony import */ var _mixins_colorable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../mixins/colorable */ "./node_modules/vuetify/lib/mixins/colorable/index.js");
/* harmony import */ var _mixins_delayable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../mixins/delayable */ "./node_modules/vuetify/lib/mixins/delayable/index.js");
/* harmony import */ var _mixins_sizeable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../mixins/sizeable */ "./node_modules/vuetify/lib/mixins/sizeable/index.js");
/* harmony import */ var _mixins_rippleable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../mixins/rippleable */ "./node_modules/vuetify/lib/mixins/rippleable/index.js");
/* harmony import */ var _mixins_themeable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../mixins/themeable */ "./node_modules/vuetify/lib/mixins/themeable/index.js");
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../util/helpers */ "./node_modules/vuetify/lib/util/helpers.js");
/* harmony import */ var _util_mixins__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../util/mixins */ "./node_modules/vuetify/lib/util/mixins.js");
// Styles
 // Components

 // Mixins





 // Utilities



/* @vue/component */

/* harmony default export */ __webpack_exports__["default"] = (Object(_util_mixins__WEBPACK_IMPORTED_MODULE_8__["default"])(_mixins_colorable__WEBPACK_IMPORTED_MODULE_2__["default"], _mixins_delayable__WEBPACK_IMPORTED_MODULE_3__["default"], _mixins_rippleable__WEBPACK_IMPORTED_MODULE_5__["default"], _mixins_sizeable__WEBPACK_IMPORTED_MODULE_4__["default"], _mixins_themeable__WEBPACK_IMPORTED_MODULE_6__["default"]).extend({
  name: 'v-rating',
  props: {
    backgroundColor: {
      type: String,
      default: 'accent'
    },
    color: {
      type: String,
      default: 'primary'
    },
    clearable: Boolean,
    dense: Boolean,
    emptyIcon: {
      type: String,
      default: '$ratingEmpty'
    },
    fullIcon: {
      type: String,
      default: '$ratingFull'
    },
    halfIcon: {
      type: String,
      default: '$ratingHalf'
    },
    halfIncrements: Boolean,
    hover: Boolean,
    length: {
      type: [Number, String],
      default: 5
    },
    readonly: Boolean,
    size: [Number, String],
    value: {
      type: Number,
      default: 0
    }
  },

  data() {
    return {
      hoverIndex: -1,
      internalValue: this.value
    };
  },

  computed: {
    directives() {
      if (this.readonly || !this.ripple) return [];
      return [{
        name: 'ripple',
        value: {
          circle: true
        }
      }];
    },

    iconProps() {
      const {
        dark,
        large,
        light,
        medium,
        small,
        size,
        xLarge,
        xSmall
      } = this.$props;
      return {
        dark,
        large,
        light,
        medium,
        size,
        small,
        xLarge,
        xSmall
      };
    },

    isHovering() {
      return this.hover && this.hoverIndex >= 0;
    }

  },
  watch: {
    internalValue(val) {
      val !== this.value && this.$emit('input', val);
    },

    value(val) {
      this.internalValue = val;
    }

  },
  methods: {
    createClickFn(i) {
      return e => {
        if (this.readonly) return;
        const newValue = this.genHoverIndex(e, i);

        if (this.clearable && this.internalValue === newValue) {
          this.internalValue = 0;
        } else {
          this.internalValue = newValue;
        }
      };
    },

    createProps(i) {
      const props = {
        index: i,
        value: this.internalValue,
        click: this.createClickFn(i),
        isFilled: Math.floor(this.internalValue) > i,
        isHovered: Math.floor(this.hoverIndex) > i
      };

      if (this.halfIncrements) {
        props.isHalfHovered = !props.isHovered && (this.hoverIndex - i) % 1 > 0;
        props.isHalfFilled = !props.isFilled && (this.internalValue - i) % 1 > 0;
      }

      return props;
    },

    genHoverIndex(e, i) {
      let isHalf = this.isHalfEvent(e);

      if (this.halfIncrements && this.$vuetify.rtl) {
        isHalf = !isHalf;
      }

      return i + (isHalf ? 0.5 : 1);
    },

    getIconName(props) {
      const isFull = this.isHovering ? props.isHovered : props.isFilled;
      const isHalf = this.isHovering ? props.isHalfHovered : props.isHalfFilled;
      return isFull ? this.fullIcon : isHalf ? this.halfIcon : this.emptyIcon;
    },

    getColor(props) {
      if (this.isHovering) {
        if (props.isHovered || props.isHalfHovered) return this.color;
      } else {
        if (props.isFilled || props.isHalfFilled) return this.color;
      }

      return this.backgroundColor;
    },

    isHalfEvent(e) {
      if (this.halfIncrements) {
        const rect = e.target && e.target.getBoundingClientRect();
        if (rect && e.pageX - rect.left < rect.width / 2) return true;
      }

      return false;
    },

    onMouseEnter(e, i) {
      this.runDelay('open', () => {
        this.hoverIndex = this.genHoverIndex(e, i);
      });
    },

    onMouseLeave() {
      this.runDelay('close', () => this.hoverIndex = -1);
    },

    genItem(i) {
      const props = this.createProps(i);
      if (this.$scopedSlots.item) return this.$scopedSlots.item(props);
      const listeners = {
        click: props.click
      };

      if (this.hover) {
        listeners.mouseenter = e => this.onMouseEnter(e, i);

        listeners.mouseleave = this.onMouseLeave;

        if (this.halfIncrements) {
          listeners.mousemove = e => this.onMouseEnter(e, i);
        }
      }

      return this.$createElement(_VIcon__WEBPACK_IMPORTED_MODULE_1__["default"], this.setTextColor(this.getColor(props), {
        attrs: {
          tabindex: -1
        },
        directives: this.directives,
        props: this.iconProps,
        on: listeners
      }), [this.getIconName(props)]);
    }

  },

  render(h) {
    const children = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_7__["createRange"])(Number(this.length)).map(i => this.genItem(i));
    return h('div', {
      staticClass: 'v-rating',
      class: {
        'v-rating--readonly': this.readonly,
        'v-rating--dense': this.dense
      }
    }, children);
  }

}));
//# sourceMappingURL=VRating.js.map

/***/ }),

/***/ "./node_modules/vuetify/lib/components/VRating/index.js":
/*!**************************************************************!*\
  !*** ./node_modules/vuetify/lib/components/VRating/index.js ***!
  \**************************************************************/
/*! exports provided: VRating, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _VRating__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./VRating */ "./node_modules/vuetify/lib/components/VRating/VRating.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "VRating", function() { return _VRating__WEBPACK_IMPORTED_MODULE_0__["default"]; });



/* harmony default export */ __webpack_exports__["default"] = (_VRating__WEBPACK_IMPORTED_MODULE_0__["default"]);
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/vuetify/lib/mixins/delayable/index.js":
/*!************************************************************!*\
  !*** ./node_modules/vuetify/lib/mixins/delayable/index.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

/**
 * Delayable
 *
 * @mixin
 *
 * Changes the open or close delay time for elements
 */

/* harmony default export */ __webpack_exports__["default"] = (vue__WEBPACK_IMPORTED_MODULE_0__["default"].extend().extend({
  name: 'delayable',
  props: {
    openDelay: {
      type: [Number, String],
      default: 0
    },
    closeDelay: {
      type: [Number, String],
      default: 0
    }
  },
  data: () => ({
    openTimeout: undefined,
    closeTimeout: undefined
  }),
  methods: {
    /**
     * Clear any pending delay timers from executing
     */
    clearDelay() {
      clearTimeout(this.openTimeout);
      clearTimeout(this.closeTimeout);
    },

    /**
     * Runs callback after a specified delay
     */
    runDelay(type, cb) {
      this.clearDelay();
      const delay = parseInt(this[`${type}Delay`], 10);
      this[`${type}Timeout`] = setTimeout(cb || (() => {
        this.isActive = {
          open: true,
          close: false
        }[type];
      }), delay);
    }

  }
}));
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/vuetify/lib/mixins/rippleable/index.js":
/*!*************************************************************!*\
  !*** ./node_modules/vuetify/lib/mixins/rippleable/index.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _directives_ripple__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../directives/ripple */ "./node_modules/vuetify/lib/directives/ripple/index.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
// Directives
 // Types


/* harmony default export */ __webpack_exports__["default"] = (vue__WEBPACK_IMPORTED_MODULE_1__["default"].extend({
  name: 'rippleable',
  directives: {
    ripple: _directives_ripple__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: {
    ripple: {
      type: [Boolean, Object],
      default: true
    }
  },
  methods: {
    genRipple(data = {}) {
      if (!this.ripple) return null;
      data.staticClass = 'v-input--selection-controls__ripple';
      data.directives = data.directives || [];
      data.directives.push({
        name: 'ripple',
        value: {
          center: true
        }
      });
      return this.$createElement('div', data);
    }

  }
}));
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/vuetify/src/components/VRating/VRating.sass":
/*!******************************************************************!*\
  !*** ./node_modules/vuetify/src/components/VRating/VRating.sass ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvdnVlL2NvbXBvbmVudHMvYWRtaW4vU2tpbGwudnVlIiwid2VicGFjazovLy8uL2Fzc2V0cy92dWUvY29tcG9uZW50cy9hZG1pbi9Ta2lsbC52dWU/YjA3NSIsIndlYnBhY2s6Ly8vLi9hc3NldHMvdnVlL2NvbXBvbmVudHMvYWRtaW4vU2tpbGwudnVlP2RjOGMiLCJ3ZWJwYWNrOi8vL2Fzc2V0cy92dWUvY29tcG9uZW50cy9hZG1pbi9Ta2lsbC52dWUiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3Z1ZS9jb21wb25lbnRzL2FkbWluL1NraWxsLnZ1ZT9mOWVhIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy92dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZSYXRpbmcvVlJhdGluZy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvdnVldGlmeS9saWIvY29tcG9uZW50cy9WUmF0aW5nL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy92dWV0aWZ5L2xpYi9taXhpbnMvZGVsYXlhYmxlL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy92dWV0aWZ5L2xpYi9taXhpbnMvcmlwcGxlYWJsZS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvdnVldGlmeS9zcmMvY29tcG9uZW50cy9WUmF0aW5nL1ZSYXRpbmcuc2FzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFvRjtBQUMzQjtBQUNMOzs7QUFHcEQ7QUFDZ0c7QUFDaEcsZ0JBQWdCLDJHQUFVO0FBQzFCLEVBQUUsMkVBQU07QUFDUixFQUFFLGdGQUFNO0FBQ1IsRUFBRSx5RkFBZTtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUN5RztBQUN0RDtBQUNFO0FBQ0k7QUFDQTtBQUNEO0FBQ087QUFDL0Qsb0dBQWlCLGFBQWEsc0VBQUksQ0FBQyx5RUFBSyxDQUFDLGlGQUFTLENBQUMsK0VBQU8sQ0FBQywrRUFBUSxDQUFDLHdGQUFVLENBQUM7OztBQUcvRTtBQUNBLElBQUksS0FBVSxFQUFFLFlBaUJmO0FBQ0Q7QUFDZSxnRjs7Ozs7Ozs7Ozs7O0FDakRmO0FBQUE7QUFBQSx3Q0FBMkwsQ0FBZ0IsaVBBQUcsRUFBQyxDOzs7Ozs7Ozs7Ozs7QUNBL007QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ21CQTtBQUNBLE1BREEsa0JBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUxBLEc7Ozs7Ozs7Ozs7OztBQ25CQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLLFNBQVMsc0JBQXNCLEVBQUU7QUFDdEM7QUFDQTtBQUNBO0FBQ0EsU0FBUyxTQUFTLFdBQVcsRUFBRTtBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0JBQXdCO0FBQ3hCLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0EsMkJBQTJCLFNBQVMsbUJBQW1CLEVBQUU7QUFDekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQy9DQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDc0Q7O0FBRXpCOztBQUVrQjtBQUNBO0FBQ0Y7QUFDSTtBQUNGOztBQUVFO0FBQ1Y7QUFDdkM7O0FBRWUsMkhBQU0sQ0FBQyx5REFBUyxFQUFFLHlEQUFTLEVBQUUsMERBQVUsRUFBRSx3REFBUSxFQUFFLHlEQUFTO0FBQzNFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSzs7QUFFTDtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxpQ0FBaUMsOENBQUs7QUFDdEM7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7O0FBRUEsR0FBRzs7QUFFSDtBQUNBLHFCQUFxQixpRUFBVztBQUNoQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUEsQ0FBQyxDQUFDLEVBQUM7QUFDSCxtQzs7Ozs7Ozs7Ozs7O0FDbk9BO0FBQUE7QUFBQTtBQUFBO0FBQWdDO0FBQ2I7QUFDSiwrR0FBTyxFQUFDO0FBQ3ZCLGlDOzs7Ozs7Ozs7Ozs7QUNIQTtBQUFBO0FBQXNCO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVlLDBHQUFHO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUNBQXFDLEtBQUs7QUFDMUMsY0FBYyxLQUFLO0FBQ25CO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCxPQUFPO0FBQ1A7O0FBRUE7QUFDQSxDQUFDLENBQUMsRUFBQztBQUNILGlDOzs7Ozs7Ozs7Ozs7QUNsREE7QUFBQTtBQUFBO0FBQUE7QUFDNkM7O0FBRXZCO0FBQ1AsMEdBQUc7QUFDbEI7QUFDQTtBQUNBLElBQUksa0VBQU07QUFDVixHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQSxDQUFDLENBQUMsRUFBQztBQUNILGlDOzs7Ozs7Ozs7OztBQy9CQSx1QyIsImZpbGUiOiI1LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSBmcm9tIFwiLi9Ta2lsbC52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9ZDZkMmU2ZTImXCJcbmltcG9ydCBzY3JpcHQgZnJvbSBcIi4vU2tpbGwudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9Ta2lsbC52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcblxuXG4vKiBub3JtYWxpemUgY29tcG9uZW50ICovXG5pbXBvcnQgbm9ybWFsaXplciBmcm9tIFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanNcIlxudmFyIGNvbXBvbmVudCA9IG5vcm1hbGl6ZXIoXG4gIHNjcmlwdCxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZhbHNlLFxuICBudWxsLFxuICBudWxsLFxuICBudWxsXG4gIFxuKVxuXG4vKiB2dWV0aWZ5LWxvYWRlciAqL1xuaW1wb3J0IGluc3RhbGxDb21wb25lbnRzIGZyb20gXCIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZXRpZnktbG9hZGVyL2xpYi9ydW50aW1lL2luc3RhbGxDb21wb25lbnRzLmpzXCJcbmltcG9ydCB7IFZCdG4gfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZCdG4nO1xuaW1wb3J0IHsgVkNhcmQgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZDYXJkJztcbmltcG9ydCB7IFZDYXJkVGV4dCB9IGZyb20gJ3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVkNhcmQnO1xuaW1wb3J0IHsgVlJhdGluZyB9IGZyb20gJ3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVlJhdGluZyc7XG5pbXBvcnQgeyBWVGFiSXRlbSB9IGZyb20gJ3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVlRhYnMnO1xuaW1wb3J0IHsgVlRleHRGaWVsZCB9IGZyb20gJ3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVlRleHRGaWVsZCc7XG5pbnN0YWxsQ29tcG9uZW50cyhjb21wb25lbnQsIHtWQnRuLFZDYXJkLFZDYXJkVGV4dCxWUmF0aW5nLFZUYWJJdGVtLFZUZXh0RmllbGR9KVxuXG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7XG4gIHZhciBhcGkgPSByZXF1aXJlKFwiL3Zhci93d3cvaHRtbC9ub2RlX21vZHVsZXMvdnVlLWhvdC1yZWxvYWQtYXBpL2Rpc3QvaW5kZXguanNcIilcbiAgYXBpLmluc3RhbGwocmVxdWlyZSgndnVlJykpXG4gIGlmIChhcGkuY29tcGF0aWJsZSkge1xuICAgIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgICBpZiAoIWFwaS5pc1JlY29yZGVkKCdkNmQyZTZlMicpKSB7XG4gICAgICBhcGkuY3JlYXRlUmVjb3JkKCdkNmQyZTZlMicsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVsb2FkKCdkNmQyZTZlMicsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH1cbiAgICBtb2R1bGUuaG90LmFjY2VwdChcIi4vU2tpbGwudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPWQ2ZDJlNmUyJlwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBhcGkucmVyZW5kZXIoJ2Q2ZDJlNmUyJywge1xuICAgICAgICByZW5kZXI6IHJlbmRlcixcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnNcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxufVxuY29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJhc3NldHMvdnVlL2NvbXBvbmVudHMvYWRtaW4vU2tpbGwudnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0wLTAhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Ta2lsbC52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCI7IGV4cG9ydCBkZWZhdWx0IG1vZDsgZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMC0wIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vU2tpbGwudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiIiwiZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2xvYWRlcnMvdGVtcGxhdGVMb2FkZXIuanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1NraWxsLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD1kNmQyZTZlMiZcIiIsIjx0ZW1wbGF0ZT5cbiAgPHYtdGFiLWl0ZW0gdmFsdWU9XCJ0YWItc2tpbGxzXCI+XG4gICAgPHYtY2FyZCBmbGF0PlxuICAgICAgPHYtY2FyZC10ZXh0PlxuICAgICAgICA8di10ZXh0LWZpZWxkIHBsYWNlaG9sZGVyPVwiTmFtZVwiIGZpbGxlZD48L3YtdGV4dC1maWVsZD5cbiAgICAgICAgTGV2ZWw6IDx2LXJhdGluZ1xuICAgICAgICAgIHYtbW9kZWw9XCJyYXRpbmdcIlxuICAgICAgICAgIGVtcHR5LWljb249XCJtZGktY2hlY2tib3gtYmxhbmstY2lyY2xlLW91dGxpbmVcIlxuICAgICAgICAgIGZ1bGwtaWNvbj1cIm1kaS1jaGVja2JveC1ibGFuay1jaXJjbGVcIlxuICAgICAgICAgIGNvbG9yPVwiYW1iZXJcIlxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I9XCJ3aGl0ZVwiXG4gICAgICAgIC8+XG4gICAgICAgIDx2LWJ0biBjb2xvcj1cInByaW1hcnlcIj5BZGQ8L3YtYnRuPlxuICAgICAgPC92LWNhcmQtdGV4dD5cbiAgICA8L3YtY2FyZD5cbiAgPC92LXRhYi1pdGVtPlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgZGF0YSgpIHtcbiAgICByZXR1cm4ge1xuICAgICAgcmF0aW5nOiAwLFxuICAgIH1cbiAgfVxufVxuPC9zY3JpcHQ+XG4iLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFxuICAgIFwidi10YWItaXRlbVwiLFxuICAgIHsgYXR0cnM6IHsgdmFsdWU6IFwidGFiLXNraWxsc1wiIH0gfSxcbiAgICBbXG4gICAgICBfYyhcbiAgICAgICAgXCJ2LWNhcmRcIixcbiAgICAgICAgeyBhdHRyczogeyBmbGF0OiBcIlwiIH0gfSxcbiAgICAgICAgW1xuICAgICAgICAgIF9jKFxuICAgICAgICAgICAgXCJ2LWNhcmQtdGV4dFwiLFxuICAgICAgICAgICAgW1xuICAgICAgICAgICAgICBfYyhcInYtdGV4dC1maWVsZFwiLCB7XG4gICAgICAgICAgICAgICAgYXR0cnM6IHsgcGxhY2Vob2xkZXI6IFwiTmFtZVwiLCBmaWxsZWQ6IFwiXCIgfVxuICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiXFxuICAgICAgTGV2ZWw6IFwiKSxcbiAgICAgICAgICAgICAgX2MoXCJ2LXJhdGluZ1wiLCB7XG4gICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgIFwiZW1wdHktaWNvblwiOiBcIm1kaS1jaGVja2JveC1ibGFuay1jaXJjbGUtb3V0bGluZVwiLFxuICAgICAgICAgICAgICAgICAgXCJmdWxsLWljb25cIjogXCJtZGktY2hlY2tib3gtYmxhbmstY2lyY2xlXCIsXG4gICAgICAgICAgICAgICAgICBjb2xvcjogXCJhbWJlclwiLFxuICAgICAgICAgICAgICAgICAgXCJiYWNrZ3JvdW5kLWNvbG9yXCI6IFwid2hpdGVcIlxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgbW9kZWw6IHtcbiAgICAgICAgICAgICAgICAgIHZhbHVlOiBfdm0ucmF0aW5nLFxuICAgICAgICAgICAgICAgICAgY2FsbGJhY2s6IGZ1bmN0aW9uKCQkdikge1xuICAgICAgICAgICAgICAgICAgICBfdm0ucmF0aW5nID0gJCR2XG4gICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgZXhwcmVzc2lvbjogXCJyYXRpbmdcIlxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgIF9jKFwidi1idG5cIiwgeyBhdHRyczogeyBjb2xvcjogXCJwcmltYXJ5XCIgfSB9LCBbX3ZtLl92KFwiQWRkXCIpXSlcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAxXG4gICAgICAgICAgKVxuICAgICAgICBdLFxuICAgICAgICAxXG4gICAgICApXG4gICAgXSxcbiAgICAxXG4gIClcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5cbmV4cG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0iLCIvLyBTdHlsZXNcbmltcG9ydCBcIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL1ZSYXRpbmcvVlJhdGluZy5zYXNzXCI7IC8vIENvbXBvbmVudHNcblxuaW1wb3J0IFZJY29uIGZyb20gJy4uL1ZJY29uJzsgLy8gTWl4aW5zXG5cbmltcG9ydCBDb2xvcmFibGUgZnJvbSAnLi4vLi4vbWl4aW5zL2NvbG9yYWJsZSc7XG5pbXBvcnQgRGVsYXlhYmxlIGZyb20gJy4uLy4uL21peGlucy9kZWxheWFibGUnO1xuaW1wb3J0IFNpemVhYmxlIGZyb20gJy4uLy4uL21peGlucy9zaXplYWJsZSc7XG5pbXBvcnQgUmlwcGxlYWJsZSBmcm9tICcuLi8uLi9taXhpbnMvcmlwcGxlYWJsZSc7XG5pbXBvcnQgVGhlbWVhYmxlIGZyb20gJy4uLy4uL21peGlucy90aGVtZWFibGUnOyAvLyBVdGlsaXRpZXNcblxuaW1wb3J0IHsgY3JlYXRlUmFuZ2UgfSBmcm9tICcuLi8uLi91dGlsL2hlbHBlcnMnO1xuaW1wb3J0IG1peGlucyBmcm9tICcuLi8uLi91dGlsL21peGlucyc7XG4vKiBAdnVlL2NvbXBvbmVudCAqL1xuXG5leHBvcnQgZGVmYXVsdCBtaXhpbnMoQ29sb3JhYmxlLCBEZWxheWFibGUsIFJpcHBsZWFibGUsIFNpemVhYmxlLCBUaGVtZWFibGUpLmV4dGVuZCh7XG4gIG5hbWU6ICd2LXJhdGluZycsXG4gIHByb3BzOiB7XG4gICAgYmFja2dyb3VuZENvbG9yOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiAnYWNjZW50J1xuICAgIH0sXG4gICAgY29sb3I6IHtcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6ICdwcmltYXJ5J1xuICAgIH0sXG4gICAgY2xlYXJhYmxlOiBCb29sZWFuLFxuICAgIGRlbnNlOiBCb29sZWFuLFxuICAgIGVtcHR5SWNvbjoge1xuICAgICAgdHlwZTogU3RyaW5nLFxuICAgICAgZGVmYXVsdDogJyRyYXRpbmdFbXB0eSdcbiAgICB9LFxuICAgIGZ1bGxJY29uOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiAnJHJhdGluZ0Z1bGwnXG4gICAgfSxcbiAgICBoYWxmSWNvbjoge1xuICAgICAgdHlwZTogU3RyaW5nLFxuICAgICAgZGVmYXVsdDogJyRyYXRpbmdIYWxmJ1xuICAgIH0sXG4gICAgaGFsZkluY3JlbWVudHM6IEJvb2xlYW4sXG4gICAgaG92ZXI6IEJvb2xlYW4sXG4gICAgbGVuZ3RoOiB7XG4gICAgICB0eXBlOiBbTnVtYmVyLCBTdHJpbmddLFxuICAgICAgZGVmYXVsdDogNVxuICAgIH0sXG4gICAgcmVhZG9ubHk6IEJvb2xlYW4sXG4gICAgc2l6ZTogW051bWJlciwgU3RyaW5nXSxcbiAgICB2YWx1ZToge1xuICAgICAgdHlwZTogTnVtYmVyLFxuICAgICAgZGVmYXVsdDogMFxuICAgIH1cbiAgfSxcblxuICBkYXRhKCkge1xuICAgIHJldHVybiB7XG4gICAgICBob3ZlckluZGV4OiAtMSxcbiAgICAgIGludGVybmFsVmFsdWU6IHRoaXMudmFsdWVcbiAgICB9O1xuICB9LFxuXG4gIGNvbXB1dGVkOiB7XG4gICAgZGlyZWN0aXZlcygpIHtcbiAgICAgIGlmICh0aGlzLnJlYWRvbmx5IHx8ICF0aGlzLnJpcHBsZSkgcmV0dXJuIFtdO1xuICAgICAgcmV0dXJuIFt7XG4gICAgICAgIG5hbWU6ICdyaXBwbGUnLFxuICAgICAgICB2YWx1ZToge1xuICAgICAgICAgIGNpcmNsZTogdHJ1ZVxuICAgICAgICB9XG4gICAgICB9XTtcbiAgICB9LFxuXG4gICAgaWNvblByb3BzKCkge1xuICAgICAgY29uc3Qge1xuICAgICAgICBkYXJrLFxuICAgICAgICBsYXJnZSxcbiAgICAgICAgbGlnaHQsXG4gICAgICAgIG1lZGl1bSxcbiAgICAgICAgc21hbGwsXG4gICAgICAgIHNpemUsXG4gICAgICAgIHhMYXJnZSxcbiAgICAgICAgeFNtYWxsXG4gICAgICB9ID0gdGhpcy4kcHJvcHM7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBkYXJrLFxuICAgICAgICBsYXJnZSxcbiAgICAgICAgbGlnaHQsXG4gICAgICAgIG1lZGl1bSxcbiAgICAgICAgc2l6ZSxcbiAgICAgICAgc21hbGwsXG4gICAgICAgIHhMYXJnZSxcbiAgICAgICAgeFNtYWxsXG4gICAgICB9O1xuICAgIH0sXG5cbiAgICBpc0hvdmVyaW5nKCkge1xuICAgICAgcmV0dXJuIHRoaXMuaG92ZXIgJiYgdGhpcy5ob3ZlckluZGV4ID49IDA7XG4gICAgfVxuXG4gIH0sXG4gIHdhdGNoOiB7XG4gICAgaW50ZXJuYWxWYWx1ZSh2YWwpIHtcbiAgICAgIHZhbCAhPT0gdGhpcy52YWx1ZSAmJiB0aGlzLiRlbWl0KCdpbnB1dCcsIHZhbCk7XG4gICAgfSxcblxuICAgIHZhbHVlKHZhbCkge1xuICAgICAgdGhpcy5pbnRlcm5hbFZhbHVlID0gdmFsO1xuICAgIH1cblxuICB9LFxuICBtZXRob2RzOiB7XG4gICAgY3JlYXRlQ2xpY2tGbihpKSB7XG4gICAgICByZXR1cm4gZSA9PiB7XG4gICAgICAgIGlmICh0aGlzLnJlYWRvbmx5KSByZXR1cm47XG4gICAgICAgIGNvbnN0IG5ld1ZhbHVlID0gdGhpcy5nZW5Ib3ZlckluZGV4KGUsIGkpO1xuXG4gICAgICAgIGlmICh0aGlzLmNsZWFyYWJsZSAmJiB0aGlzLmludGVybmFsVmFsdWUgPT09IG5ld1ZhbHVlKSB7XG4gICAgICAgICAgdGhpcy5pbnRlcm5hbFZhbHVlID0gMDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLmludGVybmFsVmFsdWUgPSBuZXdWYWx1ZTtcbiAgICAgICAgfVxuICAgICAgfTtcbiAgICB9LFxuXG4gICAgY3JlYXRlUHJvcHMoaSkge1xuICAgICAgY29uc3QgcHJvcHMgPSB7XG4gICAgICAgIGluZGV4OiBpLFxuICAgICAgICB2YWx1ZTogdGhpcy5pbnRlcm5hbFZhbHVlLFxuICAgICAgICBjbGljazogdGhpcy5jcmVhdGVDbGlja0ZuKGkpLFxuICAgICAgICBpc0ZpbGxlZDogTWF0aC5mbG9vcih0aGlzLmludGVybmFsVmFsdWUpID4gaSxcbiAgICAgICAgaXNIb3ZlcmVkOiBNYXRoLmZsb29yKHRoaXMuaG92ZXJJbmRleCkgPiBpXG4gICAgICB9O1xuXG4gICAgICBpZiAodGhpcy5oYWxmSW5jcmVtZW50cykge1xuICAgICAgICBwcm9wcy5pc0hhbGZIb3ZlcmVkID0gIXByb3BzLmlzSG92ZXJlZCAmJiAodGhpcy5ob3ZlckluZGV4IC0gaSkgJSAxID4gMDtcbiAgICAgICAgcHJvcHMuaXNIYWxmRmlsbGVkID0gIXByb3BzLmlzRmlsbGVkICYmICh0aGlzLmludGVybmFsVmFsdWUgLSBpKSAlIDEgPiAwO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gcHJvcHM7XG4gICAgfSxcblxuICAgIGdlbkhvdmVySW5kZXgoZSwgaSkge1xuICAgICAgbGV0IGlzSGFsZiA9IHRoaXMuaXNIYWxmRXZlbnQoZSk7XG5cbiAgICAgIGlmICh0aGlzLmhhbGZJbmNyZW1lbnRzICYmIHRoaXMuJHZ1ZXRpZnkucnRsKSB7XG4gICAgICAgIGlzSGFsZiA9ICFpc0hhbGY7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBpICsgKGlzSGFsZiA/IDAuNSA6IDEpO1xuICAgIH0sXG5cbiAgICBnZXRJY29uTmFtZShwcm9wcykge1xuICAgICAgY29uc3QgaXNGdWxsID0gdGhpcy5pc0hvdmVyaW5nID8gcHJvcHMuaXNIb3ZlcmVkIDogcHJvcHMuaXNGaWxsZWQ7XG4gICAgICBjb25zdCBpc0hhbGYgPSB0aGlzLmlzSG92ZXJpbmcgPyBwcm9wcy5pc0hhbGZIb3ZlcmVkIDogcHJvcHMuaXNIYWxmRmlsbGVkO1xuICAgICAgcmV0dXJuIGlzRnVsbCA/IHRoaXMuZnVsbEljb24gOiBpc0hhbGYgPyB0aGlzLmhhbGZJY29uIDogdGhpcy5lbXB0eUljb247XG4gICAgfSxcblxuICAgIGdldENvbG9yKHByb3BzKSB7XG4gICAgICBpZiAodGhpcy5pc0hvdmVyaW5nKSB7XG4gICAgICAgIGlmIChwcm9wcy5pc0hvdmVyZWQgfHwgcHJvcHMuaXNIYWxmSG92ZXJlZCkgcmV0dXJuIHRoaXMuY29sb3I7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpZiAocHJvcHMuaXNGaWxsZWQgfHwgcHJvcHMuaXNIYWxmRmlsbGVkKSByZXR1cm4gdGhpcy5jb2xvcjtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXMuYmFja2dyb3VuZENvbG9yO1xuICAgIH0sXG5cbiAgICBpc0hhbGZFdmVudChlKSB7XG4gICAgICBpZiAodGhpcy5oYWxmSW5jcmVtZW50cykge1xuICAgICAgICBjb25zdCByZWN0ID0gZS50YXJnZXQgJiYgZS50YXJnZXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgICAgIGlmIChyZWN0ICYmIGUucGFnZVggLSByZWN0LmxlZnQgPCByZWN0LndpZHRoIC8gMikgcmV0dXJuIHRydWU7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9LFxuXG4gICAgb25Nb3VzZUVudGVyKGUsIGkpIHtcbiAgICAgIHRoaXMucnVuRGVsYXkoJ29wZW4nLCAoKSA9PiB7XG4gICAgICAgIHRoaXMuaG92ZXJJbmRleCA9IHRoaXMuZ2VuSG92ZXJJbmRleChlLCBpKTtcbiAgICAgIH0pO1xuICAgIH0sXG5cbiAgICBvbk1vdXNlTGVhdmUoKSB7XG4gICAgICB0aGlzLnJ1bkRlbGF5KCdjbG9zZScsICgpID0+IHRoaXMuaG92ZXJJbmRleCA9IC0xKTtcbiAgICB9LFxuXG4gICAgZ2VuSXRlbShpKSB7XG4gICAgICBjb25zdCBwcm9wcyA9IHRoaXMuY3JlYXRlUHJvcHMoaSk7XG4gICAgICBpZiAodGhpcy4kc2NvcGVkU2xvdHMuaXRlbSkgcmV0dXJuIHRoaXMuJHNjb3BlZFNsb3RzLml0ZW0ocHJvcHMpO1xuICAgICAgY29uc3QgbGlzdGVuZXJzID0ge1xuICAgICAgICBjbGljazogcHJvcHMuY2xpY2tcbiAgICAgIH07XG5cbiAgICAgIGlmICh0aGlzLmhvdmVyKSB7XG4gICAgICAgIGxpc3RlbmVycy5tb3VzZWVudGVyID0gZSA9PiB0aGlzLm9uTW91c2VFbnRlcihlLCBpKTtcblxuICAgICAgICBsaXN0ZW5lcnMubW91c2VsZWF2ZSA9IHRoaXMub25Nb3VzZUxlYXZlO1xuXG4gICAgICAgIGlmICh0aGlzLmhhbGZJbmNyZW1lbnRzKSB7XG4gICAgICAgICAgbGlzdGVuZXJzLm1vdXNlbW92ZSA9IGUgPT4gdGhpcy5vbk1vdXNlRW50ZXIoZSwgaSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXMuJGNyZWF0ZUVsZW1lbnQoVkljb24sIHRoaXMuc2V0VGV4dENvbG9yKHRoaXMuZ2V0Q29sb3IocHJvcHMpLCB7XG4gICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgdGFiaW5kZXg6IC0xXG4gICAgICAgIH0sXG4gICAgICAgIGRpcmVjdGl2ZXM6IHRoaXMuZGlyZWN0aXZlcyxcbiAgICAgICAgcHJvcHM6IHRoaXMuaWNvblByb3BzLFxuICAgICAgICBvbjogbGlzdGVuZXJzXG4gICAgICB9KSwgW3RoaXMuZ2V0SWNvbk5hbWUocHJvcHMpXSk7XG4gICAgfVxuXG4gIH0sXG5cbiAgcmVuZGVyKGgpIHtcbiAgICBjb25zdCBjaGlsZHJlbiA9IGNyZWF0ZVJhbmdlKE51bWJlcih0aGlzLmxlbmd0aCkpLm1hcChpID0+IHRoaXMuZ2VuSXRlbShpKSk7XG4gICAgcmV0dXJuIGgoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiAndi1yYXRpbmcnLFxuICAgICAgY2xhc3M6IHtcbiAgICAgICAgJ3YtcmF0aW5nLS1yZWFkb25seSc6IHRoaXMucmVhZG9ubHksXG4gICAgICAgICd2LXJhdGluZy0tZGVuc2UnOiB0aGlzLmRlbnNlXG4gICAgICB9XG4gICAgfSwgY2hpbGRyZW4pO1xuICB9XG5cbn0pO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9VlJhdGluZy5qcy5tYXAiLCJpbXBvcnQgVlJhdGluZyBmcm9tICcuL1ZSYXRpbmcnO1xuZXhwb3J0IHsgVlJhdGluZyB9O1xuZXhwb3J0IGRlZmF1bHQgVlJhdGluZztcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWluZGV4LmpzLm1hcCIsImltcG9ydCBWdWUgZnJvbSAndnVlJztcbi8qKlxuICogRGVsYXlhYmxlXG4gKlxuICogQG1peGluXG4gKlxuICogQ2hhbmdlcyB0aGUgb3BlbiBvciBjbG9zZSBkZWxheSB0aW1lIGZvciBlbGVtZW50c1xuICovXG5cbmV4cG9ydCBkZWZhdWx0IFZ1ZS5leHRlbmQoKS5leHRlbmQoe1xuICBuYW1lOiAnZGVsYXlhYmxlJyxcbiAgcHJvcHM6IHtcbiAgICBvcGVuRGVsYXk6IHtcbiAgICAgIHR5cGU6IFtOdW1iZXIsIFN0cmluZ10sXG4gICAgICBkZWZhdWx0OiAwXG4gICAgfSxcbiAgICBjbG9zZURlbGF5OiB7XG4gICAgICB0eXBlOiBbTnVtYmVyLCBTdHJpbmddLFxuICAgICAgZGVmYXVsdDogMFxuICAgIH1cbiAgfSxcbiAgZGF0YTogKCkgPT4gKHtcbiAgICBvcGVuVGltZW91dDogdW5kZWZpbmVkLFxuICAgIGNsb3NlVGltZW91dDogdW5kZWZpbmVkXG4gIH0pLFxuICBtZXRob2RzOiB7XG4gICAgLyoqXG4gICAgICogQ2xlYXIgYW55IHBlbmRpbmcgZGVsYXkgdGltZXJzIGZyb20gZXhlY3V0aW5nXG4gICAgICovXG4gICAgY2xlYXJEZWxheSgpIHtcbiAgICAgIGNsZWFyVGltZW91dCh0aGlzLm9wZW5UaW1lb3V0KTtcbiAgICAgIGNsZWFyVGltZW91dCh0aGlzLmNsb3NlVGltZW91dCk7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIFJ1bnMgY2FsbGJhY2sgYWZ0ZXIgYSBzcGVjaWZpZWQgZGVsYXlcbiAgICAgKi9cbiAgICBydW5EZWxheSh0eXBlLCBjYikge1xuICAgICAgdGhpcy5jbGVhckRlbGF5KCk7XG4gICAgICBjb25zdCBkZWxheSA9IHBhcnNlSW50KHRoaXNbYCR7dHlwZX1EZWxheWBdLCAxMCk7XG4gICAgICB0aGlzW2Ake3R5cGV9VGltZW91dGBdID0gc2V0VGltZW91dChjYiB8fCAoKCkgPT4ge1xuICAgICAgICB0aGlzLmlzQWN0aXZlID0ge1xuICAgICAgICAgIG9wZW46IHRydWUsXG4gICAgICAgICAgY2xvc2U6IGZhbHNlXG4gICAgICAgIH1bdHlwZV07XG4gICAgICB9KSwgZGVsYXkpO1xuICAgIH1cblxuICB9XG59KTtcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWluZGV4LmpzLm1hcCIsIi8vIERpcmVjdGl2ZXNcbmltcG9ydCByaXBwbGUgZnJvbSAnLi4vLi4vZGlyZWN0aXZlcy9yaXBwbGUnOyAvLyBUeXBlc1xuXG5pbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XG5leHBvcnQgZGVmYXVsdCBWdWUuZXh0ZW5kKHtcbiAgbmFtZTogJ3JpcHBsZWFibGUnLFxuICBkaXJlY3RpdmVzOiB7XG4gICAgcmlwcGxlXG4gIH0sXG4gIHByb3BzOiB7XG4gICAgcmlwcGxlOiB7XG4gICAgICB0eXBlOiBbQm9vbGVhbiwgT2JqZWN0XSxcbiAgICAgIGRlZmF1bHQ6IHRydWVcbiAgICB9XG4gIH0sXG4gIG1ldGhvZHM6IHtcbiAgICBnZW5SaXBwbGUoZGF0YSA9IHt9KSB7XG4gICAgICBpZiAoIXRoaXMucmlwcGxlKSByZXR1cm4gbnVsbDtcbiAgICAgIGRhdGEuc3RhdGljQ2xhc3MgPSAndi1pbnB1dC0tc2VsZWN0aW9uLWNvbnRyb2xzX19yaXBwbGUnO1xuICAgICAgZGF0YS5kaXJlY3RpdmVzID0gZGF0YS5kaXJlY3RpdmVzIHx8IFtdO1xuICAgICAgZGF0YS5kaXJlY3RpdmVzLnB1c2goe1xuICAgICAgICBuYW1lOiAncmlwcGxlJyxcbiAgICAgICAgdmFsdWU6IHtcbiAgICAgICAgICBjZW50ZXI6IHRydWVcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgICByZXR1cm4gdGhpcy4kY3JlYXRlRWxlbWVudCgnZGl2JywgZGF0YSk7XG4gICAgfVxuXG4gIH1cbn0pO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aW5kZXguanMubWFwIiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIl0sInNvdXJjZVJvb3QiOiIifQ==