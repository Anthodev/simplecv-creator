(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./assets/vue/components/PortfolioPro.vue":
/*!************************************************!*\
  !*** ./assets/vue/components/PortfolioPro.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PortfolioPro_vue_vue_type_template_id_2a0aa392___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PortfolioPro.vue?vue&type=template&id=2a0aa392& */ "./assets/vue/components/PortfolioPro.vue?vue&type=template&id=2a0aa392&");
/* harmony import */ var _PortfolioPro_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PortfolioPro.vue?vue&type=script&lang=js& */ "./assets/vue/components/PortfolioPro.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vuetify-loader/lib/runtime/installComponents.js */ "./node_modules/vuetify-loader/lib/runtime/installComponents.js");
/* harmony import */ var _node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vuetify_lib_components_VCarousel__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuetify/lib/components/VCarousel */ "./node_modules/vuetify/lib/components/VCarousel/index.js");
/* harmony import */ var vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vuetify/lib/components/VGrid */ "./node_modules/vuetify/lib/components/VGrid/index.js");
/* harmony import */ var vuetify_lib_components_VSheet__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vuetify/lib/components/VSheet */ "./node_modules/vuetify/lib/components/VSheet/index.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PortfolioPro_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PortfolioPro_vue_vue_type_template_id_2a0aa392___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PortfolioPro_vue_vue_type_template_id_2a0aa392___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* vuetify-loader */





_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default()(component, {VCarousel: vuetify_lib_components_VCarousel__WEBPACK_IMPORTED_MODULE_4__["VCarousel"],VCarouselItem: vuetify_lib_components_VCarousel__WEBPACK_IMPORTED_MODULE_4__["VCarouselItem"],VRow: vuetify_lib_components_VGrid__WEBPACK_IMPORTED_MODULE_5__["VRow"],VSheet: vuetify_lib_components_VSheet__WEBPACK_IMPORTED_MODULE_6__["VSheet"]})


/* hot reload */
if (false) { var api; }
component.options.__file = "assets/vue/components/PortfolioPro.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./assets/vue/components/PortfolioPro.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./assets/vue/components/PortfolioPro.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PortfolioPro_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--0-0!../../../node_modules/vue-loader/lib??vue-loader-options!./PortfolioPro.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/PortfolioPro.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PortfolioPro_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./assets/vue/components/PortfolioPro.vue?vue&type=template&id=2a0aa392&":
/*!*******************************************************************************!*\
  !*** ./assets/vue/components/PortfolioPro.vue?vue&type=template&id=2a0aa392& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PortfolioPro_vue_vue_type_template_id_2a0aa392___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./PortfolioPro.vue?vue&type=template&id=2a0aa392& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/PortfolioPro.vue?vue&type=template&id=2a0aa392&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PortfolioPro_vue_vue_type_template_id_2a0aa392___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PortfolioPro_vue_vue_type_template_id_2a0aa392___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/PortfolioPro.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/components/PortfolioPro.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      colors: ['indigo', 'warning', 'pink darken-2', 'red lighten-1', 'deep-purple accent-4'],
      slides: ['First', 'Second', 'Third', 'Fourth', 'Fifth']
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./assets/vue/components/PortfolioPro.vue?vue&type=template&id=2a0aa392&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./assets/vue/components/PortfolioPro.vue?vue&type=template&id=2a0aa392& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-carousel",
    { attrs: { cycle: "", height: "600", continuous: "" } },
    _vm._l(_vm.slides, function(slide, i) {
      return _c(
        "v-carousel-item",
        { key: i },
        [
          _c(
            "v-sheet",
            { attrs: { color: _vm.colors[i], height: "100%" } },
            [
              _c(
                "v-row",
                {
                  staticClass: "fill-height",
                  attrs: { align: "center", justify: "center" }
                },
                [
                  _c("div", { staticClass: "display-3" }, [
                    _vm._v(_vm._s(slide) + " Slide")
                  ])
                ]
              )
            ],
            1
          )
        ],
        1
      )
    }),
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vuetify/lib/components/VCarousel/VCarousel.js":
/*!********************************************************************!*\
  !*** ./node_modules/vuetify/lib/components/VCarousel/VCarousel.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _src_components_VCarousel_VCarousel_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../src/components/VCarousel/VCarousel.sass */ "./node_modules/vuetify/src/components/VCarousel/VCarousel.sass");
/* harmony import */ var _src_components_VCarousel_VCarousel_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VCarousel_VCarousel_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _VWindow_VWindow__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../VWindow/VWindow */ "./node_modules/vuetify/lib/components/VWindow/VWindow.js");
/* harmony import */ var _VBtn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../VBtn */ "./node_modules/vuetify/lib/components/VBtn/index.js");
/* harmony import */ var _VIcon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../VIcon */ "./node_modules/vuetify/lib/components/VIcon/index.js");
/* harmony import */ var _VProgressLinear__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../VProgressLinear */ "./node_modules/vuetify/lib/components/VProgressLinear/index.js");
/* harmony import */ var _mixins_button_group__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../mixins/button-group */ "./node_modules/vuetify/lib/mixins/button-group/index.js");
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../util/helpers */ "./node_modules/vuetify/lib/util/helpers.js");
/* harmony import */ var _util_console__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../util/console */ "./node_modules/vuetify/lib/util/console.js");
// Styles
 // Extensions

 // Components



 // Mixins
// TODO: Move this into core components v2.0

 // Utilities



/* harmony default export */ __webpack_exports__["default"] = (_VWindow_VWindow__WEBPACK_IMPORTED_MODULE_1__["default"].extend({
  name: 'v-carousel',
  props: {
    continuous: {
      type: Boolean,
      default: true
    },
    cycle: Boolean,
    delimiterIcon: {
      type: String,
      default: '$delimiter'
    },
    height: {
      type: [Number, String],
      default: 500
    },
    hideDelimiters: Boolean,
    hideDelimiterBackground: Boolean,
    interval: {
      type: [Number, String],
      default: 6000,
      validator: value => value > 0
    },
    mandatory: {
      type: Boolean,
      default: true
    },
    progress: Boolean,
    progressColor: String,
    showArrows: {
      type: Boolean,
      default: true
    },
    verticalDelimiters: {
      type: String,
      default: undefined
    }
  },

  data() {
    return {
      internalHeight: this.height,
      slideTimeout: undefined
    };
  },

  computed: {
    classes() {
      return { ..._VWindow_VWindow__WEBPACK_IMPORTED_MODULE_1__["default"].options.computed.classes.call(this),
        'v-carousel': true,
        'v-carousel--hide-delimiter-background': this.hideDelimiterBackground,
        'v-carousel--vertical-delimiters': this.isVertical
      };
    },

    isDark() {
      return this.dark || !this.light;
    },

    isVertical() {
      return this.verticalDelimiters != null;
    }

  },
  watch: {
    internalValue: 'restartTimeout',
    interval: 'restartTimeout',

    height(val, oldVal) {
      if (val === oldVal || !val) return;
      this.internalHeight = val;
    },

    cycle(val) {
      if (val) {
        this.restartTimeout();
      } else {
        clearTimeout(this.slideTimeout);
        this.slideTimeout = undefined;
      }
    }

  },

  created() {
    /* istanbul ignore next */
    if (this.$attrs.hasOwnProperty('hide-controls')) {
      Object(_util_console__WEBPACK_IMPORTED_MODULE_7__["breaking"])('hide-controls', ':show-arrows="false"', this);
    }
  },

  mounted() {
    this.startTimeout();
  },

  methods: {
    genControlIcons() {
      if (this.isVertical) return null;
      return _VWindow_VWindow__WEBPACK_IMPORTED_MODULE_1__["default"].options.methods.genControlIcons.call(this);
    },

    genDelimiters() {
      return this.$createElement('div', {
        staticClass: 'v-carousel__controls',
        style: {
          left: this.verticalDelimiters === 'left' && this.isVertical ? 0 : 'auto',
          right: this.verticalDelimiters === 'right' ? 0 : 'auto'
        }
      }, [this.genItems()]);
    },

    genItems() {
      const length = this.items.length;
      const children = [];

      for (let i = 0; i < length; i++) {
        const child = this.$createElement(_VBtn__WEBPACK_IMPORTED_MODULE_2__["default"], {
          staticClass: 'v-carousel__controls__item',
          attrs: {
            'aria-label': this.$vuetify.lang.t('$vuetify.carousel.ariaLabel.delimiter', i + 1, length)
          },
          props: {
            icon: true,
            small: true,
            value: this.getValue(this.items[i], i)
          }
        }, [this.$createElement(_VIcon__WEBPACK_IMPORTED_MODULE_3__["default"], {
          props: {
            size: 18
          }
        }, this.delimiterIcon)]);
        children.push(child);
      }

      return this.$createElement(_mixins_button_group__WEBPACK_IMPORTED_MODULE_5__["default"], {
        props: {
          value: this.internalValue,
          mandatory: this.mandatory
        },
        on: {
          change: val => {
            this.internalValue = val;
          }
        }
      }, children);
    },

    genProgress() {
      return this.$createElement(_VProgressLinear__WEBPACK_IMPORTED_MODULE_4__["default"], {
        staticClass: 'v-carousel__progress',
        props: {
          color: this.progressColor,
          value: (this.internalIndex + 1) / this.items.length * 100
        }
      });
    },

    restartTimeout() {
      this.slideTimeout && clearTimeout(this.slideTimeout);
      this.slideTimeout = undefined;
      window.requestAnimationFrame(this.startTimeout);
    },

    startTimeout() {
      if (!this.cycle) return;
      this.slideTimeout = window.setTimeout(this.next, +this.interval > 0 ? +this.interval : 6000);
    }

  },

  render(h) {
    const render = _VWindow_VWindow__WEBPACK_IMPORTED_MODULE_1__["default"].options.render.call(this, h);
    render.data.style = `height: ${Object(_util_helpers__WEBPACK_IMPORTED_MODULE_6__["convertToUnit"])(this.height)};`;
    /* istanbul ignore else */

    if (!this.hideDelimiters) {
      render.children.push(this.genDelimiters());
    }
    /* istanbul ignore else */


    if (this.progress || this.progressColor) {
      render.children.push(this.genProgress());
    }

    return render;
  }

}));
//# sourceMappingURL=VCarousel.js.map

/***/ }),

/***/ "./node_modules/vuetify/lib/components/VCarousel/VCarouselItem.js":
/*!************************************************************************!*\
  !*** ./node_modules/vuetify/lib/components/VCarousel/VCarouselItem.js ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _VWindow_VWindowItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../VWindow/VWindowItem */ "./node_modules/vuetify/lib/components/VWindow/VWindowItem.js");
/* harmony import */ var _VImg__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../VImg */ "./node_modules/vuetify/lib/components/VImg/index.js");
/* harmony import */ var _util_mixins__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../util/mixins */ "./node_modules/vuetify/lib/util/mixins.js");
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../util/helpers */ "./node_modules/vuetify/lib/util/helpers.js");
/* harmony import */ var _mixins_routable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../mixins/routable */ "./node_modules/vuetify/lib/mixins/routable/index.js");
// Extensions
 // Components

 // Utilities



 // Types

const baseMixins = Object(_util_mixins__WEBPACK_IMPORTED_MODULE_2__["default"])(_VWindow_VWindowItem__WEBPACK_IMPORTED_MODULE_0__["default"], _mixins_routable__WEBPACK_IMPORTED_MODULE_4__["default"]);
/* @vue/component */

/* harmony default export */ __webpack_exports__["default"] = (baseMixins.extend({
  name: 'v-carousel-item',
  inheritAttrs: false,
  methods: {
    genDefaultSlot() {
      return [this.$createElement(_VImg__WEBPACK_IMPORTED_MODULE_1__["VImg"], {
        staticClass: 'v-carousel__item',
        props: { ...this.$attrs,
          height: this.windowGroup.internalHeight
        },
        on: this.$listeners,
        scopedSlots: {
          placeholder: this.$scopedSlots.placeholder
        }
      }, Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__["getSlot"])(this))];
    },

    genWindowItem() {
      const {
        tag,
        data
      } = this.generateRouteLink();
      data.staticClass = 'v-window-item';
      data.directives.push({
        name: 'show',
        value: this.isActive
      });
      return this.$createElement(tag, data, this.genDefaultSlot());
    }

  }
}));
//# sourceMappingURL=VCarouselItem.js.map

/***/ }),

/***/ "./node_modules/vuetify/lib/components/VCarousel/index.js":
/*!****************************************************************!*\
  !*** ./node_modules/vuetify/lib/components/VCarousel/index.js ***!
  \****************************************************************/
/*! exports provided: VCarousel, VCarouselItem, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _VCarousel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./VCarousel */ "./node_modules/vuetify/lib/components/VCarousel/VCarousel.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "VCarousel", function() { return _VCarousel__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _VCarouselItem__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./VCarouselItem */ "./node_modules/vuetify/lib/components/VCarousel/VCarouselItem.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "VCarouselItem", function() { return _VCarouselItem__WEBPACK_IMPORTED_MODULE_1__["default"]; });




/* harmony default export */ __webpack_exports__["default"] = ({
  $_vuetify_subcomponents: {
    VCarousel: _VCarousel__WEBPACK_IMPORTED_MODULE_0__["default"],
    VCarouselItem: _VCarouselItem__WEBPACK_IMPORTED_MODULE_1__["default"]
  }
});
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/vuetify/lib/mixins/button-group/index.js":
/*!***************************************************************!*\
  !*** ./node_modules/vuetify/lib/mixins/button-group/index.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_VItemGroup_VItemGroup__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../components/VItemGroup/VItemGroup */ "./node_modules/vuetify/lib/components/VItemGroup/VItemGroup.js");
// Extensions

/* @vue/component */

/* harmony default export */ __webpack_exports__["default"] = (_components_VItemGroup_VItemGroup__WEBPACK_IMPORTED_MODULE_0__["BaseItemGroup"].extend({
  name: 'button-group',

  provide() {
    return {
      btnToggle: this
    };
  },

  computed: {
    classes() {
      return _components_VItemGroup_VItemGroup__WEBPACK_IMPORTED_MODULE_0__["BaseItemGroup"].options.computed.classes.call(this);
    }

  },
  methods: {
    // Isn't being passed down through types
    genData: _components_VItemGroup_VItemGroup__WEBPACK_IMPORTED_MODULE_0__["BaseItemGroup"].options.methods.genData
  }
}));
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/vuetify/src/components/VCarousel/VCarousel.sass":
/*!**********************************************************************!*\
  !*** ./node_modules/vuetify/src/components/VCarousel/VCarousel.sass ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvdnVlL2NvbXBvbmVudHMvUG9ydGZvbGlvUHJvLnZ1ZSIsIndlYnBhY2s6Ly8vLi9hc3NldHMvdnVlL2NvbXBvbmVudHMvUG9ydGZvbGlvUHJvLnZ1ZT9kMmY1Iiwid2VicGFjazovLy8uL2Fzc2V0cy92dWUvY29tcG9uZW50cy9Qb3J0Zm9saW9Qcm8udnVlPzc4ODQiLCJ3ZWJwYWNrOi8vL2Fzc2V0cy92dWUvY29tcG9uZW50cy9Qb3J0Zm9saW9Qcm8udnVlIiwid2VicGFjazovLy8uL2Fzc2V0cy92dWUvY29tcG9uZW50cy9Qb3J0Zm9saW9Qcm8udnVlP2YxMDgiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVkNhcm91c2VsL1ZDYXJvdXNlbC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvdnVldGlmeS9saWIvY29tcG9uZW50cy9WQ2Fyb3VzZWwvVkNhcm91c2VsSXRlbS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvdnVldGlmeS9saWIvY29tcG9uZW50cy9WQ2Fyb3VzZWwvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3Z1ZXRpZnkvbGliL21peGlucy9idXR0b24tZ3JvdXAvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3Z1ZXRpZnkvc3JjL2NvbXBvbmVudHMvVkNhcm91c2VsL1ZDYXJvdXNlbC5zYXNzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBMkY7QUFDM0I7QUFDTDs7O0FBRzNEO0FBQzZGO0FBQzdGLGdCQUFnQiwyR0FBVTtBQUMxQixFQUFFLGtGQUFNO0FBQ1IsRUFBRSx1RkFBTTtBQUNSLEVBQUUsZ0dBQWU7QUFDakI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDc0c7QUFDekM7QUFDSTtBQUNiO0FBQ0c7QUFDdkQsb0dBQWlCLGFBQWEscUZBQVMsQ0FBQyw2RkFBYSxDQUFDLHVFQUFJLENBQUMsNEVBQU0sQ0FBQzs7O0FBR2xFO0FBQ0EsSUFBSSxLQUFVLEVBQUUsWUFpQmY7QUFDRDtBQUNlLGdGOzs7Ozs7Ozs7Ozs7QUMvQ2Y7QUFBQTtBQUFBLHdDQUE0TCxDQUFnQix3UEFBRyxFQUFDLEM7Ozs7Ozs7Ozs7OztBQ0FoTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN3QkE7QUFDQSxNQURBLGtCQUNBO0FBQ0E7QUFDQSxlQUNBLFFBREEsRUFFQSxTQUZBLEVBR0EsZUFIQSxFQUlBLGVBSkEsRUFLQSxzQkFMQSxDQURBO0FBUUEsZUFDQSxPQURBLEVBRUEsUUFGQSxFQUdBLE9BSEEsRUFJQSxRQUpBLEVBS0EsT0FMQTtBQVJBO0FBZ0JBO0FBbEJBLEc7Ozs7Ozs7Ozs7OztBQ3hCQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLLFNBQVMsMkNBQTJDLEVBQUU7QUFDM0Q7QUFDQTtBQUNBO0FBQ0EsU0FBUyxTQUFTO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBLGFBQWEsU0FBUyx1Q0FBdUMsRUFBRTtBQUMvRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCO0FBQzFCLGlCQUFpQjtBQUNqQjtBQUNBLDZCQUE2QiwyQkFBMkI7QUFDeEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUN2Q0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUMwRDs7QUFFakI7O0FBRWQ7QUFDRTtBQUNvQjtBQUNqRDs7QUFFb0Q7O0FBRUQ7QUFDTDtBQUMvQix1SEFBTztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQSxjQUFjLElBQUksd0RBQU87QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBOztBQUVBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0EsTUFBTSw4REFBUTtBQUNkO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQSxhQUFhLHdEQUFPO0FBQ3BCLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7O0FBRUEscUJBQXFCLFlBQVk7QUFDakMsMENBQTBDLDZDQUFJO0FBQzlDO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUyx1QkFBdUIsOENBQUs7QUFDckM7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7O0FBRUEsaUNBQWlDLDREQUFXO0FBQzVDO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7O0FBRUw7QUFDQSxpQ0FBaUMsd0RBQWU7QUFDaEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxHQUFHOztBQUVIO0FBQ0EsbUJBQW1CLHdEQUFPO0FBQzFCLG1DQUFtQyxtRUFBYSxlQUFlO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxDQUFDLENBQUMsRUFBQztBQUNILHFDOzs7Ozs7Ozs7Ozs7QUMzTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDaUQ7O0FBRWxCOztBQUVRO0FBQ007QUFDQTs7QUFFN0MsbUJBQW1CLDREQUFNLENBQUMsNERBQVcsRUFBRSx3REFBUTtBQUMvQzs7QUFFZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0NBQWtDLDBDQUFJO0FBQ3RDO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTyxFQUFFLDZEQUFPO0FBQ2hCLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBLENBQUMsQ0FBQyxFQUFDO0FBQ0gseUM7Ozs7Ozs7Ozs7OztBQzVDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFvQztBQUNRO0FBQ1I7QUFDckI7QUFDZjtBQUNBLElBQUksNkRBQVM7QUFDYixJQUFJLHFFQUFhO0FBQ2pCO0FBQ0EsQ0FBQyxFQUFDO0FBQ0YsaUM7Ozs7Ozs7Ozs7OztBQ1RBO0FBQUE7QUFBQTtBQUN1RTtBQUN2RTs7QUFFZSw4SUFBYTtBQUM1Qjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBLGFBQWEsK0VBQWE7QUFDMUI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQSxhQUFhLCtFQUFhO0FBQzFCO0FBQ0EsQ0FBQyxDQUFDLEVBQUM7QUFDSCxpQzs7Ozs7Ozs7Ozs7QUN4QkEsdUMiLCJmaWxlIjoiMy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0gZnJvbSBcIi4vUG9ydGZvbGlvUHJvLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0yYTBhYTM5MiZcIlxuaW1wb3J0IHNjcmlwdCBmcm9tIFwiLi9Qb3J0Zm9saW9Qcm8udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9Qb3J0Zm9saW9Qcm8udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5cblxuLyogbm9ybWFsaXplIGNvbXBvbmVudCAqL1xuaW1wb3J0IG5vcm1hbGl6ZXIgZnJvbSBcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvcnVudGltZS9jb21wb25lbnROb3JtYWxpemVyLmpzXCJcbnZhciBjb21wb25lbnQgPSBub3JtYWxpemVyKFxuICBzY3JpcHQsXG4gIHJlbmRlcixcbiAgc3RhdGljUmVuZGVyRm5zLFxuICBmYWxzZSxcbiAgbnVsbCxcbiAgbnVsbCxcbiAgbnVsbFxuICBcbilcblxuLyogdnVldGlmeS1sb2FkZXIgKi9cbmltcG9ydCBpbnN0YWxsQ29tcG9uZW50cyBmcm9tIFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWV0aWZ5LWxvYWRlci9saWIvcnVudGltZS9pbnN0YWxsQ29tcG9uZW50cy5qc1wiXG5pbXBvcnQgeyBWQ2Fyb3VzZWwgfSBmcm9tICd2dWV0aWZ5L2xpYi9jb21wb25lbnRzL1ZDYXJvdXNlbCc7XG5pbXBvcnQgeyBWQ2Fyb3VzZWxJdGVtIH0gZnJvbSAndnVldGlmeS9saWIvY29tcG9uZW50cy9WQ2Fyb3VzZWwnO1xuaW1wb3J0IHsgVlJvdyB9IGZyb20gJ3Z1ZXRpZnkvbGliL2NvbXBvbmVudHMvVkdyaWQnO1xuaW1wb3J0IHsgVlNoZWV0IH0gZnJvbSAndnVldGlmeS9saWIvY29tcG9uZW50cy9WU2hlZXQnO1xuaW5zdGFsbENvbXBvbmVudHMoY29tcG9uZW50LCB7VkNhcm91c2VsLFZDYXJvdXNlbEl0ZW0sVlJvdyxWU2hlZXR9KVxuXG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7XG4gIHZhciBhcGkgPSByZXF1aXJlKFwiL3Zhci93d3cvaHRtbC9ub2RlX21vZHVsZXMvdnVlLWhvdC1yZWxvYWQtYXBpL2Rpc3QvaW5kZXguanNcIilcbiAgYXBpLmluc3RhbGwocmVxdWlyZSgndnVlJykpXG4gIGlmIChhcGkuY29tcGF0aWJsZSkge1xuICAgIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgICBpZiAoIWFwaS5pc1JlY29yZGVkKCcyYTBhYTM5MicpKSB7XG4gICAgICBhcGkuY3JlYXRlUmVjb3JkKCcyYTBhYTM5MicsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVsb2FkKCcyYTBhYTM5MicsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH1cbiAgICBtb2R1bGUuaG90LmFjY2VwdChcIi4vUG9ydGZvbGlvUHJvLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0yYTBhYTM5MiZcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgYXBpLnJlcmVuZGVyKCcyYTBhYTM5MicsIHtcbiAgICAgICAgcmVuZGVyOiByZW5kZXIsXG4gICAgICAgIHN0YXRpY1JlbmRlckZuczogc3RhdGljUmVuZGVyRm5zXG4gICAgICB9KVxuICAgIH0pXG4gIH1cbn1cbmNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiYXNzZXRzL3Z1ZS9jb21wb25lbnRzL1BvcnRmb2xpb1Byby52dWVcIlxuZXhwb3J0IGRlZmF1bHQgY29tcG9uZW50LmV4cG9ydHMiLCJpbXBvcnQgbW9kIGZyb20gXCItIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTAtMCEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1BvcnRmb2xpb1Byby52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCI7IGV4cG9ydCBkZWZhdWx0IG1vZDsgZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMC0wIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vUG9ydGZvbGlvUHJvLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIiIsImV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9sb2FkZXJzL3RlbXBsYXRlTG9hZGVyLmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Qb3J0Zm9saW9Qcm8udnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTJhMGFhMzkyJlwiIiwiPHRlbXBsYXRlPlxuICA8di1jYXJvdXNlbFxuICAgIGN5Y2xlXG4gICAgaGVpZ2h0PVwiNjAwXCJcbiAgICBjb250aW51b3VzXG4gID5cbiAgICA8di1jYXJvdXNlbC1pdGVtIHYtZm9yPVwiKHNsaWRlLCBpKSBpbiBzbGlkZXNcIiA6a2V5PVwiaVwiPlxuICAgICAgPHYtc2hlZXRcbiAgICAgICAgOmNvbG9yPVwiY29sb3JzW2ldXCJcbiAgICAgICAgaGVpZ2h0PVwiMTAwJVwiXG4gICAgICA+XG4gICAgICAgIDx2LXJvd1xuICAgICAgICAgIGNsYXNzPVwiZmlsbC1oZWlnaHRcIlxuICAgICAgICAgIGFsaWduPVwiY2VudGVyXCJcbiAgICAgICAgICBqdXN0aWZ5PVwiY2VudGVyXCJcbiAgICAgICAgPlxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJkaXNwbGF5LTNcIj57eyBzbGlkZSB9fSBTbGlkZTwvZGl2PlxuICAgICAgICA8L3Ytcm93PlxuICAgICAgPC92LXNoZWV0PlxuICAgIDwvdi1jYXJvdXNlbC1pdGVtPlxuICA8L3YtY2Fyb3VzZWw+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuZXhwb3J0IGRlZmF1bHQge1xuICBkYXRhICgpIHtcbiAgICByZXR1cm4ge1xuICAgICAgY29sb3JzOiBbXG4gICAgICAgICdpbmRpZ28nLFxuICAgICAgICAnd2FybmluZycsXG4gICAgICAgICdwaW5rIGRhcmtlbi0yJyxcbiAgICAgICAgJ3JlZCBsaWdodGVuLTEnLFxuICAgICAgICAnZGVlcC1wdXJwbGUgYWNjZW50LTQnLFxuICAgICAgXSxcbiAgICAgIHNsaWRlczogW1xuICAgICAgICAnRmlyc3QnLFxuICAgICAgICAnU2Vjb25kJyxcbiAgICAgICAgJ1RoaXJkJyxcbiAgICAgICAgJ0ZvdXJ0aCcsXG4gICAgICAgICdGaWZ0aCcsXG4gICAgICBdLFxuICAgIH1cbiAgfSxcbn1cbjwvc2NyaXB0PlxuIiwidmFyIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3ZtID0gdGhpc1xuICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gIHJldHVybiBfYyhcbiAgICBcInYtY2Fyb3VzZWxcIixcbiAgICB7IGF0dHJzOiB7IGN5Y2xlOiBcIlwiLCBoZWlnaHQ6IFwiNjAwXCIsIGNvbnRpbnVvdXM6IFwiXCIgfSB9LFxuICAgIF92bS5fbChfdm0uc2xpZGVzLCBmdW5jdGlvbihzbGlkZSwgaSkge1xuICAgICAgcmV0dXJuIF9jKFxuICAgICAgICBcInYtY2Fyb3VzZWwtaXRlbVwiLFxuICAgICAgICB7IGtleTogaSB9LFxuICAgICAgICBbXG4gICAgICAgICAgX2MoXG4gICAgICAgICAgICBcInYtc2hlZXRcIixcbiAgICAgICAgICAgIHsgYXR0cnM6IHsgY29sb3I6IF92bS5jb2xvcnNbaV0sIGhlaWdodDogXCIxMDAlXCIgfSB9LFxuICAgICAgICAgICAgW1xuICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICBcInYtcm93XCIsXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZmlsbC1oZWlnaHRcIixcbiAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGFsaWduOiBcImNlbnRlclwiLCBqdXN0aWZ5OiBcImNlbnRlclwiIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiZGlzcGxheS0zXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICBfdm0uX3YoX3ZtLl9zKHNsaWRlKSArIFwiIFNsaWRlXCIpXG4gICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIDFcbiAgICAgICAgICApXG4gICAgICAgIF0sXG4gICAgICAgIDFcbiAgICAgIClcbiAgICB9KSxcbiAgICAxXG4gIClcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5cbmV4cG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0iLCIvLyBTdHlsZXNcbmltcG9ydCBcIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL1ZDYXJvdXNlbC9WQ2Fyb3VzZWwuc2Fzc1wiOyAvLyBFeHRlbnNpb25zXG5cbmltcG9ydCBWV2luZG93IGZyb20gJy4uL1ZXaW5kb3cvVldpbmRvdyc7IC8vIENvbXBvbmVudHNcblxuaW1wb3J0IFZCdG4gZnJvbSAnLi4vVkJ0bic7XG5pbXBvcnQgVkljb24gZnJvbSAnLi4vVkljb24nO1xuaW1wb3J0IFZQcm9ncmVzc0xpbmVhciBmcm9tICcuLi9WUHJvZ3Jlc3NMaW5lYXInOyAvLyBNaXhpbnNcbi8vIFRPRE86IE1vdmUgdGhpcyBpbnRvIGNvcmUgY29tcG9uZW50cyB2Mi4wXG5cbmltcG9ydCBCdXR0b25Hcm91cCBmcm9tICcuLi8uLi9taXhpbnMvYnV0dG9uLWdyb3VwJzsgLy8gVXRpbGl0aWVzXG5cbmltcG9ydCB7IGNvbnZlcnRUb1VuaXQgfSBmcm9tICcuLi8uLi91dGlsL2hlbHBlcnMnO1xuaW1wb3J0IHsgYnJlYWtpbmcgfSBmcm9tICcuLi8uLi91dGlsL2NvbnNvbGUnO1xuZXhwb3J0IGRlZmF1bHQgVldpbmRvdy5leHRlbmQoe1xuICBuYW1lOiAndi1jYXJvdXNlbCcsXG4gIHByb3BzOiB7XG4gICAgY29udGludW91czoge1xuICAgICAgdHlwZTogQm9vbGVhbixcbiAgICAgIGRlZmF1bHQ6IHRydWVcbiAgICB9LFxuICAgIGN5Y2xlOiBCb29sZWFuLFxuICAgIGRlbGltaXRlckljb246IHtcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6ICckZGVsaW1pdGVyJ1xuICAgIH0sXG4gICAgaGVpZ2h0OiB7XG4gICAgICB0eXBlOiBbTnVtYmVyLCBTdHJpbmddLFxuICAgICAgZGVmYXVsdDogNTAwXG4gICAgfSxcbiAgICBoaWRlRGVsaW1pdGVyczogQm9vbGVhbixcbiAgICBoaWRlRGVsaW1pdGVyQmFja2dyb3VuZDogQm9vbGVhbixcbiAgICBpbnRlcnZhbDoge1xuICAgICAgdHlwZTogW051bWJlciwgU3RyaW5nXSxcbiAgICAgIGRlZmF1bHQ6IDYwMDAsXG4gICAgICB2YWxpZGF0b3I6IHZhbHVlID0+IHZhbHVlID4gMFxuICAgIH0sXG4gICAgbWFuZGF0b3J5OiB7XG4gICAgICB0eXBlOiBCb29sZWFuLFxuICAgICAgZGVmYXVsdDogdHJ1ZVxuICAgIH0sXG4gICAgcHJvZ3Jlc3M6IEJvb2xlYW4sXG4gICAgcHJvZ3Jlc3NDb2xvcjogU3RyaW5nLFxuICAgIHNob3dBcnJvd3M6IHtcbiAgICAgIHR5cGU6IEJvb2xlYW4sXG4gICAgICBkZWZhdWx0OiB0cnVlXG4gICAgfSxcbiAgICB2ZXJ0aWNhbERlbGltaXRlcnM6IHtcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6IHVuZGVmaW5lZFxuICAgIH1cbiAgfSxcblxuICBkYXRhKCkge1xuICAgIHJldHVybiB7XG4gICAgICBpbnRlcm5hbEhlaWdodDogdGhpcy5oZWlnaHQsXG4gICAgICBzbGlkZVRpbWVvdXQ6IHVuZGVmaW5lZFxuICAgIH07XG4gIH0sXG5cbiAgY29tcHV0ZWQ6IHtcbiAgICBjbGFzc2VzKCkge1xuICAgICAgcmV0dXJuIHsgLi4uVldpbmRvdy5vcHRpb25zLmNvbXB1dGVkLmNsYXNzZXMuY2FsbCh0aGlzKSxcbiAgICAgICAgJ3YtY2Fyb3VzZWwnOiB0cnVlLFxuICAgICAgICAndi1jYXJvdXNlbC0taGlkZS1kZWxpbWl0ZXItYmFja2dyb3VuZCc6IHRoaXMuaGlkZURlbGltaXRlckJhY2tncm91bmQsXG4gICAgICAgICd2LWNhcm91c2VsLS12ZXJ0aWNhbC1kZWxpbWl0ZXJzJzogdGhpcy5pc1ZlcnRpY2FsXG4gICAgICB9O1xuICAgIH0sXG5cbiAgICBpc0RhcmsoKSB7XG4gICAgICByZXR1cm4gdGhpcy5kYXJrIHx8ICF0aGlzLmxpZ2h0O1xuICAgIH0sXG5cbiAgICBpc1ZlcnRpY2FsKCkge1xuICAgICAgcmV0dXJuIHRoaXMudmVydGljYWxEZWxpbWl0ZXJzICE9IG51bGw7XG4gICAgfVxuXG4gIH0sXG4gIHdhdGNoOiB7XG4gICAgaW50ZXJuYWxWYWx1ZTogJ3Jlc3RhcnRUaW1lb3V0JyxcbiAgICBpbnRlcnZhbDogJ3Jlc3RhcnRUaW1lb3V0JyxcblxuICAgIGhlaWdodCh2YWwsIG9sZFZhbCkge1xuICAgICAgaWYgKHZhbCA9PT0gb2xkVmFsIHx8ICF2YWwpIHJldHVybjtcbiAgICAgIHRoaXMuaW50ZXJuYWxIZWlnaHQgPSB2YWw7XG4gICAgfSxcblxuICAgIGN5Y2xlKHZhbCkge1xuICAgICAgaWYgKHZhbCkge1xuICAgICAgICB0aGlzLnJlc3RhcnRUaW1lb3V0KCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjbGVhclRpbWVvdXQodGhpcy5zbGlkZVRpbWVvdXQpO1xuICAgICAgICB0aGlzLnNsaWRlVGltZW91dCA9IHVuZGVmaW5lZDtcbiAgICAgIH1cbiAgICB9XG5cbiAgfSxcblxuICBjcmVhdGVkKCkge1xuICAgIC8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovXG4gICAgaWYgKHRoaXMuJGF0dHJzLmhhc093blByb3BlcnR5KCdoaWRlLWNvbnRyb2xzJykpIHtcbiAgICAgIGJyZWFraW5nKCdoaWRlLWNvbnRyb2xzJywgJzpzaG93LWFycm93cz1cImZhbHNlXCInLCB0aGlzKTtcbiAgICB9XG4gIH0sXG5cbiAgbW91bnRlZCgpIHtcbiAgICB0aGlzLnN0YXJ0VGltZW91dCgpO1xuICB9LFxuXG4gIG1ldGhvZHM6IHtcbiAgICBnZW5Db250cm9sSWNvbnMoKSB7XG4gICAgICBpZiAodGhpcy5pc1ZlcnRpY2FsKSByZXR1cm4gbnVsbDtcbiAgICAgIHJldHVybiBWV2luZG93Lm9wdGlvbnMubWV0aG9kcy5nZW5Db250cm9sSWNvbnMuY2FsbCh0aGlzKTtcbiAgICB9LFxuXG4gICAgZ2VuRGVsaW1pdGVycygpIHtcbiAgICAgIHJldHVybiB0aGlzLiRjcmVhdGVFbGVtZW50KCdkaXYnLCB7XG4gICAgICAgIHN0YXRpY0NsYXNzOiAndi1jYXJvdXNlbF9fY29udHJvbHMnLFxuICAgICAgICBzdHlsZToge1xuICAgICAgICAgIGxlZnQ6IHRoaXMudmVydGljYWxEZWxpbWl0ZXJzID09PSAnbGVmdCcgJiYgdGhpcy5pc1ZlcnRpY2FsID8gMCA6ICdhdXRvJyxcbiAgICAgICAgICByaWdodDogdGhpcy52ZXJ0aWNhbERlbGltaXRlcnMgPT09ICdyaWdodCcgPyAwIDogJ2F1dG8nXG4gICAgICAgIH1cbiAgICAgIH0sIFt0aGlzLmdlbkl0ZW1zKCldKTtcbiAgICB9LFxuXG4gICAgZ2VuSXRlbXMoKSB7XG4gICAgICBjb25zdCBsZW5ndGggPSB0aGlzLml0ZW1zLmxlbmd0aDtcbiAgICAgIGNvbnN0IGNoaWxkcmVuID0gW107XG5cbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgbGVuZ3RoOyBpKyspIHtcbiAgICAgICAgY29uc3QgY2hpbGQgPSB0aGlzLiRjcmVhdGVFbGVtZW50KFZCdG4sIHtcbiAgICAgICAgICBzdGF0aWNDbGFzczogJ3YtY2Fyb3VzZWxfX2NvbnRyb2xzX19pdGVtJyxcbiAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgJ2FyaWEtbGFiZWwnOiB0aGlzLiR2dWV0aWZ5LmxhbmcudCgnJHZ1ZXRpZnkuY2Fyb3VzZWwuYXJpYUxhYmVsLmRlbGltaXRlcicsIGkgKyAxLCBsZW5ndGgpXG4gICAgICAgICAgfSxcbiAgICAgICAgICBwcm9wczoge1xuICAgICAgICAgICAgaWNvbjogdHJ1ZSxcbiAgICAgICAgICAgIHNtYWxsOiB0cnVlLFxuICAgICAgICAgICAgdmFsdWU6IHRoaXMuZ2V0VmFsdWUodGhpcy5pdGVtc1tpXSwgaSlcbiAgICAgICAgICB9XG4gICAgICAgIH0sIFt0aGlzLiRjcmVhdGVFbGVtZW50KFZJY29uLCB7XG4gICAgICAgICAgcHJvcHM6IHtcbiAgICAgICAgICAgIHNpemU6IDE4XG4gICAgICAgICAgfVxuICAgICAgICB9LCB0aGlzLmRlbGltaXRlckljb24pXSk7XG4gICAgICAgIGNoaWxkcmVuLnB1c2goY2hpbGQpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy4kY3JlYXRlRWxlbWVudChCdXR0b25Hcm91cCwge1xuICAgICAgICBwcm9wczoge1xuICAgICAgICAgIHZhbHVlOiB0aGlzLmludGVybmFsVmFsdWUsXG4gICAgICAgICAgbWFuZGF0b3J5OiB0aGlzLm1hbmRhdG9yeVxuICAgICAgICB9LFxuICAgICAgICBvbjoge1xuICAgICAgICAgIGNoYW5nZTogdmFsID0+IHtcbiAgICAgICAgICAgIHRoaXMuaW50ZXJuYWxWYWx1ZSA9IHZhbDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0sIGNoaWxkcmVuKTtcbiAgICB9LFxuXG4gICAgZ2VuUHJvZ3Jlc3MoKSB7XG4gICAgICByZXR1cm4gdGhpcy4kY3JlYXRlRWxlbWVudChWUHJvZ3Jlc3NMaW5lYXIsIHtcbiAgICAgICAgc3RhdGljQ2xhc3M6ICd2LWNhcm91c2VsX19wcm9ncmVzcycsXG4gICAgICAgIHByb3BzOiB7XG4gICAgICAgICAgY29sb3I6IHRoaXMucHJvZ3Jlc3NDb2xvcixcbiAgICAgICAgICB2YWx1ZTogKHRoaXMuaW50ZXJuYWxJbmRleCArIDEpIC8gdGhpcy5pdGVtcy5sZW5ndGggKiAxMDBcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSxcblxuICAgIHJlc3RhcnRUaW1lb3V0KCkge1xuICAgICAgdGhpcy5zbGlkZVRpbWVvdXQgJiYgY2xlYXJUaW1lb3V0KHRoaXMuc2xpZGVUaW1lb3V0KTtcbiAgICAgIHRoaXMuc2xpZGVUaW1lb3V0ID0gdW5kZWZpbmVkO1xuICAgICAgd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZSh0aGlzLnN0YXJ0VGltZW91dCk7XG4gICAgfSxcblxuICAgIHN0YXJ0VGltZW91dCgpIHtcbiAgICAgIGlmICghdGhpcy5jeWNsZSkgcmV0dXJuO1xuICAgICAgdGhpcy5zbGlkZVRpbWVvdXQgPSB3aW5kb3cuc2V0VGltZW91dCh0aGlzLm5leHQsICt0aGlzLmludGVydmFsID4gMCA/ICt0aGlzLmludGVydmFsIDogNjAwMCk7XG4gICAgfVxuXG4gIH0sXG5cbiAgcmVuZGVyKGgpIHtcbiAgICBjb25zdCByZW5kZXIgPSBWV2luZG93Lm9wdGlvbnMucmVuZGVyLmNhbGwodGhpcywgaCk7XG4gICAgcmVuZGVyLmRhdGEuc3R5bGUgPSBgaGVpZ2h0OiAke2NvbnZlcnRUb1VuaXQodGhpcy5oZWlnaHQpfTtgO1xuICAgIC8qIGlzdGFuYnVsIGlnbm9yZSBlbHNlICovXG5cbiAgICBpZiAoIXRoaXMuaGlkZURlbGltaXRlcnMpIHtcbiAgICAgIHJlbmRlci5jaGlsZHJlbi5wdXNoKHRoaXMuZ2VuRGVsaW1pdGVycygpKTtcbiAgICB9XG4gICAgLyogaXN0YW5idWwgaWdub3JlIGVsc2UgKi9cblxuXG4gICAgaWYgKHRoaXMucHJvZ3Jlc3MgfHwgdGhpcy5wcm9ncmVzc0NvbG9yKSB7XG4gICAgICByZW5kZXIuY2hpbGRyZW4ucHVzaCh0aGlzLmdlblByb2dyZXNzKCkpO1xuICAgIH1cblxuICAgIHJldHVybiByZW5kZXI7XG4gIH1cblxufSk7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1WQ2Fyb3VzZWwuanMubWFwIiwiLy8gRXh0ZW5zaW9uc1xuaW1wb3J0IFZXaW5kb3dJdGVtIGZyb20gJy4uL1ZXaW5kb3cvVldpbmRvd0l0ZW0nOyAvLyBDb21wb25lbnRzXG5cbmltcG9ydCB7IFZJbWcgfSBmcm9tICcuLi9WSW1nJzsgLy8gVXRpbGl0aWVzXG5cbmltcG9ydCBtaXhpbnMgZnJvbSAnLi4vLi4vdXRpbC9taXhpbnMnO1xuaW1wb3J0IHsgZ2V0U2xvdCB9IGZyb20gJy4uLy4uL3V0aWwvaGVscGVycyc7XG5pbXBvcnQgUm91dGFibGUgZnJvbSAnLi4vLi4vbWl4aW5zL3JvdXRhYmxlJzsgLy8gVHlwZXNcblxuY29uc3QgYmFzZU1peGlucyA9IG1peGlucyhWV2luZG93SXRlbSwgUm91dGFibGUpO1xuLyogQHZ1ZS9jb21wb25lbnQgKi9cblxuZXhwb3J0IGRlZmF1bHQgYmFzZU1peGlucy5leHRlbmQoe1xuICBuYW1lOiAndi1jYXJvdXNlbC1pdGVtJyxcbiAgaW5oZXJpdEF0dHJzOiBmYWxzZSxcbiAgbWV0aG9kczoge1xuICAgIGdlbkRlZmF1bHRTbG90KCkge1xuICAgICAgcmV0dXJuIFt0aGlzLiRjcmVhdGVFbGVtZW50KFZJbWcsIHtcbiAgICAgICAgc3RhdGljQ2xhc3M6ICd2LWNhcm91c2VsX19pdGVtJyxcbiAgICAgICAgcHJvcHM6IHsgLi4udGhpcy4kYXR0cnMsXG4gICAgICAgICAgaGVpZ2h0OiB0aGlzLndpbmRvd0dyb3VwLmludGVybmFsSGVpZ2h0XG4gICAgICAgIH0sXG4gICAgICAgIG9uOiB0aGlzLiRsaXN0ZW5lcnMsXG4gICAgICAgIHNjb3BlZFNsb3RzOiB7XG4gICAgICAgICAgcGxhY2Vob2xkZXI6IHRoaXMuJHNjb3BlZFNsb3RzLnBsYWNlaG9sZGVyXG4gICAgICAgIH1cbiAgICAgIH0sIGdldFNsb3QodGhpcykpXTtcbiAgICB9LFxuXG4gICAgZ2VuV2luZG93SXRlbSgpIHtcbiAgICAgIGNvbnN0IHtcbiAgICAgICAgdGFnLFxuICAgICAgICBkYXRhXG4gICAgICB9ID0gdGhpcy5nZW5lcmF0ZVJvdXRlTGluaygpO1xuICAgICAgZGF0YS5zdGF0aWNDbGFzcyA9ICd2LXdpbmRvdy1pdGVtJztcbiAgICAgIGRhdGEuZGlyZWN0aXZlcy5wdXNoKHtcbiAgICAgICAgbmFtZTogJ3Nob3cnLFxuICAgICAgICB2YWx1ZTogdGhpcy5pc0FjdGl2ZVxuICAgICAgfSk7XG4gICAgICByZXR1cm4gdGhpcy4kY3JlYXRlRWxlbWVudCh0YWcsIGRhdGEsIHRoaXMuZ2VuRGVmYXVsdFNsb3QoKSk7XG4gICAgfVxuXG4gIH1cbn0pO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9VkNhcm91c2VsSXRlbS5qcy5tYXAiLCJpbXBvcnQgVkNhcm91c2VsIGZyb20gJy4vVkNhcm91c2VsJztcbmltcG9ydCBWQ2Fyb3VzZWxJdGVtIGZyb20gJy4vVkNhcm91c2VsSXRlbSc7XG5leHBvcnQgeyBWQ2Fyb3VzZWwsIFZDYXJvdXNlbEl0ZW0gfTtcbmV4cG9ydCBkZWZhdWx0IHtcbiAgJF92dWV0aWZ5X3N1YmNvbXBvbmVudHM6IHtcbiAgICBWQ2Fyb3VzZWwsXG4gICAgVkNhcm91c2VsSXRlbVxuICB9XG59O1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aW5kZXguanMubWFwIiwiLy8gRXh0ZW5zaW9uc1xuaW1wb3J0IHsgQmFzZUl0ZW1Hcm91cCB9IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvVkl0ZW1Hcm91cC9WSXRlbUdyb3VwJztcbi8qIEB2dWUvY29tcG9uZW50ICovXG5cbmV4cG9ydCBkZWZhdWx0IEJhc2VJdGVtR3JvdXAuZXh0ZW5kKHtcbiAgbmFtZTogJ2J1dHRvbi1ncm91cCcsXG5cbiAgcHJvdmlkZSgpIHtcbiAgICByZXR1cm4ge1xuICAgICAgYnRuVG9nZ2xlOiB0aGlzXG4gICAgfTtcbiAgfSxcblxuICBjb21wdXRlZDoge1xuICAgIGNsYXNzZXMoKSB7XG4gICAgICByZXR1cm4gQmFzZUl0ZW1Hcm91cC5vcHRpb25zLmNvbXB1dGVkLmNsYXNzZXMuY2FsbCh0aGlzKTtcbiAgICB9XG5cbiAgfSxcbiAgbWV0aG9kczoge1xuICAgIC8vIElzbid0IGJlaW5nIHBhc3NlZCBkb3duIHRocm91Z2ggdHlwZXNcbiAgICBnZW5EYXRhOiBCYXNlSXRlbUdyb3VwLm9wdGlvbnMubWV0aG9kcy5nZW5EYXRhXG4gIH1cbn0pO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aW5kZXguanMubWFwIiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIl0sInNvdXJjZVJvb3QiOiIifQ==